run:
	python pwaa/simplecontroler.py
runpypy:
	pypy pwaa/simplecontroler.py
createbd:
	cd pwaa;python database/maadb.py
	cd pwaa/database;python initaadb.py
insertsequence:
	cd pwaa/database;python insertsequences.py
test:
	cd pwaa; python -m unittest discover utils
	#~ cd pwaa; python -m unittest discover basicknowledge
	#~ cd pwaa;python -m unittest discover analyser
	#~ cd pwaa;python -m unittest discover evaluation
	#~ cd pwaa;python -m unittest discover learning
	
testpypy:
	cd pwaa;pypy -m unittest discover basicknowledge
#	cd pwaa;pypy -m unittest discover analyser
doc:
	epydoc --debug --html --name "PWAA" -v -o doc `python .script/allpath.py`
clean:
	rm -rf doc
	python .script/rmpyc.py
	python .script/rmpycache.py
open:
	geany `python .script/allpath.py` &
pylint:
	pylint `python .script/allpath.py`
genetic:
	ls
