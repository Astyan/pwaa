#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_mcombielems.py unittest for mcombielems class 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys

#~ sys.path.append('../')

from pwaa.basicknowledge.mcombielems import (MDoubleLinks,
                                             MSingleAAPTM,
                                             MSingleAAPTMResidue, 
                                             #~ MSingleAAPTMNLResidue,
                                             MSingleAAPTMNL,
                                             MAAPTMNLDLinks,
                                             SequenceDlink)
                                        
from pwaa.basicknowledge.melems import (MSingleAA, MSinglePTM, 
                                        MSingleResidue, MSingleLink,
                                        MSingleNeutralLoss)


class TestMDoubleLinks(unittest.TestCase):
    """ test of MDoubleLinks class """
    
    def test_10_a_x_a(self):
        """ test of the creation of doublelinks """
        #test unprot unprot unprot (compl, join, cut)
        compl = MSingleLink("a-cut", {}, "test2", "test2", 
                         {})
        join = MSingleLink("x-cut", {'H':1, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                         {'H':1, 'N':1, 'O':1, 'C': 1})
        cut = MSingleLink("a-cut", {}, "test2", "test2", 
                         {})
        res = MDoubleLinks(join, compl, cut)
        self.assertEqual(res.composition, {'H':1, 'N':1, 'O':1, 'C': 1})
        
    def test_10_a_x_c(self):
        """ test of the creation of doublelinks """
        #test unprot unprot prot (compl, join, cut)
        compl = MSingleLink("a-cut", {}, "test2", "test2", 
                         {})
        join = MSingleLink("x-cut", {'H':1, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                         {'H':1, 'N':1, 'O':1, 'C': 1})
        cut = MSingleLink("c-cut", {'H':2, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                         {'H':1, 'N':1, 'O':1, 'C': 1})
        res = MDoubleLinks(join, compl, cut)
        self.assertEqual(res.composition, {'H':4, 'N':2, 'O':2, 'C': 2})
        
    def test_10_c_z_a(self):
        #test prot unprot unprot
        compl = MSingleLink("c-cut", {'H':2, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                            {'H':1, 'N':1, 'O':1, 'C': 1})
        join = MSingleLink("z-cut", {}, "test2", "test2", 
                           {})
        cut = MSingleLink("a-cut", {}, "test2", "test2", 
                           {})
        res = MDoubleLinks(join, compl, cut)
        self.assertEqual(res.composition, {'H': -2})
    
    def test_10_c_z_c(self):
        #test prot unprot prot
        compl = MSingleLink("c-cut", {'H':2, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                            {'H':1, 'N':1, 'O':1, 'C': 1})
        join = MSingleLink("z-cut", {}, "test2", "test2", 
                           {})
        cut = MSingleLink("c-cut", {'H':2, 'N':1, 'O':1, 'C': 1}, "test2", "test2", 
                            {'H':1, 'N':1, 'O':1, 'C': 1})
        res = MDoubleLinks(join, compl, cut)
        self.assertEqual(res.composition, {'H':1, 'N':1, 'O':1, 'C': 1})
        
    
class TestMSingleAAPTM(unittest.TestCase):
    """test for the MSingleAAPTM class"""
        
    def test_10_init_msingleaaptm(self):
        """ test the init of msingleaaptm and the rule to create
        the mSingleAAPTM object"""
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("test", {'H':1, 'N':2}, "center", "A")
        res = MSingleAAPTM(msaa, msptm)
        self.assertIsInstance(res, MSingleAAPTM)
        self.assertEqual(res.ptm, "test")
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.composition, {'H':3, 'N':2})
        self.assertEqual(res.position, "center")
        
class TestMSingleAAPTMNL(unittest.TestCase):
    """test for the MSingleAAPTMNL class"""
    
    def test_10_msingleaaptmnl(self):
        """ test the init of MSingleAAPTMNL and the rule to create 
        the MSingleAAPTMNL object"""
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("ptm", {'H':1, 'N':2}, "center", "A")
        msaaptm = MSingleAAPTM(msaa, msptm)
        msnl = MSingleNeutralLoss("nl", {'H':-1}, "A", "ptm", "center")
        res = MSingleAAPTMNL(msaaptm, msnl)
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.ptm, "ptm")
        self.assertEqual(res.nl, "nl")
        self.assertEqual(res.composition, {'H':2, 'N':2})
        self.assertEqual(res.position, "center")
    
    def test_10_msingleaaptmnl_none(self):
        """ test the init of MSingleAAPTMNL abd the rule to create
        the MSingleAAPTMNL object ig the NL object is None"""
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("ptm", {'H':1, 'N':2}, "center", "A")
        msaaptm = MSingleAAPTM(msaa, msptm)
        res = MSingleAAPTMNL(msaaptm, None)
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.ptm, "ptm")
        self.assertEqual(res.nl, "None")
        self.assertEqual(res.composition, {'H':3, 'N':2})
        self.assertEqual(res.position, "center")
    


class TestMSingleAAPTMResidue(unittest.TestCase):
    """test for the MSingleAAPTMResidue class"""
    
    def test_10_init_aaptmresidue(self):
        """Test of the init"""
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("test", {'H':1, 'N':2}, "center", "A")
        msresidue = MSingleResidue("resi", {'H':1, 'N':2})
        msaaptm = MSingleAAPTM(msaa, msptm)
        res = MSingleAAPTMResidue(msaaptm, msresidue)
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.ptm, "test")
        self.assertEqual(res.composition, {'H': 4, 'N': 4})
        self.assertEqual(res.residue, "resi")
        
class TestMAAPTMNLDLinks(unittest.TestCase):
    """ test for MAAPTMNLDLinks class """
    
    def test_10_initaaptmnldlinks(self):
        """ test the creation of a aaptmnl with a MDoubleLink """
        l1 = MSingleLink("test1", {'H':1, 'N':2}, "test2", "test2", 
                         {'N' :2})
        l2 = MSingleLink("test2", {'H':1, 'N':2}, "test2", "test2", 
                         {'H':1, 'N':2})
        l3 = MSingleLink("test3", {'H':1, 'N':2}, "test2", "test2", 
                         {'H':1, 'N':2})
        dlinks = MDoubleLinks(l1, l2, l3)
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("ptm", {'H':1, 'N':2}, "center", "A")
        msaaptm = MSingleAAPTM(msaa, msptm)
        msaaptmnl = MSingleAAPTMNL(msaaptm, None)
        res = MAAPTMNLDLinks(msaaptmnl, dlinks)
        self.assertEqual(res.composition, {'H': 4, 'N': 6})
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.ptm, "ptm")
        self.assertEqual(res.nl, "None")
        self.assertEqual(res.joincomplement, "test2")
        self.assertEqual(res.joinside, "test1")
        self.assertEqual(res.cutside, "test3")
        
class TestSequenceDlink(unittest.TestCase):
    """ test for the SequenceDlink creation """
    
    def test_10_initsequencedlinks(self):
        """ test of SequenceDlinks creation"""
        l1 = MSingleLink("test1", {'H':1, 'N':2}, "test2", "test2", 
                         {'N' :2})
        l2 = MSingleLink("test2", {'H':1, 'N':2}, "test2", "test2", 
                         {'H':1, 'N':2})
        l3 = MSingleLink("test3", {'H':1, 'N':2}, "test2", "test2", 
                         {'H':1, 'N':2})
        dlinks = MDoubleLinks(l1, l2, l3)
        msaa = MSingleAA("A", {'H':2})
        msptm = MSinglePTM("ptm", {'H':1, 'N':2}, "center", "A")
        msaaptm = MSingleAAPTM(msaa, msptm)
        msaaptmnl = MSingleAAPTMNL(msaaptm, None)
        l_msaaptmnl = [msaaptmnl, msaaptmnl]
        res = SequenceDlink(l_msaaptmnl, dlinks)
        self.assertEqual({'H': 8, 'C': 1, 'O': 1, 'N': 9}, 
                         res.composition)
        self.assertEqual(('A', 'ptm', 'None'), res.sequence[0])
        self.assertEqual(res.compl, "test2")
        self.assertEqual(res.joinside, "test1")
        self.assertEqual(res.cutside, "test3")
        
if __name__ == '__main__':
    unittest.main()

