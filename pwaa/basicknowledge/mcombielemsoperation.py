#!/usr/bin/env python
# encoding: utf-8
"""mcombielemsoperation.py is used for extended operation we can do
on all mcombielems. Mostly to geting the minimal and the maximal
possible mass for given parameter
@author: Vezin Aurelien
@license: CECILL-B"""

import sys
import random



from pwaa.utils.masses import mult_compo, combine_compo, in_ppm_range
from pwaa.utils.atoms import MAtomsMassOp
        
    
class LinksOperations(object):
    """class used to make operations with links. It's been create
    for the class AAPTMNLLinksOperation and allow the computation
    of the maximal and minimal mass for more than one amino acid.
    It can also find possible cuts from 2 masses and a error rate.
    @ivar llinks: the list of all the link we want to work with
    @type llinks: list(MSingleLink)
    @ivar mtype: the type of mass we want to work with (avg or mono)
    isotopic mass is not handle right now
    @type mtype: str
    @ivar middle: the peptidic link between all peptidic elements 
    in atoms and an occurence number for each atoms
    @type middle: dict(str : int)
    @ivar cside: the heavier cut of the cside
    in atoms and an occurence number for each atoms
    @type cside: dict(str : int)
    @ivar nside: the heavier cut of the nside
    in atoms and an occurence number for each atoms
    @type nside: dict(str : int)
    @ivar clinks: all link of the cside except c-term
    @type clinks: list(MSingleLink)
    @ivar nlinks: all link of the nside except n-term
    @type nlinks: list(MSingleLink)
    """
        
    def __init__(self, alllinks, atoms=None):
        """ init of the class to do operations with links
        @param alllinks: a list of links we want work with
        @type alllinks: list(MSingleLink)"""
        self.mamo = MAtomsMassOp(atoms)
        self.llinks = alllinks
        self.mtype = "mono"
        self.middle = self._nocut()
        self.cside = self._maxlink("n-term")
        self.nside = self._maxlink("c-term")
        self.clinks = self._opposite_links("n-term", "c-term")
        self.nlinks = self._opposite_links("c-term", "n-term")
    
    def get_link(self, name):
        """ get the link with the name in param
        @param name: the name of the link
        @type name: str
        @return: the link
        @rtype: MSingleLink """
        for link in self.llinks:
            if link.link == name:
                return link.composition
    
    def _maxlink(self, side):
        """get the maximal link (in mass) of the opposite side
        @param side: the oppositie side of the max link (in mass)
        we want to find (n-term or c-term)
        @type side: str
        @return: the maximal link composition
        @rtype: dict(str : int)"""
        maxmass = 0.
        maxside = {}
        for link in self.llinks:
            if link.opposite == side:
                compomass = self.mamo.atomlist_to_masses(link.composition, 
                                                         self.mtype)
                if compomass > maxmass:
                    maxside = link.composition
                    maxmass = compomass
        return maxside 

    def _opposite_links(self, oppositeside, selfside):
        """ get all links opposite of the side oppositeside without
        selfside
        @param oppositeside: the opposite of links we want (n-term,
        c-term, ...)
        @type oppositeside: str
        @param selfside: the link (usualy term) of the self side
        we don't want on return links 
        @type selfside: str"""
        op_links = []
        for link in self.llinks:
            if link.opposite == oppositeside and link.link != selfside:
                op_links.append(link)
        return op_links
        
    
    def _nocut(self):
        """ get the middle without any cut, right now it's the same
        composition that x-cut
        @return: the composition of the peptidic link between 2
        amino acid
        @rtype: dict(str : int)
        """
        return self.get_link("x-cut")
    
    def minmass(self, masslist):
        """ compute the minimal peptide mass composed with
        elements masses find in masslist
        @precondition: masslist is represent the minimal mass list
        of an element list
        @precondition: mass type is mono or avg
        @param masslist: the list of minimal possible mass for each
        elements in the peptide
        @type masslist: list(float)
        @return: the minimal mass of the whole peptide
        @rtype: float"""
        middlenumber = len(masslist) - 1
        middlecompo = mult_compo(self.middle, middlenumber)
        return (sum(masslist) + self.mamo.atomlist_to_masses(middlecompo, 
                                                             self.mtype))

    def maxmass(self, masslist):
        """ compute the maximal peptide mass composed with elements
        masses find in masslist
        @precondition: masslist is represent the maximal mass list
        of an element list
        @precondition: mass type is mono or avg
        @param masslist: the list of maximal possible mass for each
        elements in the peptide
        @type masslist: list(float)
        @return: the maximal mass of the whole peptide
        @rtype: float"""
        middlenumber = len(masslist) - 1
        middlecompo = mult_compo(self.middle, middlenumber)
        overlinkscompo = combine_compo(self.cside, self.nside)
        deltaaddcompo = combine_compo(middlecompo, overlinkscompo)
        #in the masslist all mass is the maximal
        #that's means that all mass have the maximal link of both
        #side. So we remove it
        deltasubcompo = mult_compo(overlinkscompo, middlenumber + 1)
        deltaaddmass = self.mamo.atomlist_to_masses(deltaaddcompo,
                                                    self.mtype)
        deltasubmass = self.mamo.atomlist_to_masses(deltasubcompo,
                                                    self.mtype)
        return sum(masslist) +  deltaaddmass - deltasubmass
    
    def get_cuts(self, masslist, masstarget, ppm):
        """  get possibles links that work for matching a theorical
        mass compute with a list of minimal mass with a experimental
        mass
        @param masslist: the list of minimal possible mass for each
        elements in the peptide
        @type masslist: list(float)
        @param masstarget: an experiemental mass
        @type masstarget: float
        @param ppm: the error rate in ppm
        @type ppm: int
        @return: a list of couple links matching for the compute
        theorical mass with the experimental mass
        @rtype: list((MSingleLink, MSingleLink)) """
        cuts = []
        masscore = self.minmass(masslist)
        for clink in self.llinks:
            if clink.opposite == "n-term":
                for nlink in self.llinks:
                    if nlink.opposite == "c-term":
                        compoadd = combine_compo(clink.composition,
                                                 nlink.composition)
                        massadd = self.mamo.atomlist_to_masses(compoadd,
                                                               self.mtype)
                        massres = masscore + massadd
                        if in_ppm_range(masstarget, massres, ppm):
                            cuts.append((clink, nlink))
        return cuts


class AAPTMOperations(object):
    """ Class used to do some opperations on aaptm
    @ivar aasptms: list of all aa with ptm (ptm None can be in)
    @type aasptms: list(MSingleAAPTM)
    @ivar cterm: list of all aa with ptm that can be on the cterm
    @type cterm: list(MSingleAAPTM)
    @ivar nterm: list of all aa with ptm that can be on the nterm
    @type nterm: list(MSingleAAPTM)
    @ivar center: list of all aa with ptm that can be on the any other
    side (not at c-term or n-term)
    @type center: list(MSingleAAPTM)"""
    
    def __init__(self, aasptms):
        """add the aasptms in the class and compute to set all side
        @param aasptms: the list of aasptms to evaluate
        @type aasptms: list(MSingleAAPTM)"""
        self.aasptms = aasptms
        self.cterm = self._all_side("c-term")
        self.nterm = self._all_side("n-term")
        self.center = self._all_side("center")
    
    def _all_side(self, side):
        """give all aasptms that can be on the given side
        @param side: the position where aasptms can be (c-term, 
        n-term, center)
        @type side: str
        @return: the list of aasptms matching with the side
        @rtype:  list(MSingleAAPTM)"""
        aasptmsside = []
        for aaptm in self.aasptms:
            if aaptm.position == side:
                aasptmsside.append(aaptm)
        return aasptmsside

    def create_sequence(self, size):
        """ create a random sequence of aasptms 
        from n-term to c-term
        @param size: the size of the sequence
        @type size: int
        @return: a random sequence of aasptms
        @rtype: list(MSingleAAPTM)"""
        sequence = []
        for pos in range(0, size):
            if pos == 0:
                sequence.append(random.choice(self.nterm))
            elif pos == size-1:
                sequence.append(random.choice(self.cterm))
            else:
                sequence.append(random.choice(self.center))
        return sequence

    def mutate_sequence(self, sequence):
        """ do the mutation of one sequence 
        @param sequence: a sequence of aaptm
        @type sequence: list(MSingleAAPTM)
        @return: the sequence with one elem modify
        @rtype: list(MSingleAAPTM)
        ! can change by the same aaptm"""
        size = len(sequence)
        to_mute = random.randint(0, size-1)
        if to_mute == 0:
            sequence[to_mute] = random.choice(self.nterm)
        elif to_mute == size - 1:
            sequence[to_mute] = random.choice(self.cterm)
        else:
            sequence[to_mute] = random.choice(self.center)
        return sequence
