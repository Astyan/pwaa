#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_melemscreate.py unittest for creations rules in melemscreate 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys


from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_residue, 
                                              fill_all_link,
                                              fill_all_neutral_loss)
from pwaa.basicknowledge.melems import (MSingleAA, MSinglePTM, 
                                        MSingleResidue, MSingleLink,
                                        MSingleNeutralLoss)


class TestMelemscreate(unittest.TestCase):
    """test of all reason in melemscreate"""
    
    def test_10_fill_all_aa(self):
        "test of fill_all_aa"
        lres = fill_all_aa()
        self.assertEqual(len(lres), 22)
        self.assertEqual(lres[3].aa, "D")
        self.assertEqual(lres[3].composition, {u'C': 3, u'H': 4,
                                               u'O': 2, u'N': 0,
                                               u'S': 0, u'Se': 0})
        for elem in lres:
            self.assertIsInstance(elem, MSingleAA)
            
            
    def test_10_fill_all_ptm(self):
        """ test of fill_all_ptm """
        lres = fill_all_ptm()
        self.assertEqual(len(lres), 457)
        self.assertEqual(lres[0].ptm, "Carbamidomethyl")
        self.assertEqual(lres[0].composition, {u'As': 0, u'C': 2, 
                                               u'H': 3, u'N': 1, 
                                               u'O': 1, u'P': 0, 
                                               u'S': 0, u'Se': 0})


        self.assertEqual(lres[0].position, "c-term")
        self.assertEqual(lres[0].posaa, "C")
        for elem in lres:
            self.assertIsInstance(elem, MSinglePTM)
            
    def test_10_fill_all_residue(self):
        """ test of fill_all_resude"""
        lres = fill_all_residue()
        self.assertEqual(len(lres), 1)
        self.assertEqual(lres[0].residue, "Immonium")
        self.assertEqual(lres[0].composition, {u'C': 1, u'H': 3, 
                                               u'O': 0, u'N': 1, 
                                               u'S': 0, u'Se': 0})
        for elem in lres:
            self.assertIsInstance(elem, MSingleResidue)
    
    def test_10_fill_all_link(self):
        """ test of fill_all_link """
        lres = fill_all_link()
        self.assertEqual(len(lres), 32)
        self.assertEqual(lres[0].link, "c-term")
        self.assertEqual(lres[0].composition, {u'H': 1, u'C': 1, 
                                               u'O': 2, u'N': 0})
        self.assertEqual(lres[0].complementary, None)
        self.assertIn(lres[0].opposite, ["x-cut", "y-cut", 
                                         "z-cut", "n-term"])
        for elem in lres:
            self.assertIsInstance(elem, MSingleLink)
    
    def test_10_fill_all_neutral_loss(self):
        """test of fill_all_neutral_loss"""
        lres = fill_all_neutral_loss()
        self.assertEqual(len(lres), 69)
        for elem in lres:
            self.assertIsInstance(elem, MSingleNeutralLoss)
            
        
        
if __name__ == '__main__':
    unittest.main()
        
        
