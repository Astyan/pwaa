#!/usr/bin/env python
# encoding: utf-8
"""melems.py contain function to create objects in melems
@author: Vezin Aurelien
@license: CECILL-B"""

import sys


from pwaa.basicknowledge.melems import (MSingleAA, MSinglePTM,
                                        MSingleResidue, MSingleLink,
                                        MSingleNeutralLoss)
import pwaa.database.vaadb as vaadb


def fill_all_aa(allaa=vaadb.get_amino_acid()):
    """Get all the aa from the database and create a list of
    MSingleAA
    @return: the list of all MSingleAA
    @rtype: list(MSingleAA)"""
    lmsaa = []
    for key in allaa:
        lmsaa.append(MSingleAA(key, allaa[key]))
    return lmsaa
        
    
def fill_all_ptm(allptmpos=vaadb.get_ptm_pos(), allptmcompo=vaadb.get_ptm_compo()):
    """Get all the ptms from the database and create a list of
    MSinglePTM
    @return: the list of all single ptm
    @rtype: list(MSinglePTM)
    """
    lmsptm = []
    for ptm in allptmpos:
        for aa in allptmpos[ptm]:
            poslist = sorted(allptmpos[ptm][aa])
            for pos in poslist:
                lmsptm.append(MSinglePTM(ptm, allptmcompo[ptm], 
                                         pos, aa))
    return lmsptm

def fill_all_residue(allresiduecom=vaadb.get_small_residue_compo()):
    """Get all the residue from the database and create a list of
    MSingleResidue
    @return: the list of all residue
    @rtype: list(MSingleResidue)
    """
    #why the order of aa is random ?
    lmsresidue = []
    for residue in allresiduecom:
        composition = allresiduecom[residue]
        lmsresidue.append(MSingleResidue(residue, composition))
    return lmsresidue


def fill_all_link(alllinks=vaadb.get_link()):
    """Get all the links from the database and create a list of 
    MSingleLink
    @return: the list of link*
    @rtype: list(MSingleLink)"""
    lmslink = []
    for link in alllinks:
        for opposite in link["opposite"]:
            lmslink.append(MSingleLink(link["name"], 
                                       link["composition"],
                                       link["complementary"],
                                       opposite,
                                       link["unprotcompo"]))
    return lmslink

def fill_all_neutral_loss(allneutrallosspos=vaadb.get_neutral_loss_pos(),
    allneutrallosscom=vaadb.get_neutral_loss_compo()):
    """Get all the neutral loss from the database and create a
    list of MSingleNeutralLoss
    @return: the list of all neutral loss
    @rtype: list(MSingleNeutralLoss)"""
    lmsnl = []
    for neutralloss in allneutrallosscom:
        composition = allneutrallosscom[neutralloss]
        for elem in allneutrallosspos[neutralloss]:
            lmsnl.append(MSingleNeutralLoss(neutralloss, composition,
                                            elem['aa'],
                                            elem['ptms'],
                                            elem['position']))
    return lmsnl

if "__main__" == __name__:
    #~ fill_all_aa()
    #~ fill_all_ptm()
    #~ fill_all_residue()
    fill_all_link()

