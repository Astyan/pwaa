#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_mcombielemsoperation.py unittest for all mcombielemsoperations 
class 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys
import copy

sys.path.append('../')


from basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                         fill_all_link)

from basicknowledge.mcombielemscreate import (reason_all_aaptm)

from basicknowledge.mcombielemsoperation import (LinksOperations,
                                                 AAPTMOperations)


class TestLinksOperations(unittest.TestCase):
    """ test for the TestLinksOperations class """
    
    @classmethod
    def setUpClass(cls):
        """ a setup to run only one """
        links = fill_all_link()
        cls.lop = LinksOperations(links)

    def test_10_links(self):
        """ test if the setup has work well"""
        self.assertEqual(self.lop.middle,
                         {u'H': 1, u'C': 1, u'O': 1, u'N': 1})
        self.assertEqual(self.lop.cside,
                         {u'H': 1, u'C': 1, u'O': 2, u'N': 0})
        self.assertEqual(self.lop.nside,
                         {u'H': 1, u'C': 1, u'O': 1, u'N': 1})
    
    
    def test_10_opposite_links(self):
        """ a test of opposite links """
        self.assertEqual(len(self.lop.clinks), 3)
        self.assertEqual(len(self.lop.nlinks), 3)
    
    def test_11_minmass(self):
        """test of minmass """
        mini = self.lop.minmass([100., 42.])
        self.assertAlmostEqual(mini, 185.005814, 4)
    
    def test_11_maxmass(self):
        """test of maxmass """
        maxi = self.lop.maxmass([499., 499.])
        self.assertAlmostEqual(maxi, 953.00234, 4)

    def test_12_cuts(self):
        """test of cuts """
        res = self.lop.get_cuts([100.], 100., 50)
        self.assertEqual(res[0][0].link, "a-cut")
        self.assertEqual(res[0][1].link, "z-cut")
    

class TestAAPTMOperations(unittest.TestCase):
    """class to test operations on list of MSingleAAPTM """
    
    @classmethod
    def setUpClass(cls):
        """ the setup method to lauch only once """
        aas = fill_all_aa() 
        ptms = fill_all_ptm()
        aasptms = reason_all_aaptm(aas, ptms)
        cls.aaptmop = AAPTMOperations(aasptms)
        
    def test_10_all_side(self):
        """ test all side creation"""
        self.assertEqual(len(self.aaptmop.cterm), 132)
        self.assertEqual(len(self.aaptmop.nterm), 220)
        self.assertEqual(len(self.aaptmop.center), 105)
    
    def test_11_create_sequence(self):
        """ test the creation of random sequence """
        self.assertEqual(len(self.aaptmop.create_sequence(15)), 15)
    
    def test_12_mutate_sequence(self):
        """ test mutate_sequence """
        seq_to_mute = self.aaptmop.create_sequence(15)
        seq_muted = self.aaptmop.mutate_sequence(copy.deepcopy(seq_to_mute))
        nbdiff = 0
        for i in range(0, len(seq_to_mute) - 1):
            if (seq_to_mute[i].aa != seq_muted[i].aa or
                    seq_to_mute[i].ptm != seq_muted[i].ptm):
                nbdiff += 1
        self.assertIn(nbdiff, [0, 1])
        

if __name__ == '__main__':
    unittest.main()
        
