#!/usr/bin/env python
# encoding: utf-8
"""melems.py is a model to create individual amino acid
ptms, neutral loss and other simple things
@author: Vezin Aurelien
@license: CECILL-B"""

import sys

#~ sys.path.append('../')

from pwaa.utils.masses import dif_compo, combine_compo
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.atoms import MAtoms

class ChimicalElement(object):
    """ this class represent an element with a composition
    @ivar composition: the composition of the chemical element
    @type composition: dict(char: int) """
    
    def __init__(self, composition):
        """ This class is the mother class for any element
        @param composition: the composition of the chemical element
        @type composition: dict(char: int)"""
        self.composition = composition
    
    def __eq__(self, other):
        """ check if the element is equal to an other element
        (composition check) 
        @param other: the other element 
        @type other: ChimicalElement
        @return: True if equal, False if not
        @rtype: Boolean """
        compo_tmp = combine_compo(self.composition, other.composition)
        for key in compo_tmp:
            if compo_tmp[key] != 0:
                scompo = self.composition.get(key, 0)
                ocompo = other.composition.get(key, 0)
                if scompo != ocompo:
                    return False
        return True
    
    def __ne__(self, other):
        """ check if the element is not equal to an other element
        (composition check) 
        @param other: the other element 
        @type other: ChimicalElement
        @return: False if equal, True if not
        @rtype: Boolean """
        return not self.__eq__(other)
    
    def __hash__(self):
        """ return the hash of the element by its mono mass
        @return: the hash
        @rtype: int"""
        to_hash = []
        for k, v in self.composition.items():
            to_hash.append(str(k))
            to_hash.append(str(v))        
        return hash(''.join(to_hash))
            
        
class MSingleAA(ChimicalElement):
    """this class represent one amino acid
    @ivar aa: the keychar of the amino acid
    @type aa: char
    @ivar composition: the compositon of the amino acid in atoms
    @type composition: {char: int}"""
    
    def __init__(self, aakey, composition):
        """the init lauch the rule to create the aa
        @param aakey: the key representing the aa
        @type aakey: char
        @param composition: the composition of the amino_acid
        @type composition: {char: int}"""
        ChimicalElement.__init__(self, composition)
        self.rule_create_aa(aakey)
    
    def rule_create_aa(self, aakey):
        """this rule create the aa
        @param aakey: the key representing the aa
        @type aakey: char"""
        self.aa = aakey
    
    def __eq__(self, other):
        """ check if the element is equal to an other element
        (aakey check) 
        @param other: the other element 
        @type other: MSingleAA
        @return: True if equal, False if not
        @rtype: Boolean """
        return (isinstance(self, MSingleAA) and 
                isinstance(other, MSingleAA) and self.aa == other.aa)
    
    def __ne__(self, other):
        """ check if the element is not equal to an other element
        (aakey check) 
        @param other: the other element 
        @type other: MSingleAA
        @return: False if equal, True if not
        @rtype: Boolean """
        return not self.__eq__(other)
    
    def __hash__(self):
        """ return the hash of the element by its aakey
        @return: the hash
        @rtype: int"""
        return hash(self.aa)

class MSinglePTM(ChimicalElement):
    """ this class is used to represent one PTM
    @ivar ptm: the name of the ptm
    @type ptm: string
    @ivar composition: the composition of the ptm
    @type composition: {char: int}
    @ivar position: the position of the amino acid needed to have the 
    ptm (n-term, c-term, any)
    @type position: string
    @ivar posaa: the amino acid that can have this ptm
    @type posaa: char"""
    
    def __init__(self, name, composition, position, posaa):
        """this init lauch the rule to create the ptm
        @param name: the name of the ptm
        @type name: string
        @param composition: the composition of the ptm
        @type composition: {char: int}
        @param position: the position of the amino acid needed to have the 
        ptm (n-term, c-term, any)
        @type position: string
        @param posaa: the amino acid that can have this ptm
        @type posaa: char"""
        ChimicalElement.__init__(self, composition)
        self.rule_create_ptm(name, position, posaa)
    
    def rule_create_ptm(self, name, position, posaa):
        """this rule to create the ptm
        @param name: the name of the ptm
        @type name: string
        @param position: the position of the amino acid needed to have the 
        ptm (n-term, c-term, any)
        @type position: string
        @param posaa: the amino acid that can have this ptm
        @type posaa: char"""
        self.ptm = name
        self.position = position
        self.posaa = posaa

        
class MSingleResidue(ChimicalElement):
    """this class is used to represent a Small Residue (immonium ions,
    etc ...
    @ivar residue: the type of residue (Immonium ...)
    @type residue: string
    @ivar composition: a composition in atoms
    @type composition: dict(char: int)
    @ivar aa: the aa where the residue could be form with
    @type aa: char
    @ivar ptm: the ptm where the residue could be form with
    @type ptm: string
    ivar side: the side where the residue could be form with 
    (if center everywhere except c-term and n-term)
    @type side: string
    """
    
    def __init__(self, name, composition):
        """the init lauch a rule to create the residue
        @param name: the name of the residue
        @type name: string
        @param composition: a composition in atoms
        @type composition: dict(char: int)
        @param aa: the aa where the residue could be form with
        @type aa: char
        @param ptm: the ptm where the residue could be form with
        @type ptm: string
        ivar side: the side where the residue could be form with 
        (if center everywhere except c-term and n-term)
        @type side: string"""
        ChimicalElement.__init__(self, composition)
        self.rule_create_residue(name)
    
    def rule_create_residue(self, name):
        """the rule to create the residue
        @param name: the name of the residue
        @type name: string
        @param aa: the aa where the residue could be form with
        @type aa: char
        @param ptm: the ptm where the residue could be form with
        @type ptm: string
        ivar side: the side where the residue could be form with 
        (if center everywhere except c-term and n-term)
        @type side: string"""
        self.residue = name
        
class MSingleLink(ChimicalElement):
    """This class is used to represent a Link
    @ivar name: the type link (n-term, x-cut, etc ..)
    @type name: string
    @ivar composition: a composition in atoms
    @type composition: dict(char: int)
    @ivar complementary: the complementary (the complementary of y-cut 
    is b-cut)
    @type complementary: string
    @ivar opposite: it's the cuts that could be on the opposite side
    @type opposite: string
    @ivar unprot: a composition in atoms as unprotoned link
    @type unprot: dict(char: int)
    """
    
    def __init__(self, name, composition, complementary, opposite,
                 unprotcomposition):
        """the init lauch the rule to create the link
        @param name: the type link (n-term, x-cut, etc ..)
        @type name: string
        @param composition: a composition in atoms
        @type composition: dict(char: int)
        @param complementary: the complementary (the complementary of y-cut 
        is b-cut)
        @type complementary: string
        @param opposite: it's the cuts that could be on the opposite side
        @type opposite: string
        @param unprotcomposition: a composition in atoms
        @type unprotcomposition: dict(char: int)
        """
        ChimicalElement.__init__(self, composition)
        self.rule_create_link(name, complementary, opposite, unprotcomposition)
    
    def rule_create_link(self, name, complementary, opposite, unprotcomposition):
        """ the rule to create the link
        @param name: the type link (n-term, x-cut, etc ..)
        @type name: string
        @param complementary: the complementary (the complementary of y-cut 
        is b-cut)
        @type complementary: string
        @param opposite: it's the cuts that could be on the opposite side
        @type opposite: string
        @param unprotcomposition: a composition in atoms
        @type unprotcomposition: dict(char: int)
        """
        self.link = name
        self.unprot = unprotcomposition
        self.complementary = complementary
        self.opposite = opposite
    
    def get_delta(self):
        """ get the difference between the composition and the
        unprot composition 
        @return: the difference between the composition and
        the unprot composition
        @rtype: dict(str : int)"""
        ddelta = dif_compo(self.composition, self.unprot)
        resdict = {}
        for key in ddelta:
            if ddelta[key] != 0:
                resdict[key] = ddelta[key]
        return resdict
        

class MSingleNeutralLoss(ChimicalElement):
    """this class is used to represent a neutral loss (H2O,
    etc ...
    @ivar neutral_loss: the type of neutral loss (H2O ...)
    @type neutral_loss: string
    @ivar composition: a composition in atoms
    @type composition: dict(char: int)
    @ivar aa: the aa where the neutral loss could appear
    @type aa: char
    @ivar ptm: the ptm where the neutral loss could appear
    @type ptm: string
    ivar side: the side where the neutral loss could appear 
    (if center everywhere except c-term and n-term)
    @type side: string
    """
    
    def __init__(self, name, composition, aa, ptm, side):
        """the init lauch a rule to create the neutral loss
        @param name: the name of the neutral loss
        @type name: string
        @param composition: a composition in atoms
        @type composition: dict(char: int)
        @param aa: the aa where the neutral loss could appear
        @type aa: char
        @param ptm: the ptm where the neutral loss could appear
        @type ptm: string
        ivar side: the side where the neutral loss could appear 
        (if center everywhere except c-term and n-term)
        @type side: string"""
        ChimicalElement.__init__(self, composition)
        self.rule_create_residue(name, aa, ptm, side)
    
    def rule_create_residue(self, name, aa, ptm, side):
        """the rule to create the neutral loss
        @param name: the name of the residue
        @type name: string
        @param aa: the aa where the neutral loss could appear
        @type aa: char
        @param ptm: the ptm where the neutral loss could appear
        @type ptm: string
        ivar side: the side where the neutral loss could appear
        (if center everywhere except c-term and n-term)
        @type side: string"""
        self.neutral_loss = name
        self.aa = aa
        self.ptm = ptm
        self.side = side  
    
