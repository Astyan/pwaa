#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_mcombielemscreate.py unittest for creations rules in 
mcombielemscreate.py
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
#~ from copy import deepcopy
#~ from sets import Set
import sys

from pwaa.basicknowledge.mcombielemscreate import (reason_all_doublelinks,
                                                   reason_all_aaptm,
                                                   reason_all_aaptmresidue,
                                                   reason_all_aaptmnl,
                                                   reason_all_aaptmnl_dlink,
                                                   reason_all_aaptm_dlink,
                                                   ReasonAllDLINKSequence,
                                                   ReasonAllMasses)


from pwaa.basicknowledge.mcombielems import (MSingleAAPTM, 
                                             MSingleAAPTMResidue,
                                             #~ MSingleAAPTMNLResidue,
                                             MSingleAAPTMNL)


from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_residue,
                                              fill_all_link,
                                              fill_all_neutral_loss)

class TestMCombiElemsCreate(unittest.TestCase):
    """test of all reason in mcombielemscreate"""
    
    @classmethod
    def setUpClass(cls):
        """ to run only once """
        cls.l_all_aa = fill_all_aa()
        cls.l_all_ptm = fill_all_ptm()
        cls.l_all_link = fill_all_link()
        cls.l_all_nl = fill_all_neutral_loss()
        cls.l_all_residue = fill_all_residue()
        cls.l_all_dlinks = reason_all_doublelinks(cls.l_all_link)
        cls.l_all_aaptm = reason_all_aaptm(cls.l_all_aa, cls.l_all_ptm) 
        cls.l_all_aaptmnl = reason_all_aaptmnl(cls.l_all_aaptm, 
                                               cls.l_all_nl)
        cls.l_all_aaptmresidue = reason_all_aaptmresidue(cls.l_all_aaptm, 
                                                         cls.l_all_residue)
        cls.l_allaaptmdlinks = reason_all_aaptm_dlink(cls.l_all_aaptm,
                                                      cls.l_all_dlinks)
        cls.l_all_aaptmnldlinks = reason_all_aaptmnl_dlink(cls.l_all_aaptmnl,
                                                           cls.l_all_dlinks)
        
    
    def test_10_reason_all_doublelinks(self):
        """ test of reason_all_doublelinks"""
        self.assertEqual(len(self.l_all_dlinks), 32)
    
    def test_10_reason_all_aaptm(self):
        """ test of reason_all_aaptm """
        self.assertEqual(len(self.l_all_aaptm), len(self.l_all_ptm)) #449
        self.assertEqual(self.l_all_aaptm[1].aa, "A")
        self.assertEqual(self.l_all_aaptm[1].ptm, "Amidation")
        self.assertEqual(self.l_all_aaptm[1].composition, {u'C': 2, 
                                                           u'H': 5,
                                                           u'O': -1, 
                                                           u'N': 1, 
                                                           u'P': 0, 
                                                           u'As': 0,
                                                           u'S': 0, 
                                                           u'Se': 0})
        self.assertEqual(self.l_all_aaptm[1].position, "c-term")
        for elem in self.l_all_aaptm:
            self.assertIsInstance(elem, MSingleAAPTM) 
    
    def test_11_reason_all_aaptmnl(self):
        """ test of reason_all_aaptmnl """
        self.assertEqual(len(self.l_all_aaptmnl), 
                         len(self.l_all_ptm) + len(self.l_all_nl)) 
                         #449 + 69 = 518
        for elem in self.l_all_aaptmnl:
            self.assertIsInstance(elem, MSingleAAPTMNL)
    
    def test_11_reason_all_aaptm_dlinks(self):
        """ test of reason_all_aaptm_dlinks """
        #test NOT SURE
        self.assertEqual(len(self.l_allaaptmdlinks), 4706)
        
    def test_12_reason_all_aaptmresidue(self):
        """test of all reason_all_aaptmresidue"""
        for elem in self.l_all_aaptmresidue:
            self.assertIsInstance(elem, MSingleAAPTMResidue) 
        
    def test_13_reason_all_aaptmnl_dlink(self):
        """ test of reason_all_aaptmnl_dlink """
        #test NOT SURE
        self.assertEqual(len(self.l_all_aaptmnldlinks), 5488)
        
class TestReasonAllDLINKSequence(unittest.TestCase):
    """ tests for ReasonAllDLINKSequence """
    
    @classmethod
    def setUpClass(cls):
        """ init of the class, run only once"""
        cls.l_all_aa = fill_all_aa()
        cls.l_all_ptm = fill_all_ptm()
        cls.l_all_nl = fill_all_neutral_loss()
        cls.l_all_link = fill_all_link()
        cls.l_all_aaptm = reason_all_aaptm(cls.l_all_aa, cls.l_all_ptm)
        cls.l_all_aaptmnl = reason_all_aaptmnl(cls.l_all_aaptm, 
                                               cls.l_all_nl) 
        cls.l_all_dlinks = reason_all_doublelinks(cls.l_all_link)
        cls.dlinksequence = ReasonAllDLINKSequence(cls.l_all_aaptmnl, 
                                                   cls.l_all_dlinks, 2)
        cls.dlinksequence.reason_all_sequence()
        
    def test_10_sequences(self):
        """ test if the generated sequences are good """
        self.assertEqual(len(self.dlinksequence.seqs[1]), 3963)
        self.assertEqual(len(self.dlinksequence.seqs[2]), 398625)
                                   
            

class TestReasonAllMasses(unittest.TestCase):
    """ tests of the creation of masses """
    
    @classmethod
    def setUpClass(cls):
        """ setup of the class to do only once """
        cls.l_all_aa = fill_all_aa()
        cls.l_all_ptm = fill_all_ptm()
        cls.l_all_nl = fill_all_neutral_loss()
        cls.l_all_link = fill_all_link()
        cls.l_all_aaptm = reason_all_aaptm(cls.l_all_aa, cls.l_all_ptm)
        cls.l_all_aaptmnl = reason_all_aaptmnl(cls.l_all_aaptm, 
                                               cls.l_all_nl)
        cls.all_masses = ReasonAllMasses(3, cls.l_all_aaptmnl, 3)
    
    def test_10_set_base(self):
        """ test of set_base """
        self.all_masses.set_base()
        self.assertEqual(len(self.all_masses.base_masses["center"]), 101)
        self.assertEqual(len(self.all_masses.base_masses["n-term"]),
                         203)
        self.assertEqual(len(self.all_masses.base_masses["c-term"]),
                         121)

    def test_11_mass1(self):
        """ test of mass1 """
        r1, r2, r3, r4 = self.all_masses.mass1()
        self.assertEqual(len(r1), 
                         len(self.all_masses.base_masses["center"]))
        self.assertEqual(len(r2),
                         len(self.all_masses.base_masses["c-term"]))
        self.assertEqual(len(r3),
                         len(self.all_masses.base_masses["n-term"]))
        self.assertEqual(len(r4), 0)    

    def test_12_new_center(self):
        """ test of new_center """
        res = self.all_masses.new_center([400000])
        self.assertEqual(len(self.all_masses.base_masses["center"]),
                         len(res))


    def test_13_set_all_masses(self):
        """ test of set_all_masses"""
        self.all_masses.set_all_masses()

if __name__ == '__main__':
    unittest.main()
