#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_melems.py unittest for models in melems.py 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys
sys.path.append('../')

from basicknowledge.melems import (ChimicalElement, 
                                   MSingleAA, MSinglePTM, 
                                   MSingleResidue, MSingleLink,
                                   MSingleNeutralLoss) 

class TestChimicalElement(unittest.TestCase):
    """ class used to test the creation of a ChimicalElement"""
    
    def test_10_create_chimicalelement(self):
        """ test of the creation of a chimicalelement """
        res = ChimicalElement({"H": 1, "N": 2})
        self.assertEqual(res.composition, {"H": 1, "N": 2})

class TestMSingleAA(unittest.TestCase):
    """class use to test the creation of a MSingleAA"""
    
    def test_10_rule_create_aa(self):
        """test the rule to create an AA"""
        res = MSingleAA("A", {'H':1, 'N':2})
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.composition, {'H':1, 'N':2})
        res2 = ChimicalElement({"H": 1, "N": 2})
        #~ print res2 != res
        
class TestMSinglePTM(unittest.TestCase):
    """this class is used to test the creation of a MSinglePTM"""
    
    def test_10_rule_create_ptm(self):
        """test the rule to create a PTM"""
        res = MSinglePTM("test", {'H':1, 'N':2}, "center", "A")
        self.assertEqual(res.ptm, "test")
        self.assertEqual(res.composition, {'H':1, 'N':2})
        self.assertEqual(res.position, "center")
        self.assertEqual(res.posaa, "A")

class TestMSingleResidue(unittest.TestCase):
    """this class is used to test the creation of a MSingleResidue"""
    
    def test_10_rule_create_residue(self):
        """test the rule to create residue"""
        res = MSingleResidue("test", {'H':1, 'N':2})
        self.assertEqual(res.residue, "test")
        self.assertEqual(res.composition, {'H':1, 'N':2})

class TestMSingleLink(unittest.TestCase):
    """this class is used to test the creation a MSingleLink"""
    
    def test_10_rule_create_link(self):
        """test the rule to create a link"""
        res = MSingleLink("test", {'H':1, 'N':2}, "test2", "test2", 
                          {'N' :2})
        self.assertEqual(res.link, "test")
        self.assertEqual(res.composition, {'H':1, 'N':2})
        self.assertEqual(res.complementary, "test2")
        self.assertEqual(res.opposite, "test2")
        self.assertEqual(res.get_delta(), {'H':1})

class TestMSingleNeutralLoss(unittest.TestCase):
    """ this class is used to test the creation of a 
    TestMSingleNeutralLoss"""
    
    def test_10_rule_create_neutral(self):
        "test the rule to create a neutral loss"
        res = MSingleNeutralLoss("test", {'H':2, 'O':1}, "A", "test2",
                                 "center")
        self.assertEqual(res.neutral_loss, "test")
        self.assertEqual(res.composition, {'H':2, 'O':1})
        self.assertEqual(res.aa, "A")
        self.assertEqual(res.ptm, "test2")
        self.assertEqual(res.side, "center")
        

if __name__ == '__main__':
    unittest.main()
