#!/usr/bin/env python
# encoding: utf-8
"""mcombielemscreate.py contain functions to create objects in 
mcombieelems.py
@author: Vezin Aurelien
@license: CECILL-B"""

import sys
import copy
from sets import Set

from pwaa.basicknowledge.melems import MSingleLink
from pwaa.basicknowledge.mcombielems import (MDoubleLinks,
                                             MSingleAAPTM, 
                                             MSingleAAPTMResidue,
                                             MSingleAAResidue,
                                             MSingleAAPTMNL,
                                             MAAPTMDLinks,
                                             MAAPTMNLDLinks,
                                             SequenceDlink)
#~ import database.vaadb as vaadb
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.masses import int_round
from pwaa.utils.tree import RecTree


def reason_all_doublelinks(l_msingle_link):
    """Create all double links from a list of sigle links
    @param l_msingle_link: a list of MSingleLink
    @type l_msingle_link: list(MSingleLink)
    @return: all double links generated
    @rtype: list(MDoubleLinks)
    """
    l_comp = copy.deepcopy(l_msingle_link)
    #adding links to have c-term and n-term on the join side
    l_comp.append(MSingleLink("None", {}, "n-term", "n-term", {}))
    l_comp.append(MSingleLink("None", {}, "c-term", "c-term", {}))
    l_double = []
    for lcut in l_msingle_link:
        for lcomp in l_comp:
            for ljoin in l_msingle_link:
                if ljoin.opposite == lcut.link and lcut.opposite == ljoin.link:
                    if (ljoin.complementary == lcomp.link or 
                            lcomp.link == "None"):
                        if lcomp.opposite == lcut.opposite:
                            to_add = MDoubleLinks(ljoin, lcomp, lcut)
                            l_double.append(to_add)
    return l_double


def reason_all_aaptm(l_msingle_aa, l_msingle_ptm):
    """ Create all MSingleAAPTM class possible from a list of
    MSingleAA and a list of MSinglePTM
    @param l_msingle_aa: a list of MSingleAA
    @type l_msingle_aa: MSingleAA
    @param l_msingle_ptm: a list of MSinglePTM
    @type l_msingle_ptm: MSinglePTM
    @return: a list of MSingleAAPTM
    @rtype: list(MSingleAAPTM)
    """
    lmsaaptm = []
    for msingle_aa in l_msingle_aa:
        for msingle_ptm in l_msingle_ptm:
            if msingle_aa.aa == msingle_ptm.posaa:
                lmsaaptm.append(MSingleAAPTM(msingle_aa, msingle_ptm))
    return lmsaaptm

def reason_all_aaptmnl(l_msingle_aaptm, l_msingle_nl):
    """ Create all MSingleAAPTMNL class possible from a list of
    MSingleAAPTM and a list of MSingleNL
    @param l_msingle_aaptm: a list of MSingleAAPTM
    @type l_msingle_aaptm: MSingleAAPTM
    @param l_msingle_nl: a list of MSingleNL
    @type l_msingle_nl: MSingleNL
    @return: a list of MSingleAAPTMNL
    @rtype: list(MSingleAAPTMNL)"""
    lmsaaptmnl = []
    for msingle_aaptm in l_msingle_aaptm:
        lmsaaptmnl.append(MSingleAAPTMNL(msingle_aaptm, None))
        for msingle_nl in l_msingle_nl:
            if (msingle_aaptm.aa == msingle_nl.aa and
                    msingle_aaptm.ptm == msingle_nl.ptm and
                    msingle_aaptm.position == msingle_nl.side):
                lmsaaptmnl.append(MSingleAAPTMNL(msingle_aaptm,
                                                 msingle_nl))
    return lmsaaptmnl
        

def reason_all_aaptmresidue(l_ms_aaptm, l_ms_residue):
    """ create all aaptmresidue from a  list of MSingleAAPTM and
    a list of MSingleResidue
    @param l_ms_aaptm: a list of MSingleAAPTM
    @type l_ms_aaptm: list(MSingleAAPTM)
    @param l_ms_residue: a list of MSingleResidue
    @type l_ms_residue: list(MSingleResidue)"""
    lms_aaptmresidue = []
    for ms_aaptm in l_ms_aaptm:
        for ms_residue in l_ms_residue:
            lms_aaptmresidue.append(MSingleAAPTMResidue(ms_aaptm, 
                                                        ms_residue))
    return lms_aaptmresidue
    
def reason_all_aaresidue(l_ms_aa, l_ms_residue):
    """ create all aaptmresidue from a  list of MSingleAA and
    a list of MSingleResidue
    @param l_ms_aa: a list of MSingleAA
    @type l_ms_aa: list(MSingleAA)
    @param l_ms_residue: a list of MSingleResidue
    @type l_ms_residue: list(MSingleResidue)"""
    lms_aaresidue = []
    for ms_aa in l_ms_aa:
        for ms_residue in l_ms_residue:
            lms_aaresidue.append(MSingleAAResidue(ms_aa,ms_residue))
    return lms_aaresidue
    


#~ def reason_all_aaptmnlresidue(l_ms_aaptmnl, l_ms_residue, l_ms_link):
    #~ """ create all aaptmnlresidue from complete list of MSingleAAPTMNL,
    #~ MSingleResidue, MSingleLink
    #~ @param l_ms_aaptmnl: a list of MSingleAAPTM
    #~ @type l_ms_aaptmnl: list(MSingleAAPTM)
    #~ @param l_ms_residue: a list of MSingleResidue
    #~ @type l_ms_residue: list(MSingleResidue)
    #~ @param l_ms_link: a list of MSingleLink
    #~ @type l_ms_link: list(MSingleLink)
    #~ @return: the result list ofMSingleAAPTMResidue
    #~ @rtype: list(MSingleAAPTMResidue)
    #~ """
    #~ lms_aaptmresidue = []
    #~ center = MSingleLink("center", {}, "center", "center", {})
    #~ for aaptmnl in l_ms_aaptmnl:
        #~ for residue in l_ms_residue:
            #~ for link in l_ms_link:
                #~ #if the position is n-term or c-term add the atoms
                #~ #of the given postion
                #~ if (aaptmnl.aa == residue.aa and 
                        #~ aaptmnl.ptm == residue.ptm and 
                        #~ aaptmnl.position == residue.side and
                        #~ aaptmnl.position == link.link):
                    #~ lms_aaptmresidue.append(
                        #~ MSingleAAPTMNLResidue(aaptmnl, residue, link))
                    #~ break
                #~ #if the position if center add no atoms    
                #~ if (aaptmnl.aa == residue.aa 
                        #~ and aaptmnl.ptm == residue.ptm
                        #~ and aaptmnl.position == residue.side
                        #~ and aaptmnl.position == "center"):
                    #~ lms_aaptmresidue.append(MSingleAAPTMNLResidue(aaptmnl,
                                                                  #~ residue,
                                                                  #~ center))
                    #~ break
    #~ return lms_aaptmresidue


def reason_all_aaptmnl_dlink(lms_aaptmnl, l_doublelinks):
    """ create all aaptmnl with double link
    @param lms_aaptmnl: a list of aaptmnl
    @type lms_aaptmnl: list(MSingleAAPTMNL)
    @param l_doublelinks: a list of double links
    @type l_doublelinks: MDoubleLinks
    @return: a list aaptmnl with doublelinks
    @rtype: list(MAAPTMNLDLinks) 
    """
    l_res = []
    for aaptmnl in lms_aaptmnl:
        for dlink in l_doublelinks:
            if aaptmnl.position == "c-term":
                if (dlink.joinside == "c-term" or 
                        dlink.cutside == "c-term"):
                    l_res.append(MAAPTMNLDLinks(aaptmnl, dlink)) 
            if aaptmnl.position == "n-term":
                if (dlink.joinside == "n-term" or
                        dlink.cutside == "n-term"):
                    l_res.append(MAAPTMNLDLinks(aaptmnl, dlink))
            if aaptmnl.position == "center": 
                #check if only center because for an aaptmnl
                #of each side we find it in triple with
                #one on n-term, one on c-term, one on center
                if (dlink.joinside != "n-term" and 
                        dlink.cutside != "n-term" and
                        dlink.joinside != "c-term" and
                        dlink.cutside != "c-term"):
                    l_res.append(MAAPTMNLDLinks(aaptmnl, dlink))
                    
    return l_res
    
def reason_all_aaptm_dlink(lms_aaptm, l_doublelinks):
    """ create all aaptm with double link
    @param lms_aaptm: a list of aaptm
    @type lms_aaptm: list(MSingleAAPTM)
    @param l_doublelinks: a list of double links
    @type l_doublelinks: MDoubleLinks
    @return: a list aaptmnl with doublelinks
    @rtype: list(MAAPTMDLinks) 
    """
    l_res = []
    for aaptm in lms_aaptm:
        for dlink in l_doublelinks:
            if aaptm.position == "c-term":
                if (dlink.joinside == "c-term" or 
                        dlink.cutside == "c-term"):
                    l_res.append(MAAPTMDLinks(aaptm, dlink)) 
            if aaptm.position == "n-term":
                if (dlink.joinside == "n-term" or
                        dlink.cutside == "n-term"):
                    l_res.append(MAAPTMDLinks(aaptm, dlink))
            if aaptm.position == "center": 
                #check if only center because for an aaptmnl
                #of each side we find it in triple with
                #one on n-term, one on c-term, one on center
                if (dlink.joinside != "n-term" and 
                        dlink.cutside != "n-term" and
                        dlink.joinside != "c-term" and
                        dlink.cutside != "c-term"):
                    l_res.append(MAAPTMDLinks(aaptm, dlink))
    return l_res
    
    
class ReasonAllDLINKSequence(object):
    """ Reason all sequence with dlinks 
    a sequence with dlinks have a link on the cut side and
    one on the join side
    @ivar seqs: a dict of list of sequence for each sequence size
    @type seqs: dict(int : list(list(SequenceDlink)))
    @ivar l_aaptmsnls: a dict of list of all aaptmnl possible
    for each size
    @type l_aaptmsnls: dict(int : list(list(MSingleAAPTMNL)))
    @ivar cursize: the sequence size we are creating
    @type cursize: int
    @ivar links: a list of MDoubleLink
    @type links: list(MDoubleLink)
    """
    
    def __init__(self, lms_aaptmnl, l_d_links, seqsize):
        """ create the dict of all aaptmnl list of the max size seqsize
        and init other instance var
        @param lms_aaptmnl: list of aaptmnl
        @type lms_aaptmnl: list(MSingleAAPTMNL)
        @param l_d_links: list of double links
        @type l_d_links: list(MDoubleLink)
        @param seqsize: the max size we want for the sequences
        @type seqsize: int
         """
        rtree = RecTree(None, lms_aaptmnl, None, seqsize)
        self.seqs = {}
        self.l_aaptmsnls = rtree.dict_len()
        self.cursize = 0
        self.links = l_d_links
    
    
    def aaptmnls_valid(self, aaptmnls):
        """ check if the aaptmnl list is valid to do a sequence
        (max one aaptmnl on c_term and one aaptmnl on n-term)
        @param aaptmnls: a list of aaptmnl
        @type aaptmnls: list(MSingleAAPTMNL)
        @return: False if it's not valid. 
        The number of aaptmnls at cterm and at nterm if valid (0 or 1)
        @rtype: False or (int, int)
        """
        nterm = 0
        cterm = 0
        for aaptmnl in aaptmnls:
            if aaptmnl.position != "center":
                if aaptmnl.position == "n-term":
                    nterm += 1
                elif aaptmnl.position == "c-term":
                    cterm += 1
            if cterm > 1 or nterm > 1:
                return False
        return (nterm, cterm)
    
    def link_compatible(self, nterm, cterm, join, cut):
        """ check if a aaptmnl list is compatible with a dlink
        @param nterm: the number of aaptmnl going to nterm (0 or 1)
        @type nterm: int
        @param cterm: the number of aaptmnl going to cterm (0 or 1)
        @type cterm: int
        @param join: the join link string identifier of the doublelink
        @type join: str
        @param cut: the cit link string identifier of the doublelink
        @type cut: str
        @return: True if compatible. False if not
        @rtype: Bool"""
        if cterm == 1:
            if join != "c-term" and cut != "c-term":
                return False
        if nterm == 1:
            if join != "n-term" and cut != "b-term":
                return False
        if join == "c-term" or cut == "c-term":
            if cterm != 1:
                return False
        if join == "n-term" or cut == "n-term":
            if nterm != 1:
                return False
        return True
            
    def reason_all_sequence(self):
        """ Create all possible sequence from the instance variable"""
        for key in self.l_aaptmsnls:
            for aaptmnls in self.l_aaptmsnls[key]:
                valid = self.aaptmnls_valid(aaptmnls)
                if valid != False:
                    for link in self.links:
                        if self.link_compatible(valid[0], valid[1],
                                                link.joinside,
                                                link.cutside):
                            seql = self.seqs.get(key, [])
                            seql.append(SequenceDlink(aaptmnls, link))
                            self.seqs[key] = seql

class ReasonAllMasses(object):
    """ Can create all mass until a size given
    and an approximation (for round) and a list of aaptmnl.
    It's approximate to reduce combinatory complexity 
    like some masses will be the same.
    @ivar size: the maximal size of the sequence mass compute
    @type size: int
    @ivar approx: the number of number after comma for the masses
    @type approx: int
    @ivar aaptmnl: a list of aaptmnl. Could be something else like
    with a composition
    @type aaptmnl: list(MSingleAAPTMNL)
    @ivar link: the mass of the link between 2 aaptmnl
    @type link: float
    @ivar base_masses: the masses of aaptmnl in a dict with
    position (c-term, n-term, center) as key
    @type base_masses: dict(str:list(float))
    """
    
    def __init__(self, size, aaptmnl, approx, atoms=None):
        """ Set the param in instance variable and init other 
        instance variable
        @param size: the maximal size of the sequence we want
        @type size: int
        @param aaptmnl: a list of aaptmnl
        @type aaptmnl: list(MSingleAAPTMNL)
        @param approx: the number of number after comma for each masse
        @type approx: int"""
        self.mamo = MAtomsMassOp(atoms)
        self.size = size
        self.approx = approx
        self.aaptmnl = aaptmnl
        #link between amino acids
        linkmass = self.mamo.atomlist_to_masses({'H' : 1, 'C' : 1, 
                                                 'N' : 1, 'O' : 1},
                                                "mono")
        linkmass = int_round(linkmass, self.approx)
        self.link = linkmass
        self.base_masses = dict()
        
    def set_base(self):
        """ Set the base mass frim the aaptmnl"""
        self.base_masses["c-term"] = Set([])
        self.base_masses["n-term"] = Set([])
        self.base_masses["center"] = Set([])
        for elem in self.aaptmnl:
            mass = self.mamo.atomlist_to_masses(elem.composition,
                                                "mono")
            mass = int_round(mass, self.approx)
            if elem.position == "center":
                self.base_masses["center"].add(mass)
            elif elem.position == "n-term":
                self.base_masses["n-term"].add(mass)
            else:
                self.base_masses["c-term"].add(mass)
        
    def set_all_masses(self):
        """ Compute all mass until size
        @return: return the masses in 4 dict representing the 
        aaptmnl sequence at center, cterm, nterm, cterm_to_nterm.
        Theses dict have for key the len of the sequence
        @rtype: (dict(int:list(int)),dict(int:list(int)),
        dict(int:list(int)), dict(int:list(int)))"""
        centerdict = dict()
        ctermdict = dict()
        ntermdict = dict()
        cntermdict = dict()
        for i in range(1, self.size + 1):
            if i == 1:
                (mid, ct, nt, cnt) = self.mass1()
                centerdict[1] = mid
                ctermdict[1] = ct
                ntermdict[1] = nt
                cntermdict[1] = cnt
            else:
                (mid, ct, nt, cnt) = self.next_mass(ctermdict[i-1],
                                                    ntermdict[i-1],
                                                    centerdict[i-1],
                                                    cntermdict[i-1])
                centerdict[i] = mid
                ctermdict[i] = ct
                ntermdict[i] = nt
                cntermdict[i] = cnt
        return (centerdict, ctermdict, ntermdict, cntermdict)
                
                
            
    def mass1(self):
        """ Compute masses of sequences of size 1
        @return: 4 dict with aaptmnl at center, c-term, n-term,
        c-term-to-nterm(empty)
        @rtype: (Set([int]), Set([int]), Set([int]), 
        Set([int]))
        """
        newcenter = self.base_masses["center"]
        newcterm = self.base_masses["c-term"]
        newnterm = self.base_masses["n-term"]
        newcnterm = Set([])
        return (newcenter, newcterm, newnterm, newcnterm)
    

    def next_mass(self, cterm, nterm, center, cnterm):
        """Compute all new masses with aatptmnl at center,
        cterm, nterm or cterm and nterm
        @param cterm: the previous masses knowed at cterm
        @type cterm: list(int)
        @param nterm: the previous masses knowed at nterm
        @type nterm: list(int)
        @param center: the previous masses knowed at center
        @type center: list(int)
        @param cnterm: the previous masses knowed at nterm and cterm
        @type cnterm: list(int)
        @return: the masses knowed at center, c-term, n-term and 
        c-term to n-term for a sequence of a length =
        lenght + 1 of the previous sequence
        @rtype: (list(int), list(int), list(int), list(int))
        """
        newcenter = self.new_center(center)
        newcterm = self.new_nterm_or_cterm(center, cterm, "c-term")
        newnterm = self.new_nterm_or_cterm(center, nterm, "n-term")
        newcnterm = self.new_cnterm(cnterm, cterm, nterm)
        return (newcenter, newcterm, newnterm, newcnterm)
        
    
    def new_center(self, center):
        """Compute all the masses at the center for the next lenght
        @param center: List of masses at the previous lenght of the 
        sequence for the center
        @type center: list(int)
        @return: the list of masses of sequences of next lenght
        at center
        @rtype: list(int) """
        masses = Set([])
        for mass1 in center:
            for mass2 in self.base_masses["center"]:
                masses.add(mass1 + mass2 + self.link)
        return masses
    
    def new_nterm_or_cterm(self, center, c_or_nterm, term):
        """Compute all masses at nterm or cterm for the next lenght
        @param center: List of masses at the previous lenght of the 
        sequence for the center
        @type center: list(int)
        @param c_or_nterm: List of masses at the previous lenght of the 
        sequence for the cterm or nterm
        @type c_or_nterm: list(int) 
        @param term: the side where are the sequences ("c-term" or
        "n-term")
        @type term: str
        @return: the list of masses of sequences of next lenght
        at cterm or nterm
        @rtype: list(int)"""
        masses = Set([])
        for mass1 in center:
            for mass2 in self.base_masses[term]:
                masses.add(mass1 + mass2 + self.link)
        for mass1 in c_or_nterm:
            for mass2 in self.base_masses["center"]:
                masses.add(mass1 + mass2 + self.link)
        return masses
    
    def new_cnterm(self, cnterm, cterm, nterm):
        """Compute all masses from cterm to nterm for the next lenght
        @param cnterm: List of masses at the previous lenght of the 
        sequence from nterm to cterm
        @type cnterm: list(int)
        @param cterm: List of masses at the previous lenght of the 
        sequence for the cterm
        @type cterm: list(int)
        @param nterm: List of masses at the previous lenght of the 
        sequence for the nterm
        @type nterm: list(int)
        @return: the list of masses of sequences of next lenght
        at cterm or nterm
        @rtype: list(float)"""
        masses = Set([])
        for mass1 in cnterm:
            for mass2 in self.base_masses["center"]:
                masses.add(mass1 + mass2 + self.link)
        for mass1 in cterm:
            for mass2 in self.base_masses["n-term"]:
                masses.add(mass1 + mass2 + self.link)
        for mass1 in nterm:
            for mass2 in self.base_masses["c-term"]:
                masses.add(mass1 + mass2 + self.link)
        return masses

if "__main__" == __name__:
    pass
    #~ l_all_aa = fill_all_aa()
    #~ l_all_ptm = fill_all_ptm()
    #~ l_all_nl = fill_all_neutral_loss()
    #~ l_all_link = fill_all_link()
    #~ print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #~ l_all_aaptm = reason_all_aaptm(l_all_aa, l_all_ptm)
    #~ l_all_aaptmnl = reason_all_aaptmnl(l_all_aaptm, l_all_nl)
    #~ l_all_aaptmnllink = reason_all_aaptmnllinks(l_all_aaptmnl,
                                                #~ l_all_link)
    #~ print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #~ allsequences = ReasonAllSequences(3, l_all_aaptmnllink)
    #~ print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
