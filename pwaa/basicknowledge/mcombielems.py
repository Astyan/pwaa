#!/usr/bin/env python
# encoding: utf-8
"""mcombielems.py is a model to combinated elems find in melems
@author: Vezin Aurelien
@license: CECILL-B"""

import sys


from pwaa.utils.masses import combine_compo, dif_compo, mult_compo
from pwaa.basicknowledge.melems import (ChimicalElement,
                                        MSingleAA, MSinglePTM,
                                        MSingleResidue, MSingleLink,
                                        MSingleNeutralLoss)


class MDoubleLinks(ChimicalElement):
    """ class to create a link combinaison
    it can only stick with a complement 
    It's used to represent 2 links composition for a loss of a 
    sequence.
    For exemple:
    n-term ...... y-cut[b-cut .... c-cut] 
    n-term ...... y-cut 
    mass differency [b-cut .... c-cut]
    Like we have protonated and unprotonated cut the mass to
    add to a sequence is conditionned by the complementary of cut 
    of the join side (here y-cut)
    @ivar joinside: the cut link name to the complementary (here b-cut)
    @type joinside: str
    @ivar cutside: the link name left were there was the previous cut
    @type cutside: str """
    
    def __init__(self, linkjoin, linkjoincomplement, linkcut):
        """check if the params have the good type and good values
        then launch the creation rule 
        @param linkjoin: the cut link to the complementary 
        @type linkjoin: MSingleLink
        @param linkjoincomplement: the complement link of linkjoin.
        if the joinside is a term then None
        @type linkjoincomplement: MSingleLink|None
        @param linkcut: the link left were there was the previous cut
        @type linkcut: MSingleLink """
        if(not isinstance(linkjoin, MSingleLink) or 
           not isinstance(linkcut, MSingleLink) or
           not isinstance(linkjoincomplement, MSingleLink)):
            raise TypeError("linkjoin and linkcut must be MSingleLink")
        self.rule_create_double_link(linkjoin, linkjoincomplement, 
                                     linkcut)
        
    def rule_create_double_link(self, ljoin, ljoincomple, lcut):
        """ save the name of each link and do the computation of a
        compo of links to give the real mass
        the compo to give the real mass is the
        compo of lcut (protonated) added to 
        the compo of ljoin non protonated minus the difference
        between the protenated complement and the non protenated 
        complement. (to do a real link of 1H, 1C, 1O, 1N 
        @param ljoin: the cut link to the complementary 
        @type ljoin: MSingleLink
        @param ljoincomple: the complement link of linkjoin.
        if the joinside is a term then None
        @type ljoincomple: MSingleLink|None
        @param lcut: the link left were there was the previous cut
        @type lcut: MSingleLink"""
        self.joinside = ljoin.link
        self.cutside = lcut.link
        self.lcomplement = ljoincomple.link
        compobase = combine_compo(ljoin.unprot, lcut.unprot)
        self.composition = {'H' : -100, 'C' : -100, 'O' : -100, 'N' : -100}
        #case if we start per a term
        if ljoin.link == "n-term" or ljoin.link == "c-term":
            #untested
            #~ pass
            deltacut = dif_compo(lcut.composition, lcut.unprot)
            # if ljoin.link == "c-term":
                # compobase = combine_compo(compobase, {'H': 2})
            if deltacut.get('H', 0) != 0:
                self.composition = combine_compo(compobase, {'H': 2})
            else:
                self.composition = compobase
        #case if we finish per a term
        elif lcut.link == "n-term" or lcut.link == "c-term":
            #untested
            #~ pass
            deltacomplement = dif_compo(ljoincomple.composition, ljoincomple.unprot)
            # if lcut.link == "c-term":
                # compobase = combine_compo(compobase, {'H': 2})
            if deltacomplement.get('H', 0) != 0:
                self.composition = dif_compo(compobase, {'H': 2})
            else: 
                self.composition = compobase
        #case in the middle
        else:
            pass
            deltacut = dif_compo(lcut.composition, lcut.unprot)
            deltacomplement = dif_compo(ljoincomple.composition, ljoincomple.unprot)
            deltaglob = dif_compo(deltacomplement, deltacut)
            if deltaglob.get('H', 0) != 0:
                if deltacut.get('H', 0) != 0:
                    self.composition = combine_compo(compobase, {'H' :2})
                elif deltacomplement.get('H', 0) != 0:
                    self.composition = dif_compo(compobase, {'H' :2}) 
            else:
                self.composition = compobase
            
        
        
    def __hash__(self):
        """the hash for the set
        @return: the hash of the str of different parameter of the 
        doublelink"""
        sentence = self.joinside+self.cutside+self.lcomplement
        return hash(sentence)
    

class MSingleAAPTM(ChimicalElement):
    """this class is used to represent one AA linked to a PTM or not
    @ivar aa: the keychar of the amino acid
    @type aa: char
    @ivar ptm: the name of the ptm
    @type ptm: string
    @ivar composition: the composition of the ptm
    @type composition: {char: int}
    @ivar position: the position of the amino acid needed to have the 
    ptm (n-term, c-term, any)
    @type position: string
    """
    
    def __init__(self, msingle_aa, msingle_ptm):
        """Test if the 2 arguments are of the good type
        and if they are compatible then lauch the rule
        to create a amino acid with a ptm (the ptm can be None)
        @param msingle_aa: a MSingleAA class
        @type msingle_aa: MSingleAA
        @param msingle_ptm: a MSinglePTM class
        @type msingle_ptm: MSinglePTM
        @raise TypeError: if one of the argument is not of 
        the good class
        @raise ValueError: if the aa doesn't match with the possible aa
        for in ptm
        """
        if (not isinstance(msingle_ptm, MSinglePTM) 
                or not isinstance(msingle_aa, MSingleAA)):
            raise TypeError
        if msingle_aa.aa != msingle_ptm.posaa:
            raise ValueError
        self.rule_create_aaptm(msingle_aa, msingle_ptm)
            
    def rule_create_aaptm(self, msingle_aa, msingle_ptm):
        """Create the AAPTM
        @param msingle_aa: a MSingleAA class
        @type msingle_aa: MSingleAA
        @param msingle_ptm: a MSinglePTM class
        @type msingle_ptm: MSinglePTM
        """
        self.aa = msingle_aa.aa
        self.ptm = msingle_ptm.ptm
        self.composition = combine_compo(msingle_aa.composition,
                                         msingle_ptm.composition)
        self.position = msingle_ptm.position
    
    def __eq__(self, other):
        """check if the element is equal to an other element
        (key check) 
        @param other: the other element 
        @type other: MSingleAAPTM
        @return: True if equal, False if not
        @rtype: Boolean  """
        return (isinstance(self, MSingleAAPTM) and
                (isinstance(other, MSingleAAPTM) or 
                 isinstance(self, MSingleAAPTMResidue) and
                self.aa == other.aa and self.ptm == other.ptm))
    
    def __ne__(self, other):
        """check if the element is equal to an other element
        (key check) 
        @param other: the other element 
        @type other: MSingleAAPTM
        @return: False if equal, True if not
        @rtype: Boolean  """
        return not self.__eq__(other)
        
    def __hash__(self):
        """ give the hash of the str of the keys
        @return: the hash of the str of the key
        @rtype: int"""
        return hash(self.aa+self.ptm)

class MSingleAAPTMNL(ChimicalElement):
    """ this class is used to represent one AA linked to a PTM or not 
    and with a neutral loss or not
    @ivar aa: the keychar of the amino acid
    @type aa: char
    @ivar ptm: the name of the ptm
    @type ptm: string
    @ivar nl: the name of the neutral loss
    @type nl: string
    @ivar composition: the composition of the ptm
    @type composition: {char: int}
    @ivar position: the position of the amino acid needed to have the 
    ptm (n-term, c-term, any)
    @type position: string
    """
    
    def __init__(self, msingle_aaptm, msingle_nl):
        """Test if the 2 arguments are of the good type
        and if they are compatible then lauch the rule
        to create an MSingleAAPTMNL object
        @param msingle_aaptm: a MSingleAAPTM class
        @type msingle_aaptm: MSingleAAPTM
        @param msingle_nl: a MSingleNeutralLoss class
        @type msingle_nl: MSingleNeutraLoss
        @raise TypeError: if one of the argument is not of 
        the good class
        @raise ValueError: if the aaptm doesnt match with the neutral
        loss
        """
        if (isinstance(msingle_aaptm, MSingleAAPTM) 
                and msingle_nl is None):
            msingle_nl = MSingleNeutralLoss("None", {}, 
                                            msingle_aaptm.aa,
                                            msingle_aaptm.ptm,
                                            msingle_aaptm.position)
        if (not isinstance(msingle_aaptm, MSingleAAPTM)
                or not isinstance(msingle_nl, MSingleNeutralLoss)):
            raise TypeError
        if msingle_aaptm.aa != msingle_nl.aa:
            raise ValueError
        if msingle_aaptm.ptm != msingle_nl.ptm:
            raise ValueError
        if msingle_aaptm.position != msingle_nl.side:
            raise ValueError
        self.rule_create_aaptmnl(msingle_aaptm, msingle_nl)
    
    def rule_create_aaptmnl(self, msingle_aaptm, msingle_nl):
        """rule to create the aaptmnl
        @param msingle_aaptm: a MSingleAAPTM class
        @type msingle_aaptm: MSingleAAPTM
        @param msingle_nl: a MSingleNeutralLoss class
        @type msingle_nl: MSingleNeutraLoss
        @raise TypeError: if one of the argument is not of 
        the good class
        @raise ValueError: if the aaptm doesnt match with the neutral
        loss"""
        self.aa = msingle_aaptm.aa
        self.ptm = msingle_aaptm.ptm
        self.nl = msingle_nl.neutral_loss
        self.composition = combine_compo(msingle_aaptm.composition,
                                         msingle_nl.composition)
        self.position = msingle_aaptm.position


    def __eq__(self, other):
        """ test the equality between 2 aaptmnl 
        @param other: the other element 
        @type other: MSingleAAPTMNL
        @return: True if equal, False if not
        @rtype: Boolean  """
        return (self.aa == other.aa and self.ptm == other.ptm and
                self.nl == other.nl and self.position == other.position
                and isinstance(self, MSingleAAPTMNL) and 
                isinstance(other, MSingleAAPTMNL))
    
    def __ne__(self, other):
        """ test the inequality between 2 aaptmnl 
        @param other: the other element 
        @type other: MSingleAAPTMNL
        @return: False if equal, True if not
        @rtype: Boolean  """
        return not self.__eq__(other)
#~ 
class MSingleAAPTMResidue(ChimicalElement):
    """ This class is used to represent a residue of an amino acid
    with possibly a ptm but no neutral loss
    @ivar aa: an amino acid key char
    @type aa: char
    @ivar ptm: the name of a ptm
    @type ptm: string
    @ivar side: the side where the residu is suppose to be(c-term, 
    n-term, center)
    @type side: str
    @ivar composition: the composition of the residu in atoms
    @type composition: dict(char: int)
    @ivar residue: the type of residue (Immonuim for example)
    @type residue: string"""
    
    def __init__(self, ms_aaptm, ms_residue):
        """check if the param are of the good type and 
        compatible between them.
        if it is then lauch the rule to create it
        @param ms_aaptm: a MSingleAAPTM class
        @type ms_aaptm: MSingleAAPTM
        @param ms_residue: a MSingleResidue class
        @type ms_residue: MSingleResidue"""
        if not isinstance(ms_aaptm, MSingleAAPTM):
            raise TypeError
        if not isinstance(ms_residue, MSingleResidue):
            raise TypeError
        self.rule_create_aaptmresidue(ms_aaptm, ms_residue)
        
    def rule_create_aaptmresidue(self, ms_aaptm, ms_residue):
        """This rules create fill the MSingleAAPTMResidue
        if it is then lauch the rule to create it
        @param ms_aaptm: a MSingleAAPTM class
        @type ms_aaptm: MSingleAAPTM
        @param ms_residue: a MSingleResidue class
        @type ms_residue: MSingleResidue"""
        compo = combine_compo(ms_residue.composition, 
                              ms_aaptm.composition)
        self.aa = ms_aaptm.aa
        self.ptm = ms_aaptm.ptm
        self.residue = ms_residue.residue
        self.residue_compo = ms_residue.composition
        self.composition = compo
        
    def __eq__(self, other):
        """ test the equality between 2 aaptm or aaptmresidue
        @param other: the other element 
        @type other: MSingleAAPTMResidue or MSingleAAPTM
        @return: True if equal, False if not
        @rtype: Boolean  """
        return ((isinstance(self, MSingleAAPTMResidue) and
                (isinstance(other, MSingleAAPTM) or 
                 isinstance(self, MSingleAAPTMResidue) and
                self.aa == other.aa and self.ptm == other.ptm)) or
                (isinstance(self, MSingleAAPTMResidue) and 
                 isinstance(other, MSingleAAPTM) and
                 self.aa == other.aa and self.ptm == "None")) 
    
    def __ne__(self, other):
        """ test the inequality between 2 aaptm or aaptmresidue
        @param other: the other element 
        @type other: MSingleAAPTMResidue or MSingleAAPTM
        @return: False if equal, True if not
        @rtype: Boolean  """
        return not self.__eq__(other)
        
    def __hash__(self):
        """ the hash of the elem
        @return: the hash of the str of the aa key and the ptm key
        @rtype: int """
        return hash(self.aa+self.ptm)

class MSingleAAResidue(ChimicalElement):
    """ This class is used to represent a residue of an amino acid
    with possibly a ptm but no neutral loss
    @ivar aa: an amino acid key char
    @type aa: char
    @ivar ptm: the name of a ptm
    @type ptm: string
    @ivar side: the side where the residu is suppose to be(c-term, 
    n-term, center)
    @type side: str
    @ivar composition: the composition of the residu in atoms
    @type composition: dict(char: int)
    @ivar residue: the type of residue (Immonuim for example)
    @type residue: string"""
    
    def __init__(self, ms_aa, ms_residue):
        """check if the param are of the good type and 
        compatible between them.
        if it is then lauch the rule to create it
        @param ms_aaptm: a MSingleAAPTM class
        @type ms_aaptm: MSingleAAPTM
        @param ms_residue: a MSingleResidue class
        @type ms_residue: MSingleResidue"""
        if not isinstance(ms_aa, MSingleAA):
            raise TypeError
        if not isinstance(ms_residue, MSingleResidue):
            raise TypeError
        self.rule_create_aaresidue(ms_aa, ms_residue)
        
    def rule_create_aaresidue(self, ms_aa, ms_residue):
        """This rules create fill the MSingleAAResidue
        if it is then lauch the rule to create it
        @param ms_aaptm: a MSingleAA class
        @type ms_aaptm: MSingleAA
        @param ms_residue: a MSingleResidue class
        @type ms_residue: MSingleResidue"""
        compo = combine_compo(ms_residue.composition, 
                              ms_aa.composition)
        self.aa = ms_aa.aa
        self.residue = ms_residue.residue
        self.residue_compo = ms_residue.composition
        self.composition = compo
        
    def __eq__(self, other):
        """ test the equality between 2 aa or aaresidue
        @param other: the other element 
        @type other: MSingleAAResidue or MSingleAA
        @return: True if equal, False if not
        @rtype: Boolean  """
        return ((isinstance(self, MSingleAAResidue) and
                (isinstance(other, MSingleAA) or 
                 isinstance(self, MSingleAAResidue) and
                self.aa == other.aa)) or
                (isinstance(self, MSingleAAResidue) and 
                 isinstance(other, MSingleAA) and
                 self.aa == other.aa)) 
    
    def __ne__(self, other):
        """ test the inequality between 2 aa or aaresidue
        @param other: the other element 
        @type other: MSingleAAResidue or MSingleAA
        @return: False if equal, True if not
        @rtype: Boolean  """
        return not self.__eq__(other)
        
    def __hash__(self):
        """ the hash by key
        @return: the hash of the str of the key
        @rtype: int"""
        return hash(self.aa)
        
#~ class MSingleAAPTMNLResidue(object):
    #~ """This class is used to represent a residu of an amino acid
    #~ with possibly a ptm and a neutral loss
    #~ @ivar aa: an amino acid key char
    #~ @type aa: char
    #~ @ivar ptm: the name of a ptm
    #~ @type ptm: string
    #~ @ivar nl: a neutral loss
    #~ @type nl: string
    #~ @ivar composition: the composition of the residu in atoms
    #~ @type composition: dict(char: int)
    #~ @ivar residue: the type of residue (Immonuim for example)
    #~ @type residue: string"""
    #~ 
    #~ def __init__(self, ms_aaptmnl, ms_residue, ms_link):
        #~ """The init check if the params are compatible with each other
        #~ to use the rule to create a MSingleAAPTMResidue and use it
        #~ @param ms_aaptmnl: a MSingleAAPTM class
        #~ @type ms_aaptmnl: MSingleAAPTM 
        #~ @param ms_residue: a MsingleResidue class
        #~ @type ms_residue: MSingleResidue
        #~ @param ms_link: a MSingleLink class
        #~ @type ms_link: MSingleLink"""
        #~ if not isinstance(ms_aaptmnl, MSingleAAPTMNL):
            #~ raise TypeError
        #~ if not isinstance(ms_residue, MSingleResidue):
            #~ raise TypeError
        #~ if not isinstance(ms_link, MSingleLink):
            #~ raise TypeError
        #~ if ms_aaptmnl.aa != ms_residue.aa:
            #~ raise ValueError
        #~ if ms_aaptmnl.ptm != ms_residue.ptm:
            #~ raise ValueError
        #~ if ms_aaptmnl.position != ms_residue.side:
            #~ raise ValueError
        #~ if ms_aaptmnl.position != ms_link.link:
            #~ raise ValueError
        #~ self.rule_create_aaptmresidue(ms_aaptmnl, ms_residue, ms_link)    
        #~ 
    #~ 
    #~ def rule_create_aaptmresidue(self, ms_aaptmnl, ms_residue, ms_link):
        #~ """This rule is used to fill the MSingleAAPTMResidue object
        #~ @param ms_aaptmnl: a MSingleAAPTMNL class
        #~ @type ms_aaptmnl: MSingleAAPTMNL 
        #~ @param ms_residue: a MsingleResidue class
        #~ @type ms_residue: MsingleResidue
        #~ @param ms_link: a MSingleLink class
        #~ @type ms_link: MSingleLink"""
        #~ compo = combine_compo(ms_residue.composition, 
                              #~ ms_aaptmnl.composition)
        #~ compo = combine_compo(compo, ms_link.composition)
        #~ self.aa = ms_aaptmnl.aa
        #~ self.ptm = ms_aaptmnl.ptm
        #~ self.nl = ms_aaptmnl.nl
        #~ self.position = ms_aaptmnl.position
        #~ self.composition = compo
        #~ self.residue = ms_residue.residue
    #~ 
    #~ def __repr__(self):
        #~ return '<%s, %s, %s>' % (self.aa, self.ptm, self.nl)

class MAAPTMDLinks(ChimicalElement):
    """ This class represent a MAAPTMNL with links from the double 
    links.
    it's a part of a sequence with a side join the the sequence
    and a cut of the other side
    @ivar joinside: the cut on the side join to another sequence
    @type joinside: str
    @ivar cutside: the cut on the free side (where it's have been cut
    @type cutside: str
    @ivar compositon: the composition of the aaptmnl with 2 links
    @type composition: dict(str :int)
    @ivar aa: the amino acid key identifier of the aaptmnl
    @type aa: str
    @ivar ptm: the ptm name identifier of the aaptmnl
    @type ptm: str
    @ivar nl: the neutral loss identifer of the aaptmnl
    @type nl: str"""
    
    def __init__(self, ms_aaptmnl, dlink):
        """check if the param have the good type and
        are compatible then lauch the rule to create the 
        aaptmnldlinks
        @param ms_aaptmnl: a AAPTMNL
        @type ms_aaptmnl: MSingleAAPTMNL 
        @param dlink: a Double link
        @type dlink: MDoubleLinks"""
        if not isinstance(ms_aaptmnl, MSingleAAPTM):
            raise TypeError
        if not isinstance(dlink, MDoubleLinks):
            raise TypeError
        if ms_aaptmnl.position == "c-term":
            if dlink.joinside != "c-term" and dlink.cutside != "c-term":
                raise ValueError("the aaptmnl and the double link \
                are not compatible")
        if ms_aaptmnl.position == "n-term":
            if dlink.joinside != "n-term" and dlink.cutside != "n-term":
                raise ValueError("the aaptmnl and the double link \
                are not compatible")
        
        self.rule_create_aaptmnldlinks(ms_aaptmnl, dlink)
    
    def rule_create_aaptmnldlinks(self, ms_aaptmnl, dlink):
        """ Create the aaptmnldlinks
        @param ms_aaptmnl: a AAPTMNL
        @type ms_aaptmnl: MSingleAAPTMNL 
        @param dlink: a Double link
        @type dlink: MDoubleLinks """
        self.joinside = dlink.joinside
        self.joincomplement = dlink.lcomplement
        self.cutside = dlink.cutside
        self.composition = combine_compo(ms_aaptmnl.composition,
                                         dlink.composition)
        self.aa = ms_aaptmnl.aa
        self.ptm = ms_aaptmnl.ptm
    
    def __eq__(self, other):
        """redifine __eq__ to be equal by key"""
        return (self.aa == other.aa and self.ptm == other.ptm and
                self.joinside == other.joinside and
                self.cutside == other.cutside)


class MAAPTMNLDLinks(ChimicalElement):
    """ This class represent a MAAPTMNL with links from the double 
    links.
    it's a part of a sequence with a side join the the sequence
    and a cut of the other side
    @ivar joinside: the cut on the side join to another sequence
    @type joinside: str
    @ivar cutside: the cut on the free side (where it's have been cut
    @type cutside: str
    @ivar compositon: the composition of the aaptmnl with 2 links
    @type composition: dict(str :int)
    @ivar aa: the amino acid key identifier of the aaptmnl
    @type aa: str
    @ivar ptm: the ptm name identifier of the aaptmnl
    @type ptm: str
    @ivar nl: the neutral loss identifer of the aaptmnl
    @type nl: str"""
    
    def __init__(self, ms_aaptmnl, dlink):
        """check if the param have the good type and
        are compatible then lauch the rule to create the 
        aaptmnldlinks
        @param ms_aaptmnl: a AAPTMNL
        @type ms_aaptmnl: MSingleAAPTMNL 
        @param dlink: a Double link
        @type dlink: MDoubleLinks"""
        if not isinstance(ms_aaptmnl, MSingleAAPTMNL):
            raise TypeError
        if not isinstance(dlink, MDoubleLinks):
            raise TypeError
        if ms_aaptmnl.position == "c-term":
            if dlink.joinside != "c-term" and dlink.cutside != "c-term":
                raise ValueError("the aaptmnl and the double link \
                are not compatible")
        if ms_aaptmnl.position == "n-term":
            if dlink.joinside != "n-term" and dlink.cutside != "n-term":
                raise ValueError("the aaptmnl and the double link \
                are not compatible")
        
        self.rule_create_aaptmnldlinks(ms_aaptmnl, dlink)
    
    def rule_create_aaptmnldlinks(self, ms_aaptmnl, dlink):
        """ Create the aaptmnldlinks
        @param ms_aaptmnl: a AAPTMNL
        @type ms_aaptmnl: MSingleAAPTMNL 
        @param dlink: a Double link
        @type dlink: MDoubleLinks """
        self.joinside = dlink.joinside
        self.joincomplement = dlink.lcomplement
        self.cutside = dlink.cutside
        self.composition = combine_compo(ms_aaptmnl.composition,
                                         dlink.composition)
        self.aa = ms_aaptmnl.aa
        self.ptm = ms_aaptmnl.ptm
        self.nl = ms_aaptmnl.nl
        
    
    def __eq__(self, other):
        """redifine __eq__ to be equal by key"""
        return (self.aa == other.aa and self.ptm == other.ptm and
                self.nl == other.nl and
                self.joinside == other.joinside and
                self.cutside == other.cutside)

class SequenceDlink(ChimicalElement):
    """ Class to represent a sequence with dual link
    @ivar composition: the composition of the sequence
    @type composition: dict(str: int)
    @ivar sequence: list of all aa, ptm, nl with
    the shape list((aa, ptm, nl))
    @type sequence: list(tuple(aa, ptm, nl))
    @ivar cutside: the side of the cut (a-cut, n-term, y-cut, ect ...)
    @type cutside: str
    @ivar joinside: the side of the join (a-cut, n-term, y-cut, ect ...)
    @type joinside: str
    @ivar compl: the side complement of the joinside
    @type cutside: str"""
    
    def __init__(self, l_maaptmnl, dlink):
        """ test if everything is compatible then lauch the rule
        and init the class
        @param l_maaptmnl: a list of aaptmnl to put in the sequence
        @type l_maaptmnl: list(MSingleAAPTMNL)
        @param dlink: the double link to apply to the sequence
        @type dlink: MDoubleLinks 
         """
        self.composition = {}
        self.sequence = []
        self.cutside = ""
        self.joinside = ""
        self.compl = ""
        nbcterm = 0
        nbnterm = 0
        for aaptmnl in l_maaptmnl:
            if not isinstance(aaptmnl, MSingleAAPTMNL):
                raise TypeError
            if aaptmnl.position == "c-term":
                nbcterm += 1
            if aaptmnl.position == "n-term":
                nbnterm += 1    
        if not isinstance(dlink, MDoubleLinks):
            raise TypeError
        if nbcterm > 1:
            raise ValueError("Too much aaptmnl on cterm")
        if nbnterm > 1:
            raise ValueError("Too much aaptmnl on nterm")
        if (nbcterm == 1 and dlink.joinside != "c-term" and 
                dlink.cutside != "c-term"):
            raise ValueError("dlink not compatible with the list of \
            aaptmnl") 
        if (nbnterm == 1 and dlink.joinside != "n-term" and 
                dlink.cutside != "n-term"):
            raise ValueError("dlink not compatible with the list of \
            aaptmnl")
        self.rule_create_sequence(l_maaptmnl, dlink)
        
    def rule_create_sequence(self, l_maaptmnl, dlink):
        """ create the sequence
        @param l_maaptmnl: a list of aaptmnl to put in the sequence
        @type l_maaptmnl: list(MSingleAAPTMNL)
        @param dlink: the double link to apply to the sequence
        @type dlink: MDoubleLinks """
        for aaptmnl in l_maaptmnl:
            aa = aaptmnl.aa
            ptm = aaptmnl.ptm
            nl = aaptmnl.nl
            self.composition = combine_compo(self.composition, 
                                             aaptmnl.composition)
            self.sequence.append((aa, ptm, nl))
        links = mult_compo({'H' : 1, 'O' : 1, 'N' : 1, 'C' : 1},
                           len(l_maaptmnl)-1)
        self.cutside = dlink.cutside
        self.joinside = dlink.joinside
        self.compl = dlink.lcomplement
        links = combine_compo(links, dlink.composition)
        self.composition = combine_compo(links, self.composition)
        
        
            
        
