#!/usr/bin/env python
# encoding: utf-8
""" subsequence_probability is used to compute a probability
of a subsequence (between two peaks) with different parameter
@author: Vezin Aurelien
@license: CECILL-B"""
import sys

from pwaa.utils.atoms import MAtomsMassOp

class SubsequenceInsertionEval(object):
    #to execute in ValidGraph when isertion of edgevalue
    #not used anymore
    
    def __init__(self, immoniums, proteasecuts, atoms=None):
        self.mamo = MAtomsMassOp(atoms)
        self.immoniums = immoniums
        self.proteasecuts = proteasecuts
    
    
    def evaluate(self, sequences, mass, ppm, cut, join, dval):
        cursequence = sequences[0]
        curscore = self.evaluate_sequence(sequences[0], mass, ppm, 
                                          cut, join, dval)
        for sequence in sequences[1:]:
            gscore = self.evaluate_sequence(sequence, mass, ppm, 
                                            cut, join, dval)
            if gscore > curscore:
                curscore = gscore
                cursequence = sequence
        return cursequence
            
    def evaluate_sequence(self, sequence, mass, ppm, cut, join, dval):
        massdifscore = self.mass_dif(mass, sequence, ppm)
        immoniumscore = self.immonium_owned(sequence)
        digestscore = self.digest_owned(sequence, cut, join)
        nbelemscore = self.number_of_elems(sequence)
        #need mult coef (compute with machin learning ?)
        globalscore = dval["mass_dif"] * massdifscore +\
                      dval["immonium"] * immoniumscore +\
                      dval["digest"] * digestscore +\
                      dval["nbelems"] * nbelemscore
        return globalscore
            
    
    def mass_dif(self, mass, sequence, ppm):
        """ """
        #need a computation in masses.py
        tmass = self.mamo.subsequence_masse(sequence, "mono")
        massd = abs(tmass-mass)
        massppm = mass*ppm/1000000.
        return max(0., (massppm - massd) / massppm)
    
    def immonium_owned(self, sequence):
        """ """
        #can create a biais ? not here but in the other eval ?
        immonium = 0.
        for elem in sequence:
            if elem in self.immoniums:
                immonium = 1.
                break
        return immonium
    
    def digest_owned(self, sequence, cut, join): #use the join too !
        """ """
        proteaseaa = 0.
        for elem in sequence:
            if elem in self.proteasecuts:
                proteaseaa = 1.
        if cut != "n-term" and join != "n-term":
            proteaseaa = -proteaseaa
        return proteaseaa
    
    def number_of_elems(self, sequence):
        """ """
        return 1./len(sequence)
    
class EdgeWeight(object):
    """" Object contening the weight to apply for each edge
    @ivar peakrintensity: weight for rintensity
    @type peakrintensity: float
    @ivar peakknowedcharge: weight for knowedcharge
    @type peakknowedcharge: float
    @ivar peaksymetrics: weight for symetricmasses
    @type peaksymetrics: float
    @ivar peakcweights: weight for cweight
    @type peakcweights: float
    @ivar peakrmz: weight for rmz
    @type peakrmz: float
    @ivar peakcuts:  dict of weight. Exemple 
    {"n-term" :0.4, "c-term" :0.4, "c-cut" :0.4, "b-cut" :0.4, "a-cut" :0.4, "x-cut": 0.2 .....}
    @type peakcuts: dict(str:float)
    @ivar peakotherpeaks: weight for cweight
    @type peakotherpeaks: float
    @ivar edgecharge_diff: the weight for the charge diff
    @type edgecharge_diff: float
    @ivar found_immonium: the weight for the immonium found
    @type found_immonium: float
    @ivar found_proteasebreak: the weight for the proteasebreak found
    @type found_proteasebreak: float
    """
    
    
    def __init__(self):
        """ init of the edgeweight """
        pass
        
    def peakweight(self, rintensity, knowedcharge, symetricmasses, 
                   rmz, cweight, cuts, otherpeaks):
        """ 
        adding the weight to use for the peaks
        @param rintensity: weight for rintensity
        @type rintensity: float
        @param knowedcharge: weight for knowedcharge
        @type knowedcharge: float
        @param symetricmasses: weight for symetricmasses
        @type symetricmasses: float
        @param cweight: weight for cweight
        @type cweight: float
        @param rmz: weight for rmz
        @type rmz: float
        @param cuts:  dict of weight. Exemple 
        {"n-term" :0.4, "c-term" :0.4, "c-cut" :0.4, "b-cut" :0.4, "a-cut" :0.4, "x-cut": 0.2 .....}
        @type cuts: dict(str:float)
        @param otherpeaks: weight for cweight
        @type otherpeaks: float
        """
        self.peakrintensity = rintensity
        self.peakknowedcharge = knowedcharge
        self.peaksymetrics = symetricmasses
        self.peakcweights = cweight
        self.peakrmz = rmz
        self.peakcuts = cuts
        self.peakotherpeaks = otherpeaks
        
    def edgeweight(self, charge_diff, found_immonium, 
                   found_proteasebreak):
        """ add the weight for different parameter
        @param charge_diff: the weight for the charge diff
        @type charge_diff: float
        @param found_immonium: the weight for the immonium found
        @type found_immonium: float
        @param found_proteasebreak: the weight for the proteasebreak found
        @type found_proteasebreak: float
        """
        self.edgecharge_diff = charge_diff
        self.found_immonium = found_immonium
        self.found_proteasebreak = found_proteasebreak
        #~ self.number_of_elems = number_of_elems
        #~ self.exp_mass_match = exp_mass_match
        
    def compute_score(self, peak1, peak2, peak1data, peak2data, edgedata):
        """
        compute the score of an edge
        @param peak1: the first peak
        @type peak1: (str, str, str, str)
        @param peak2: the second peak
        @type peak2: (str, str, str, str)
        @param peak1data: the first peak data
        @type peak1data: dict(str:any)
        @param peak2data: the second peak data
        @type peak2data: dict(str:any)
        @param edgedata: the edge data
        @type edgedata: dict(str:any)
        @return: the score 
        @rtype: float
        """
        p1score = self.peakcweights * self.peakcuts[peak1[1]]  +\
                  self.peakrmz * peak1data["rmz"] +\
                  self.peaksymetrics * peak1data["symmetric"] +\
                  self.peakknowedcharge * peak1data["knowed_charge"] +\
                  self.peakrintensity * peak1data["relative_intensity"] +\
                  self.peakotherpeaks * peak1data["other_peaks"]
        p2score = self.peakcweights * self.peakcuts[peak2[1]] +\
                  self.peakrmz * peak2data["rmz"] +\
                  self.peaksymetrics * peak2data["symmetric"] +\
                  self.peakknowedcharge * peak2data["knowed_charge"] +\
                  self.peakrintensity * peak2data["relative_intensity"] +\
                  self.peakotherpeaks * peak2data["other_peaks"]
        edgescore = self.edgecharge_diff * edgedata["charge_diff"] +\
                    self.found_immonium * edgedata["immonium_found"] +\
                    self.found_proteasebreak * edgedata["proteasebreak"]
                    #~ self.number_of_elems * edgedata["nbelems"] +\
                    #~ self.exp_mass_match * edgedata["deltamass"]
        score = p1score + p2score + edgescore
        return score
                  
class PeakWeight(object):
    
    def __init__(self):
        pass
    
    def peakweight(self, rintensity, knowedcharge, symetricmasses, 
                   rmz, cweight, cuts, otherpeaks):
        """ 
        adding the weight to use for the peaks
        @param rintensity: weight for rintensity
        @type rintensity: float
        @param knowedcharge: weight for knowedcharge
        @type knowedcharge: float
        @param symetricmasses: weight for symetricmasses
        @type symetricmasses: float
        @param cweight: weight for cweight
        @type cweight: float
        @param rmz: weight for rmz
        @type rmz: float
        @param cuts:  dict of weight. Exemple 
        {"n-term" :0.4, "c-term" :0.4, "c-cut" :0.4, "b-cut" :0.4, "a-cut" :0.4, "x-cut": 0.2 .....}
        @type cuts: dict(str:float)
        @param otherpeaks: weight for cweight
        @type otherpeaks: float
        """
        self.peakrintensity = rintensity
        self.peakknowedcharge = knowedcharge
        self.peaksymetrics = symetricmasses
        self.peakcweights = cweight
        self.peakrmz = rmz
        self.peakcuts = cuts
        self.peakotherpeaks = otherpeaks

    
    def compute_score(self, peak, peakdata):
        """
        compute the score of an edge
        @param peak: the first peak
        @type peak: (str, str, str, str)
        @param peakdata: the first peak data
        @type peakdata: dict(str:any)
        @return: the score 
        @rtype: float
        """
        pscore = self.peakcweights * self.peakcuts[peak[1]]  +\
                 self.peakrmz * peakdata["rmz"] +\
                 self.peaksymetrics * peakdata["symmetric"] +\
                 self.peakknowedcharge * peakdata["knowed_charge"] +\
                 self.peakrintensity * peakdata["relative_intensity"] +\
                 self.peakotherpeaks * peakdata["other_peaks"]
        return pscore
    
    def __str__(self):
        return str(self.peakrintensity)+" "+str(self.peakknowedcharge)+\
            " "+str(self.peaksymetrics)+" "+str(self.peakcweights)+" "+str(self.peakrmz)+\
            " "+str(self.peakcuts)+" "+str(self.peakotherpeaks)
