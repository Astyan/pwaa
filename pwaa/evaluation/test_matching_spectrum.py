#!/usr/bin/env python
# encoding: utf-8
""" test_matching_spectrum.py is a class to test matching_spectrum.py"""

import unittest
import os, sys

from pwaa.evaluation.matching_spectrum import (peak_matching_score, 
                                               MatchingSpectrum, GAScore)
from pwaa.basicknowledge.melemscreate import fill_all_aa, fill_all_link
from pwaa.analyser.theorical_spectrum import TheoricalSpectrum
from pwaa.basicknowledge.mcombielemsoperation import LinksOperations
from pwaa.analyser.mztom import MassSpectrum

class TestMatchingSpectrumFunctions(unittest.TestCase):
    """ testing of all the functions"""
    
    def test_10_peak_matching_score(self):
        """ test of matching """
        self.assertEqual(peak_matching_score(100.005,100., 50) ,0.0)
        self.assertEqual(round(peak_matching_score(100.001,100., 50), 2) ,0.80)
        self.assertEqual(round(peak_matching_score(100.0, 100.0, 50), 2) ,1.0)

class TestMatchingSpectrum(unittest.TestCase):
    """ testing of the matching spectrum"""
    
    @classmethod
    def setUpClass(cls):
        """ do all the operation to create the a Matching spectrum"""
        #get the theorical spectrum
        aas = fill_all_aa()
        links = fill_all_link()
        lop = LinksOperations(links)
        seqtext = ["A", "C"]
        seqaa = []
        for aasymbol in seqtext:
            for aa in aas:
                if aasymbol == aa.aa:
                    seqaa.append(aa)
                    break
        ts = TheoricalSpectrum(seqaa, lop)
        theoricalspectrum = ts.compute_all_peaks()
        mspectrum = MassSpectrum(os.path.join("evaluation", 
                                              "files", 
                                              "factice.mgf"))
        experimentalspectrum = mspectrum.converted_peak()
        #~ experimentallist = []
        #~ for key in experimentalspectrum:
            #~ experimentallist.append(key)
        #~ print sorted(experimentallist)
        #~ print sorted(theoricalspectrum)
        cls.mspectrum = MatchingSpectrum(experimentalspectrum, theoricalspectrum)
                                
    
    def test_10_get_max_intensity(self):
        """ test of get_max_intensity """
        self.mspectrum.set_max_intensity()
        self.assertEqual(self.mspectrum.max_intensity, 423.0)
        
    def test_11_compute_score(self):
        """ test of compute_score"""
        self.assertEqual(round(self.mspectrum.compute_score(20), 2), 8.42)

class TestGAScore(unittest.TestCase):
    
    def test_10_simplegascore(self):
        s1 = [ (0.0, 1), (21.1, 0.2), (215.1, 0.6), (540.1, 0.9) ]
        s2 = [ (0.0, 1), (21.101, 0.2), (215.2, 0.6), (540.103, 0.9) ]
        gas = GAScore(s1, s2)
        self.assertEqual(round(gas.compute_score(50), 2), -3.58)


if __name__ == '__main__':
    unittest.main()
