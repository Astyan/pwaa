#!/usr/bin/env python
# encoding: utf-8
""" test_subsequence_probability.py is a class to test matching_spectrum.py"""

import unittest
import os, sys

from pwaa.evaluation.subsequence_probability import SubsequenceInsertionEval
from pwaa.basicknowledge.melemscreate import fill_all_aa

class TestSubsequenceInsertionEval(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.aalist = fill_all_aa()
        cls.sie = SubsequenceInsertionEval(cls.aalist[0:2], 
                                           cls.aalist[2:4])
        
    def test_number_of_elems(self):
        self.assertEqual(self.sie.number_of_elems(self.aalist[2:4]), 0.5)
        self.assertEqual(self.sie.number_of_elems(self.aalist[2:3]), 1.)
        
    def test_digest_owned(self):
        self.assertEqual(self.sie.digest_owned(self.aalist[3:6], 
                                               "n-term", 
                                               "c-cut"), 1.)
        self.assertEqual(self.sie.digest_owned(self.aalist[3:6], 
                                               "c-cut",
                                               "c-cut"), -1)
        self.assertEqual(self.sie.digest_owned(self.aalist[5:7], 
                                               "n-term",
                                               "c-cut"), 0.0)
        self.assertEqual(self.sie.digest_owned(self.aalist[5:7], 
                                               "c-cut",
                                               "c-cut"), 0.0)
    
    def test_immonium_owned(self):
        self.assertEqual(self.sie.immonium_owned(self.aalist[1:5]), 1.)
        self.assertEqual(self.sie.immonium_owned(self.aalist[4:5]), 0.)
    
    def test_mass_dif(self):
        self.assertEqual(self.sie.mass_dif(72.02113, self.aalist[3:4], 50),
                         1.)#72.02113
        self.assertEqual(self.sie.mass_dif(276.097, self.aalist[3:6], 50),
                         0.0)#276.111008
        self.assertEqual(self.sie.mass_dif(274.097, self.aalist[3:6], 50),
                         0.0)
        self.assertAlmostEqual(self.sie.mass_dif(276.097, self.aalist[3:6], 1000),
                               0.949264207869, 3)
                               
    def test_evaluate_sequence(self):
        weights = {"mass_dif" : 4., "immonium" : 1., 
                   "digest" : 2., "nbelems": 4.} 
        self.assertAlmostEqual(self.sie.evaluate_sequence(self.aalist[3:4], 
                                                          72.0211, 50,
                                                          "n-term", 
                                                          "c-cut", 
                                                          weights),
                               9.96667643233, 3)
                                         
if __name__ == '__main__':
    unittest.main()
