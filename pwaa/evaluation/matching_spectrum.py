#!/usr/bin/env python
# encoding: utf-8
"""matching_spectrum.py have as a goal to provide a score between
a theorical spectrum and the experimental spectrum 
@author: Vezin Aurelien
@license: CECILL-B"""

import sys

def peak_matching_score(mass_exp, mass_theoric, ppm):
    """give a score of matching between 2 peaks
    @param max_exp: the mass experimental of a peak
    @type max_exp: float
    @param mass_theoric: the mass theoric of a peak of the
    sequence to eval
    @type mass_theoric: float
    @param ppm: the ppm of the spectrum
    @type ppm: int
    @return: the score for this matching (between 0 and 1)
    @rtype: float"""
    if mass_theoric > mass_exp:
        mass_theoric_delta = mass_theoric - mass_theoric * ppm/1000000.
    else:
        mass_theoric_delta = mass_theoric + mass_theoric * ppm/1000000.
    min_mass = min(mass_theoric, mass_theoric_delta)
    max_mass = max(mass_theoric, mass_theoric_delta)
    score = 0.0
    if mass_exp >= min_mass and mass_exp <= max_mass:
        try:
            score = 1 - abs(mass_exp - mass_theoric)/(max_mass - min_mass)
        except ZeroDivisionError:
            score = 1 - abs(mass_exp - mass_theoric)
    return score


class MatchingSpectrum(object):
    """ Compute the matching spectrum score 
    @ivar spectrum_exp: the experiemental spectrum, a dictionnary with 
    the mass as the key
    @type spectrum_exp: dict(float : dict(str : all))
    @ivar spectrum_theoric: the mass of the theoric spectrum
    @type spectrum_theoric: list(float)
    @ivar max_intensity: the maximal intensity in the experimental spectrum
    @type max_intensity: float
    """
    
    def __init__(self, spectrum_exp, spectrum_theoric):
        """ set all the class attributes
        @param spectrum_exp: the experiemental spectrum, a dictionnary with 
        the mass as the key
        @type spectrum_exp: dict(float : dict(str : all))
        @param spectrum_theoric: the mass of the theoric spectrum
        @type spectrum_theoric: list(float) """
        self.spectrum_exp = spectrum_exp
        self.spectrum_theoric = spectrum_theoric
        self.max_intensity = 0.
    
    def set_max_intensity(self):
        """ set the maximal intensity using the experimental spectrum"""
        for key in self.spectrum_exp:
            if self.spectrum_exp[key]['intensity'] > self.max_intensity:
                self.max_intensity = float(self.spectrum_exp[key]['intensity'])
    
    def compute_score(self, ppm):
        """ compute the matching spectrum score 
        @param ppm: the ppm we allow
        @type ppm: int
        @return: the global score of the match
        @rtype: float"""
        gscore = 0.
        for mass_exp in self.spectrum_exp:
            for mass_theoric in self.spectrum_theoric:
                score = peak_matching_score(mass_exp, mass_theoric, ppm)
                if score != 0:
                    score = score + score * score * float(self.spectrum_exp[mass_exp]['intensity'])/self.max_intensity
                gscore += score
        return gscore
                

class GAScore(object):
    """ 
    Compute the score of the genetic algorithm
    @ivar individual_spectrum: the mass and intensity of 
    the individual spectrum
    @type individual_spectrum: list((float, float))
    @ivar valid_spectrum: the mass and intensity of 
    the valid spectrum
    @type valid_spectrum: list((float, float))
    """
    
    def __init__(self, individual_spectrum , valid_spectrum):
        """ init of the score computation
        @param individual_spectrum: the mass and intensity of 
        the individual spectrum
        @type individual_spectrum: list((float, float))
        @param valid_spectrum: the mass and intensity of 
        the valid spectrum
        @type valid_spectrum: list((float, float))
        """
        self.individual_spectrum = individual_spectrum
        self.valid_spectrum = valid_spectrum
        
    
    def compute_score(self, ppm):
        """
        compute the score for the genetic algorithm
        @param ppm: the error rate
        @type ppm: int
        @return: the score
        @rtype: float
        """
        gscore = 0.
        for individual_mass, individual_intensity in self.individual_spectrum:
            for valid_mass, valid_intensity in self.valid_spectrum:
                mscore = peak_matching_score(individual_mass, valid_mass, ppm)
                if mscore != 0:
                    mscore = mscore + mscore * mscore * valid_intensity * individual_intensity
                gscore += mscore
        return -abs(gscore)

             
        
    
