#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_atoms.py unittest for models in atoms.py 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys

sys.path.append('../')

from utils.atoms import MAtoms, MAtomsMassOp
from basicknowledge.melems import MSingleAA, ChimicalElement



class TestMAtoms(unittest.TestCase):
    """test for MAtoms class"""
    
    def setUp(self):
        """init the MAtoms class"""
        self.matoms = MAtoms()
        self.assertEqual(len(self.matoms.atoms), 8)
        
    def tearDown(self):
        """Clear after each tests"""
        pass
    
    def test_10_get_mass_atom(self):
        """test of get_mass_atom:
        test if the monoisotopic mass is the good one for some atoms"""
        self.assertEqual(self.matoms.get_mass_atom('C'), 12.0)
        self.assertEqual(round(self.matoms.get_mass_atom('H'), 7),
                         1.007825)
        self.assertEqual(round(self.matoms.get_mass_atom('O'), 7),
                         15.994915)
        self.assertEqual(round(self.matoms.get_mass_atom('N'), 7),
                         14.003074)
        self.assertEqual(round(self.matoms.get_mass_atom('S'), 7), 
                         31.972071)
    
    def test_10_get_average_mass_atom(self):
        """test of get_average_mass_atom:
        test if the avertage mass is the good one for some atoms"""
        self.assertEqual(round(self.matoms.get_average_mass_atom('C'),
                               6), round(12.010736, 6))
        self.assertEqual(round(self.matoms.get_average_mass_atom('H'),
                               6), round(1.007941, 6))
        self.assertEqual(round(self.matoms.get_average_mass_atom('O'),
                               6), round(15.999405, 6))
        self.assertEqual(round(self.matoms.get_average_mass_atom('N'),
                               6), round(14.006703, 6))
        self.assertEqual(round(self.matoms.get_average_mass_atom('S'),
                               6), round(32.064787, 6))
    
    def test_10_get_min_mass_atom(self):
        """ test of get_min_mass_aton:
        test if the min mass is the good one for some atoms """
        self.assertAlmostEqual(self.matoms.get_min_mass_atom('C'), 
                               12.0, 2)
        self.assertAlmostEqual(self.matoms.get_min_mass_atom('H'), 
                               1.00794, 3)
        self.assertAlmostEqual(self.matoms.get_min_mass_atom('O'), 
                               15.994915, 3)
    
    def  test_10_get_max_mass_atom(self):
        """ test of get_max_mass_aton:
        test if the max mass is the good one for some atoms """
        self.assertAlmostEqual(self.matoms.get_max_mass_atom('C'), 
                               13.0033548378, 6)
        self.assertAlmostEqual(self.matoms.get_max_mass_atom('H'),
                               2.0141017778, 6)
        self.assertAlmostEqual(self.matoms.get_max_mass_atom('O'),
                               17.999161, 6)
    
    def test_10_changes_bound(self):
        """ test if change_bound works """
        self.matoms.changes_bound('C', 12, 0.99)
        self.assertEqual(self.matoms.atoms['C'], 
                         [(12.0, 0.99, 12), 
                          (13.0033548378, 0.009999999999999969, 13)])
        self.matoms.changes_bound('O', 16, 0.99)
        self.assertEqual(self.matoms.atoms['O'], 
                         [(15.994915, 0.99, 16), 
                          (16.9991317, 0.0015637860082304265, 17), 
                          (17.999161, 0.008436213991769406, 18)])
    
    def test_11_get_mono_masses(self):
        """ test if get_mono_masses works"""
        res = self.matoms.get_mono_masses()
        self.assertEqual(sorted(res), sorted([1.007825, 12.0, 
                                              14.003074, 15.994915, 
                                              30.973761, 31.972071, 
                                              74.9216, 79.916519]))
    
    def test_11_get_mono_masses_dictatoms(self):
        """ test if get_mono_masses_dictatoms works"""
        res = self.matoms.get_mono_masses_dictatoms()
        self.assertEqual(res, {u'C': 12.0, u'H': 1.007825, 
                               u'As': 74.9216, u'O': 15.994915, 
                               u'N': 14.003074, u'P': 30.973761, 
                               u'S': 31.972071, u'Se': 79.916519})
    
    def test_11_enrich(self):
        """test the enrich function """
        self.matoms.enrich('S', 32, 0.1)
        self.assertEqual(round(self.matoms.atoms['S'][0][1], 4), 0.9549)
    
    
    def test_11_get_maxbin_mass_atom(self):
        """ test of get_maxbin_mass_atom """
        self.assertAlmostEqual(self.matoms.get_maxbin_mass_atom('Se', 
                                                                50),
                               3948.8659910999972, 5)
                        
class TestMAtomsMassOp(unittest.TestCase):
    """test for MAtomsMassOp class"""
    
    def test_10_atomlist_to_masses(self):
        """ test of atomlist_to_masses"""
        mamo = MAtomsMassOp()
        
        mass_mono = mamo.atomlist_to_masses({'C': 24, 'H': 30, 
                                             'O': 11, 'N': 10, 
                                             'S': 0, 'Se': 50}, 
                                             "mono")
        self.assertEqual(mass_mono, 4630.035505)
        mass_avg = mamo.atomlist_to_masses({'C': 24, 'H': 30, 
                                            'O': 11, 'N': 10, 
                                            'S': 0, 'Se': 50}, 
                                            "avg")
        self.assertEqual(mass_avg, 4582.525728574131)                                            
        mass_dis = mamo.atomlist_to_masses({'C': 24, 'H': 30, 
                                            'O': 11, 'N': 10, 
                                            'S': 0, 'Se': 50}, 
                                            "dis")
        self.assertEqual(mass_dis, 4583.075546099997)
        
    def test_10_l_mol_to_dmassmol(self):
        """ test of l_mol_to_dmassmol"""
        l_mol = [MSingleAA('A', {'H' :3, 'O' :1}), 
                 MSingleAA('S', {'H' :3, 'O' :1}),
                 MSingleAA('P', {'H' :2, 'O' :1})]
        mamo = MAtomsMassOp()
        res = mamo.l_mol_to_dmassmol(l_mol, "mono")
        self.assertEqual(len(res[19.01839]), 2)
        self.assertEqual(len(res[18.010565]), 1)
        
    def test_11_subsequence_masse(self):
        one_elem = [ChimicalElement({"H" :1})]
        two_elem = [ChimicalElement({"H" :1}), ChimicalElement({"H" :1})]
        mamo = MAtomsMassOp()
        self.assertAlmostEqual(mamo.subsequence_masse(one_elem, "mono"),
                               1.007825,
                               3)
        self.assertAlmostEqual(mamo.subsequence_masse(two_elem, "mono"),
                               45.021464,
                               3)

    
if __name__ == '__main__':
    unittest.main()
