#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=unused-argument
""" timeout.py contain a function that limit the execution time of
any function"""

import signal

class TimeoutException(Exception): 
    """ Extention of Exception class"""
    pass


def deadline(timeout, *args):
    """is a the decotator name with the timeout parameter in second"""
    def decorate(f):
        """ the decorator creation """
        def handler(signum, frame):
            """ the handler for the timeout """
            #when the signal have been handle raise the exception
            raise TimeoutException() 

        def new_f(*args):
            """ the initiation of the handler, 
            the lauch of the function and the end of it"""
            #link the SIGALRM signal to the handler
            signal.signal(signal.SIGALRM, handler) 
            signal.alarm(timeout) #create an alarm of timeout second
            #lauch the decorate function with this parameter
            res = f(*args) 
            signal.alarm(0) #reinitiate the alarm
            return res #return the return value of the fonction
    
        new_f.__name__ = f.__name__
        return new_f
    return decorate
