#!/usr/bin/env python
# encoding: utf-8
""" atoms.py contain the class to create all atoms with
there isotopes and do operations in it 
@author: Vezin Aurelien
@license: CECILL-B"""
import copy
import sys

#~ sys.path.append('../')
from pwaa.utils.masses import abound_glob, mult_compo, combine_compo
from pwaa.database.vaadb import get_atoms


class MAtoms(object):
    """The class to acces of atoms datas
    @ivar _atoms: all atoms (use in amino acids) and there mass
    @type _atoms: dict(char : float) """
    
    def __init__(self, atoms=None):
        """Open the database and extract atoms informations:
        letter, mass"""
        if atoms == None:
            self._atoms = get_atoms()
        else:
            self._atoms = atoms
    
    @property
    def atoms(self):
        """Getter of self._atoms with @property"""
        return self._atoms
            
    def get_mass_atom(self, atom):
        """Get the monoisotopic mass of an atom 
        @param atom: the letter representing the atom
        @type atom: char
        @return: the mass of the atom
        @rtype: float"""
        ab = 0.
        m = 0.
        for mass_abound_iso in self.atoms[atom]:
            mass = mass_abound_iso[0]
            abound = mass_abound_iso[1]
            if abound > ab:
                ab = abound
                m = mass
        return m
    
    def get_mono_masses(self):
        """ get all monoisotopic masses 
        @return: a list with all monoisotopic masses
        @rtype: list(float)"""
        lres = []
        for atomkey in self.atoms:
            lres.append(self.get_mass_atom(atomkey))
        return lres
    
    def get_mono_masses_dictatoms(self):
        """ get all the monoisotopic masses in a dict with the atom 
        symbol as key 
        @return: a dict of atoms symbols with monoisotopic masses
        @rtype: dict(str: float)
        """
        res = {}
        for atom in self._atoms:
            res[atom] = self.get_mass_atom(atom)
        return res
        
    def get_maxbin_mass_atom(self, atom, nbatoms):
        """get by approximation the max mass defined by binomial laws
        @param atom: an atom identifier
        @type atom: char
        @param nbatoms: number of atom to compute the mass
        @type nbatoms: int
        @return: the max mass find
        @rtype: float"""
        cur = 0
        change = True
        abound_list = []
        abound_mass_dict = {}
        for mass_abound_iso in self.atoms[atom]:
            mass = mass_abound_iso[0]
            abound = mass_abound_iso[1]
            abound_list.append(abound)
            abound_mass_dict[abound] = mass
        l_evolve = [max(abound_list)]*nbatoms
        l_temp = copy.deepcopy(l_evolve)
        #change l_evolve to get closer and closer to the maximal global
        #abound then get the maximal global abound
        while change == True and cur < len(l_evolve):
            change = False
            for abound in abound_list:
                l_temp[cur] = abound
                g_abound_evol = abound_glob(l_evolve, abound_list)
                g_abound_temp = abound_glob(l_temp, abound_list)
                if g_abound_temp > g_abound_evol:
                    l_evolve = copy.deepcopy(l_temp)
                    change = True
                    cur += 1
        #compute the mass from the best abound sequence found
        mass = 0.
        for abound in l_evolve:
            mass += abound_mass_dict[abound]
        return mass
        
    def get_average_mass_atom(self, atom):
        """Get the average mass of an atom
        @param atom: the letter representing the atom
        @type atom: char
        @return: the average mass of the atom
        @rtype: float"""
        m = 0.
        for mass_abound_iso in self.atoms[atom]:
            mass = mass_abound_iso[0]
            abound = mass_abound_iso[1]
            m = m + mass * abound
        return m
    
    def get_min_mass_atom(self, atom):
        """ Get the minimal mass of an atom
        @param atom: the letter representing then atom
        @type atom: char
        @return:  the minimal mass of the atom
        @rtype: float"""
        m = -1
        for mass_abound_iso in self.atoms[atom]:
            mass = mass_abound_iso[0]
            if m == -1:
                m = mass
            elif m > mass:
                m = mass
        return m
    
    def get_max_mass_atom(self, atom):
        """ Get the minimal mass of an atom
        @param atom: the letter representing then atom
        @type atom: char
        @return:  the minimal mass of the atom
        @rtype: float"""
        m = -1
        for mass_abound_iso in self.atoms[atom]:
            mass = mass_abound_iso[0]
            if m < mass:
                m = mass
        return m
    
    def changes_bound(self, atom, iso, new_bound, mass=0.):
        """Set the bound of one isotope of an atom
        @param atom : the atom 
        @type atom : char
        @param iso : the isotope
        @type iso : int
        @param mass : the mass of the isotop (if it's a new isotop)
        @type mass : float
        @param new_bound : the new bound
        @type new_bound: float"""
        delta = 0.
        bound_pre = 0.
        rest = 0.
        for massiso_boundiso_isotop in self.atoms[atom]:
            boundiso = massiso_boundiso_isotop[1]
            isotop = massiso_boundiso_isotop[2]
            if isotop == iso:
                bound_pre = boundiso
                delta = bound_pre - new_bound
                rest = 1. - bound_pre
                break
        if bound_pre == 0:
            self.atoms[atom].append((mass, new_bound, iso))
            delta = -new_bound
        
        for i in range(len(self.atoms[atom])):
            if self.atoms[atom][i][2] == iso:
                self.atoms[atom][i] = (self.atoms[atom][i][0], 
                                       new_bound, iso)
            else:
                self.atoms[atom][i] = (self.atoms[atom][i][0],
                                       self.atoms[atom][i][1] + 
                                       (self.atoms[atom][i][1] / rest) * 
                                       delta, 
                                       self.atoms[atom][i][2])
        
    def enrich(self, atom, iso, percent):
        """Enrich the proportion of the isotop atom/mass with the
        percent in parameter
        @param atom : an atom
        @type atom : char
        @param iso : the isotop identifier
        @type iso : int
        @param percent : the percent of enrichisment
        @type percent : float[-1., +1]"""
        for massiso_boundiso_isotop in self.atoms[atom]:
            boundiso = massiso_boundiso_isotop[1]
            isotop = massiso_boundiso_isotop[2]
            if isotop == iso:
                self.changes_bound(atom, iso, boundiso + 
                                   (1. - boundiso) * percent)


class MAtomsMassOp(object):
    """ Operation with a need of MAtoms class"""
    
    def __init__(self, atoms=None):
        if atoms==None:
            self.matoms = MAtoms()
        else:
            self.matoms = MAtoms(atoms)
    
    def atomlist_to_masses(self, atomslist, mtype):
        """get all masses of a list of atoms
        @param atomslist: dict of atoms with the number of occurences
        @type atomslist: dict(keyatom(char):occurences(int))
        @param mtype: the type of mass
        @type mtype: str ("mono", "avg", "dis")
        @return: all masses for the atomlist
        @rtype(mass_mono(float), mass_avg(float))"""
        mass = 0.
        if mtype == "mono":
            for atom in atomslist:
                mass = mass + atomslist[atom] *\
                self.matoms.get_mass_atom(atom)
        elif mtype == "avg":
            for atom in atomslist:
                mass = mass + atomslist[atom] *\
                self.matoms.get_average_mass_atom(atom)
        elif mtype == "dis":
            for atom in atomslist:
                mass = mass + \
                self.matoms.get_maxbin_mass_atom(atom, atomslist[atom])
        return mass

    def l_mol_to_dmassmol(self, l_mol, mtype):
        """ Create a dictonnary with mass of some molecule as key
        and list of molecule ad arguments 
        @param l_mol: a list of molecule
        @type l_mol: list(Class with an argument as composition)
        @param mtype: a type of mass ("mono", "avg", "dis")
        @type mtype: string
        @return: a dict of list of molecule
        @rtype: dict(float: [mol])
        """
        dmassmol = {}
        for mol in l_mol:
            massmol = self.atomlist_to_masses(mol.composition, 
                                              mtype)
            dl_mol = dmassmol.get(massmol, [])
            dl_mol.append(mol)
            dmassmol[massmol] = dl_mol
        return dmassmol
    
    def subsequence_masse(self, lelems, mtype):
        """ """
        linkcompo = {'H': 1, 'C': 1, "N": 1, "O": 1}
        compo_base = mult_compo(linkcompo, len(lelems)-1)
        for elem in lelems:
            compo_base = combine_compo(elem.composition, compo_base)
        return self.atomlist_to_masses(compo_base, mtype)
        
    
    
#~ 
if __name__ == "__main__":
    pass
