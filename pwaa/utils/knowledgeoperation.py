#!/usr/bin/env python
# encoding: utf-8
""" knowledgeoperations.py is used to do operations on results of 
all creations from basicknowledge
@author: Vezin Aurelien
@license: CECILL-B
"""
import sys

from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.masses import dif_compo

def get_only_term(dlinks):
    """ get only the dlinks with a term side
    @param dlinks: a list of MDoubleLinks
    @type dlinks: list(MDoubleLinks)
    @return: MDoubleLinks with at least one term
    @rtype: list(MDoubleLinks)"""
    res = []
    for elem in dlinks:
        if (elem.joinside == "c-term" or elem.joinside == "n-term" 
                or elem.cutside == "c-term" or elem.cutside == "n-term"):
            res.append((elem.joinside, elem.cutside, elem.lcomplement))
    return res
    
def get_only_center(dlinks):
    """ get only the dlinks with only center elems 
    @param dlinks: a list of MDoubleLinks
    @type dlinks: list(MDoubleLinks)
    @return: MDoubleLinks with only center side
    @rtype: list(MDoubleLinks)"""
    res = []
    for elem in dlinks:
        if (elem.joinside != "c-term" and elem.cutside != "n-term" and
                elem.cutside != "c-term" and elem.joinside != "n-term"):
            res.append((elem.joinside, elem.cutside, elem.lcomplement))
    return res

def construct_all_lmass(mass, links):
    """ Construct all possible masses from a mass and a list of
    MDoubleLinks
    @param mass: a mass
    @type mass: float
    @param links: a list of MDoubleLinks
    @type links: MDoubleLinks
    @return: a list of tuple with mass, joinside, cutside, complement
    @rtype: list((float, str, str, str))
    """
    l_res = []
    for join, cut, compt in links:
        l_res.append((mass, join, cut, compt))
    return l_res

    
def get_diff_link_mass(links, cut, atoms=None):
    """ Get the links mass diff between the cut and the other cut
    of it side like for y-cut we have the diff masses with x-cut and
    z-cut
    @param links: A list of MSingleLinks
    @type links: list(MSingleLinks)
    @param cut: the cut at the center (c-cut, y-cut, etc ...)
    @type cut: string
    @return: the list of masses differences between the cut and 
    other cuts of its side
    @rtype: list(float)"""
    #search an element matching with the cut
    mamo = MAtomsMassOp(atoms)
    
    opposite = ""
    compo = {}
    for elem in links:
        if elem.link == cut:
            opposite = elem.opposite
            compo = elem.composition
            break
    dmassl = []
    #search only other links on the same side
    for elem in links:
        if (elem.opposite == opposite and  elem.complementary != None 
                and elem.link != cut):
            dcompo = dif_compo(elem.composition, compo)
            dmassl.append(mamo.atomlist_to_masses(dcompo, "mono"))
    return dmassl

def get_diff_link_mass_side(links, termside):
    """ Get the list of all list mass for a side.
    For example if you chose the termside as c-term you will have 
    the differences of masses from x-cut to y-cut and to z-cut, from 
    y-cut to x-cut and z-cut, from z-cut to x-cut and y-cut
    @param links: a list of MSingleLinks
    @type links: list(MSingleLinks)
    @param termside: the termside opposite to cut we want (c-term or n-term)
    @type termside: str
    @return: a list masses
    @rtype: list(masses)"""
    alldiffmass = []
    for elem in links:
        if elem.opposite == termside and elem.complementary != None:
            alldiffmass += get_diff_link_mass(links, elem.link)
    return alldiffmass
