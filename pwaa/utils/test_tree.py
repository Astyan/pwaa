#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_tree.py unittest for tree class 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys

sys.path.append('../')

from utils.tree import NTree, RecTree

class TestNTree(unittest.TestCase):
    """ class used to test NTree """
    
    def setUp(self):
        """ setUp for the test of NTree"""
        self.ntree = NTree(1)
    
    def test_10_value(self):
        """ test the value of the node """
        self.assertEqual(self.ntree.value, 1)
    
    def test_11_add_branch(self):
        """ test the adding of a branch"""
        self.assertEqual(len(self.ntree.branch), 0)
        self.ntree.add_branch(NTree(42))
        self.assertEqual(len(self.ntree.branch), 1)
    
    def test_11_change_par(self):
        """ test the change of a parent """
        self.assertEqual(self.ntree.partree, None)
        t42 = NTree(42)
        self.ntree.change_par(t42)
        self.assertEqual(self.ntree.partree, t42)

class TestRecTree(unittest.TestCase):
    """ Test used to test RecTree """
    
    def setUp(self):
        """ setUp for the test of RecTree"""
        self.rectree = RecTree(None, ["a1", "a2", "a3"], None, 2)
        self.assertEqual(self.rectree.value, None)
    
    def test_10_ldict_rec(self):
        """ Test of ldict_rec"""
        self.assertEqual(self.rectree.ldict_rec(),
                         [['a1', 'a1', None], ['a2', 'a1', None], 
                          ['a3', 'a1', None], ['a1', None], 
                          ['a2', 'a2', None], ['a3', 'a2', None], 
                          ['a2', None], ['a3', 'a3', None], 
                          ['a3', None], [None]])
                          
    def test_11_start_dict(self):
        """ test of start_dict"""
        self.assertEqual(self.rectree.start_dict(), 
                         [['a1', 'a1'], ['a2', 'a1'], ['a3', 'a1'], 
                          ['a1'], ['a2', 'a2'], ['a3', 'a2'], 
                          ['a2'], ['a3', 'a3'], ['a3']])
    
    def test_12_dict_len(self):
        """ test of dict_len"""
        self.assertEqual(self.rectree.dict_len(),
                         {1: [['a1'], ['a2'], ['a3']], 
                          2: [['a1', 'a1'], ['a2', 'a1'], 
                              ['a3', 'a1'], ['a2', 'a2'], 
                              ['a3', 'a2'], ['a3', 'a3']]})
    

if __name__ == '__main__':
    unittest.main()
