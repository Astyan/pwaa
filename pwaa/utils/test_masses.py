#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods 
"""test_masses.py unittest for models in masses.py 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys

sys.path.append('../')

from utils.masses import (nb_occurences_of, combine_compo, dif_compo,
                          mult_compo, in_ppm_range, ppm_sup_min,
                          ppm_inf_max, keep_only_similar,
                          ppm_to_appoxmass, ppm_to_approxmass2,
                          ppm_to_approxmass_fuzzy, int_round, 
                          float_round)

class TestAllFunc(unittest.TestCase):
    """test for all function in masses.py"""

    def test_10_nb_occurences_of(self):
        """test of nb_occurences_of"""
        elem = 'A'
        elemlist = ["A", "B", "C", "a", "A"]
        self.assertEqual(nb_occurences_of(elem, elemlist), 2)
    
    def test_10_int_round(self):
        """ test of int round"""
        self.assertEqual(int_round(45.45, 2), 4545)
    
    def test_10_float_round(self):
        """ test of float_round """
        self.assertEqual(float_round(10.1, 0.25), 10.)
        self.assertEqual(float_round(10.1, 0.1), 10.1)
        self.assertEqual(float_round(10.74, 0.25), 10.5)
        self.assertEqual(float_round(10.26, 0.25), 10.25)
        self.assertEqual(float_round(10.94, 0.25), 10.75)

    def test_12_combine_compo(self):
        """test combine_compo"""
        compo1 = {'A': 1, 'C': 4, 'D' : -1}
        compo2 = {'A': 1, 'D': 43}
        res = combine_compo(compo1, compo2)
        self.assertEqual(res, {'A': 2, 'C': 4, 'D' : 42})
    
    def test_12_dif_compo(self):
        """ test dif_compo """
        compo1 = {'A': 1, 'C': 4, 'D' : -1}
        compo2 = {'A': 1, 'C': 4, 'D' : 2}
        res = dif_compo(compo1, compo2)
        self.assertEqual(res, {'A': 0, 'C': 0, 'D': -3})
    
    def test_12_mult_compo(self):
        """ test mult_compo """
        compo = {'A': 1, 'C': 4, 'D' : -1}
        res = mult_compo(compo, 2)
        self.assertEqual(res, {'A': 2, 'C': 8, 'D' : -2})
        res = mult_compo(compo, -1)
        self.assertEqual(res, {'A': -1, 'C': -4, 'D' : 1})

    def test_12_in_ppm_range(self):
        """ test in_ppm_range """
        self.assertEqual(in_ppm_range(100., 100., 0), True)
        self.assertEqual(in_ppm_range(100., 104., 0), False)
        self.assertEqual(in_ppm_range(100., 100.001, 50), True)
        self.assertEqual(in_ppm_range(100., 100.005, 50), True)
        self.assertEqual(in_ppm_range(100., 100.006, 50), False)
        self.assertEqual(in_ppm_range(100., 99.995, 50), True)
        self.assertEqual(in_ppm_range(100., 99.994, 50), False)
        
    def test_12_ppm_sup_min(self):
        """ test ppm_sup_min """
        self.assertEqual(ppm_sup_min(100., 100., 50), True)
        self.assertEqual(ppm_sup_min(100., 101., 50), False)
    
    def test_12_ppm_inf_max(self):
        """ test ppm_inf_max """
        self.assertEqual(ppm_inf_max(100., 100., 50), True)
        self.assertEqual(ppm_inf_max(100., 99., 50), False)
    
    def test_12_keep_only_similar(self):
        """ test keep_only_similar """
        self.assertEqual(keep_only_similar(500.1343, 
                                           500.1543, 
                                           500.145345453), 500.1)
        self.assertEqual(keep_only_similar(500.13433, 
                                           500.13431, 
                                           500.145345453), 500.1343)
        self.assertEqual(keep_only_similar(500.1343, 
                                           500.1343, 
                                           500.145345453), 500.1343)
        self.assertEqual(keep_only_similar(500.13433, 
                                           499.9875, 
                                           499.9999), 500.)
        self.assertEqual(keep_only_similar(501.13433, 
                                           401.9875, 
                                           449.9999), 400.)
        self.assertEqual(keep_only_similar(501.13433, 
                                           401.9875, 
                                           450.0), 500.)
    
    
    def test_13_ppm_to_appoxmass(self):
        """ test ppm_to_appoxmass"""
        self.assertEqual(ppm_to_appoxmass(50, 1000.12345), 1000.0)
        self.assertEqual(ppm_to_appoxmass(200, 142.5555), 142.0)
        self.assertEqual(ppm_to_appoxmass(50, 142.12), 142.1)
        self.assertEqual(ppm_to_appoxmass(50, 242.144), 242.1)
        self.assertEqual(ppm_to_appoxmass(100, 1000.12345), 1000.0)
        self.assertEqual(ppm_to_appoxmass(100000, 10.0), 10.0)
        
    def test_13_ppm_to_approxmass2(self):
        """ test ppm_to_approxmass2 """
        self.assertEqual(ppm_to_approxmass2(50, 100.45, "theo"), 100.4)
        self.assertEqual(ppm_to_approxmass2(50, 100.42, "expe"), 100.4)
        self.assertEqual(ppm_to_approxmass2(50, 300.11, "theo"), 300.)
        self.assertEqual(ppm_to_approxmass2(50, 300.12, "expe"), 300.1)
    
    def test_13_ppm_to_approxmass_fuzzy(self):
        """ test ppm_to_approxmass_fuzzy """
        self.assertEqual(ppm_to_approxmass_fuzzy(50, 
                                                 100.28995, 
                                                 "theo"), 100.2)
        self.assertEqual(ppm_to_approxmass_fuzzy(50, 
                                                 100.28995, 
                                                 "expe"), 100.2)
        self.assertEqual(ppm_to_approxmass_fuzzy(50, 
                                                 100.28998, 
                                                 "theo"), 100.)
        self.assertEqual(ppm_to_approxmass_fuzzy(50, 
                                                 100.28998, 
                                                 "expe"), 100.)
        
    


if __name__ == '__main__':
    unittest.main()
