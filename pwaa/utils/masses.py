#!/usr/bin/env python
# encoding: utf-8
""" masses.py contain every function to do operations on masses
(compute, approximation, etc ...) 
@author: Vezin Aurelien
@license: CECILL-B"""

import math    

def nb_occurences_of(elem, elemlist): 
    """ Search the numbers of occurence of elem in elemlist
    @param elem: element to search to compute the occurence
    @type elem: any
    @param elemlist: list where to search elem
    @type elemlist: list
    @return: the number of occurences
    @rtype: int"""
    i = 0
    for el in elemlist:
        if el == elem:
            i = i + 1
    return i

def abound_glob(ana_abound, abound_list):
    """ give the global abound from a list of abound to analyse and
    the list of abound possible 
    @param ana_abound:  the list of abound to analyse
    @type ana_abound: list(float)
    @param abound_list: the list of abound possible
    @type abound_list: list(float)
    @return: the global abound
    @rtype: float
    """
    gabound = len(ana_abound)
    #computation of all permutation possibility |ana_abound|!/
    #for each abound in ana_abound : |abound|!
    gabound = math.factorial(gabound)
    for abound in abound_list:
        gabound = gabound / \
        math.factorial(nb_occurences_of(abound, ana_abound))
    for abound in ana_abound:
        gabound = gabound * abound
    return gabound


def combine_compo(compo1, compo2):
    """method to combine 2 composition in atoms
    @param compo1 : a dict of int/float as value. It should be a 
    composition in amino acid
    @type compo1 : dict(str :int)
    @param compo2 : a dict of int/float as value. It should be a 
    composition in amino acid
    @type compo2 : dict(str :int)
    @return : the combinaison of the 2 combinaison
    @rtype : dict(str :int)"""
    res = {k: compo1.get(k, 0) + compo2.get(k, 0) 
           for k in set(compo1) | set(compo2)}
    return res

def dif_compo(compo1, compo2):
    """method to do the diff between 2 compositions in atoms
    method to combine 2 composition in atoms
    @param compo1 : a dict of int/float as value. It should be a 
    composition in amino acid
    @type compo1 : dict(str :int)
    @param compo2 : a dict of int/float as value. It should be a 
    composition in amino acid
    @type compo2 : dict(str :int)
    @return : the difference between 2 combinaison
    @rtype : dict(str :int)"""
    res = {k: compo1.get(k, 0) - compo2.get(k, 0) 
           for k in set(compo1) | set(compo2)}
    return res

def mult_compo(compo, mult):
    """method to multiply value in compo with the number mult
    @precondition: work only with compo dict with int or float as
    value
    @param compo : a dict of int/float as value. It should be a 
    composition in amino acid
    @type compo : dict(str, int)
    @param mult : a multiplicator factor
    @type mult : int/float
    @return: the compo dict with value change for the previous
    value multiply with the mult factor
    @rtype: dict(str: int) """
    res = {k: compo.get(k, 0) * mult for k in set(compo)}
    return res

def in_ppm_range(massexpe, masstheo, ppm):
    """ check if the 2 mass could be equal with a ppm error 
    @param massexpe: is a mass of an experiment
    @type massexpe: float
    @param masstheo: is a theoric mass
    @type masstheo: float
    @param ppm: the value of the error in ppm
    @type ppm: int
    @return: True if the mass could be equal, if not return False
    @rtype: bool"""
    if (masstheo >= massexpe - (massexpe*ppm)/1000000 and
            masstheo <= massexpe + (massexpe*ppm)/1000000):
        return True
    else:
        return False

def ppm_sup_min(massexpe, masstheo, ppm):
    """check if the experimental mass is above the minimal 
    theorical mass with ppm delta
    @param massexpe: is a mass of an experiment
    @type massexpe: float
    @param masstheo: is a minimal theoric mass
    @type masstheo: float
    @param ppm: the value of the error in ppm
    @type ppm: int
    @return: True if the mass if above the minimal mass
    @rtype: bool """
    if masstheo <= massexpe + (massexpe*ppm)/1000000:
        return True
    else:
        return False

def ppm_inf_max(massexpe, masstheo, ppm):
    """check if the experimental mass is under the maximal 
    theorical mass with ppm delta
    @param massexpe: is a mass of an experiment
    @type massexpe: float
    @param masstheo: is a maximal theoric mass
    @type masstheo: float
    @param ppm: the value of the error in ppm
    @type ppm: int
    @return: True if the mass if under the minimal mass
    @rtype: bool """
    if masstheo >= massexpe - (massexpe*ppm)/1000000:
        return True
    else:
        return False
    
def keep_only_similar(mass1, mass2, mass):
    """ keep only the similar part between 2 approximate mass.
    If there is no similar part keep a round of the exact mass
    @param mass1: an approximate mass
    @type mass1: float
    @param mass2: an approximate mass
    @type mass2: float
    @param mass: an exact mass
    @type mass: float
    @return: a mass with only similar part
    @rtype: float"""
    mass1_str = str(mass1)
    mass2_str = str(mass2)
    for i in range(1, min(len(mass1_str), len(mass2_str))+1):
        if mass1_str[:i] != mass2_str[:i]:
            if i > 1:
                return float(mass1_str[:i-1])
            #avoid the case where there is no matching number
            else:
                numbers = 1 - len(str(int(abs(mass))))
                return round(mass, numbers)
    return float(mass1_str[:min(len(mass1_str), len(mass2_str))])

def ppm_to_appoxmass(ppm, mass):
    """ convert a mass in an approximate mass with x number after 
    comma
    @param ppm: the precision
    @type ppm: int
    @param mass: a mass (experimental or theorical)
    @type mass: float"""
    delta = (mass*ppm)/1000000.
    mmax = (mass + delta) + ((mass + delta)*ppm)/1000000.
    mmin = (mass - delta) - ((mass + delta)*ppm)/1000000.
    return keep_only_similar(mmin, mmax, mass)

def ppm_to_approxmass2(ppm, mass, mcat):
    """ an other version of ppm_to_approxmass.
    its approximate a mass with a better precision 
    by working differentlywhen approximate a theoricalmass 
    or an experimental mass.
    This is increasing the precision but more difference between
    a theorical peak and a experimental peak could be found 
    @param ppm: the error rate
    @type ppm: int
    @param mass: the mass to approximate 
    @type mass: float
    @param mcat: the category of the mass (expe or theo)
    @type mcat: str
    @return: an approximate mass
    @rtype: float """
    delta = ppm / 1000000.
    if mcat == "expe":
        mmin = mass - mass * delta
        mmax = mass + mass * delta
    elif mcat == "theo":
        mmin = mass /(1. - delta)
        mmax = mass /(1. + delta)
    else:
        raise ValueError('mcat incorret')
    return keep_only_similar(mmin, mmax, mass)
    
    
def ppm_to_approxmass_fuzzy(ppm, mass, mcat):
    """ do an approximation of a mass with a different computation
    if the mass is theorique or experimental
    @param ppm: the error rate
    @type ppm: int
    @param mass: the experimental or theorical exact mass
    @type mass: float
    @param mcat: the category of the mass (theo or expe)
    @type mcat: str
    @return: the approximate mass
    @rtype: float """
    delta = ppm / 1000000.
    if mcat == "expe":
        mmin = (mass - mass * delta) / (1. + delta)
        mmax = (mass + mass * delta) / (1. - delta)
    elif mcat == "theo":
        mmintmp = (mass / (1. + delta))
        mmin = mmintmp - mmintmp * delta
        mmaxtmp = (mass / (1. - delta))
        mmax = mmaxtmp + mmaxtmp * delta
    else:
        raise ValueError('mcat incorret')
    return keep_only_similar(mmin, mmax, mass)

    
def int_round(nbfloat, nbnumber):
    """ round a float to a integer (keep nbnumber number after 
    the comma) 
    @param nbfloat: a float number to round
    @type nbfloat: float
    @param nbnumber: the number of number after comma to keep
    @type nbnumber: int
    @return: int(round(nbfloat * (10 ** nbnumber)))
    @rtype: int """
    return int(round(nbfloat * (10 ** nbnumber)))
    
def float_round(nbfloat, roundvalue=0.25):
    """ make a floor to a float to another float with a given step
    for exemple 
    float_round(10.1, 0.25) => 10.0
    float_round(10.27, 0.25) => 10.25
    float_round(10.94, 0.25) => 10.75
    @param nbfloat: a float number to round 
    @type nbloat: float
    @param roundvalue: the step to apply
    @type roundvalue:  float
    @return: the float with the floor matching with the roundvalue step
    @rtype: float"""
    factor = round(1/roundvalue)
    nbfloat = math.floor(nbfloat*factor)
    return float(nbfloat/factor)
    


if __name__ == "__main__":
    pass
