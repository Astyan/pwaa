#!/usr/bin/env python
# encoding: utf-8
"""test_knowledgeoperation.py unittest for models in knowledgeoperation.py 
@author: Vezin Aurelien
@license: CECILL-B """
import unittest
import sys
sys.path.append('../')

from utils.knowledgeoperation import (get_only_term, get_only_center, 
                                      get_diff_link_mass, 
                                      get_diff_link_mass_side)

from basicknowledge.melemscreate import fill_all_link
from basicknowledge.mcombielemscreate import reason_all_doublelinks

class TestKnowledgeFunctions(unittest.TestCase):
    """ Class to test all KnowledgeFunctions"""
    
    @classmethod
    def setUpClass(cls):
        """ The init to lauch only once"""
        cls.single_link = fill_all_link()
        cls.d_links = reason_all_doublelinks(cls.single_link)
        
    
    def test_10_get_only_term(self):
        """ test of get_only_term"""
        res = get_only_term(self.d_links)
        self.assertEqual(len(res), 14)
        #~ for elem in res:
            #~ print elem
        
    def test_11_get_only_center(self):
        """ test of get_only_center"""
        res = get_only_center(self.d_links)
        self.assertEqual(len(res), 18)
    
    def test_12_get_diff_link_mass(self):
        """ test of get_diff_link_mass """
        res = get_diff_link_mass(self.single_link, "x-cut")
        self.assertAlmostEqual(res[0], -26.98709, 2)
        self.assertAlmostEqual(res[1], -43.005814, 2)

    def test_13_get_diff_link_mass_side(self):
        """ test of get_diff_link_mass_side"""
        res = get_diff_link_mass_side(self.single_link, "c-term")
        self.assertEqual(len(res), 6)
        
        
