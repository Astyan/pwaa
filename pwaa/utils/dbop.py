#!/usr/bin/env python
# encoding: utf-8
"""dbop contain usualy used operation for database
@author: Vezin Aurelien
@license: CECILL-B"""

import sys

from pony.orm import select, db_session

def aas_to_list(aas):
    """get a list of aa from string like "A,B,C,D" 
    @param aas: a string of aa with coma
    @type aas: string
    @return: a list of aa keys
    @rtype: list(char)"""
    aaslist = []
    for i in range(len(aas)):
        if aas[i] != ',':
            aaslist.append(aas[i])
    return aaslist

@db_session
def get_anything(lanything, table, key=1):
    """ get any db value with the list lanything on the table 
    @param lanything: a list of any kind of object
    @type lanything: list()
    @param table: the table class
    @type table: AminoAcid, IsoAtom, Atom, etc ...
    @param key: a key in the database 1 for symbol, 2 for name
    @type key: int (only 1 or 2)
    @return: a list of object in the database
    @rtype: list(any)"""
    listtemp = []
    listres = []
    for value in lanything:
        if key == 1:
            listtemp.append(select(inst for inst in table 
                                   if inst.symbol == value))
        elif key == 2:
            listtemp.append(select(inst for inst in table 
                                   if inst.name == value))
    for value in listtemp:
        for usable in value:
            listres.append(usable)
    return listres

@db_session
def get_or_set(table, **kwargs):
    """ Try to get an objet fron the table if it's not exist then create 
    it and return it. ! working with only one object to get at most
    @param table: the table class
    @type table: AminoAcid, IsoAtom, Atom, etc ...
    @param kwargs:  the named parameter to get the object
    @type kwargs: any
    @return: the object get or create
    @rtype: any
    """
    res = table.get(**kwargs)
    if res == None:
        res = table(**kwargs)
    return res
