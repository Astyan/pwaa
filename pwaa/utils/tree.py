#!/usr/bin/env python
# encoding: utf-8
""" tree.py contain a way to create a tree with any number of branch
@author: Vezin Aurelien
@license: CECILL-B"""

import copy

class NTree(object):
    """A tree with a variable number of branch
    @ivar value: the value of the node
    @type value: any
    @ivar branch: the list of all branch of the tree
    @type branch: list(Ntree)
    @ivar partree: the parent of the tree
    @type partree: None/NTree"""
    
    def __init__(self, value=None, partree=None):
        """ add the value to the node and do a link to a 
        parent
        @param value: the value of the node
        @type value: any
        @param partree: the parent of the tree
        @type partree: None/NTree"""
        self.value = value
        self.branch = []
        self.partree = partree
    
    def add_branch(self, n_branch):
        """ add a branch to the node
        @param n_branch: the branch to add
        @type n_branch: NTree"""
        n_branch.partree = self
        self.branch.append(n_branch)
    
    def change_par(self, par):
        """ change the parent of the tree
        @param par: the new parent
        @type par: NTree
        """ 
        self.partree = par
        #adding the branch to the par
        par.branch.append(self)
            

class RecTree(NTree):
    """ RecTree is a tree to contruct list recursively
    @ivar value: the value of the node
    @type value: any
    @ivar branch: the list of all branch of the tree
    @type branch: list(Ntree)
    @ivar partree: the parent of the tree
    @type partree: None/NTree
    @ivar prof: the deep left
    @type prof: int"""
    
    def __init__(self, value=None, lvalue=None, partree=None, prof=0):
        """create the tree recursively
        @param value: the value of the node
        @type value: any
        @param lvalue: list of value to give to the node
        @type lvalue: list(any)
        @param partree: the parent tree
        @type partree: RecTree
        @param prof: the deep of the tree to create
        @type prof: int"""
        NTree.__init__(self, value, partree)
        self.prof = prof
        if lvalue != None and prof != 0:
            for i in range(0, len(lvalue)):
                child = RecTree(lvalue[i], lvalue[i:], self, prof-1)
                self.branch.append(child)
    
    def start_dict(self):
        """ create a list of all elements in childs ordered with a 
        deep exploration
        @return: a list of list of elements in each branch
        @rtype: list(list(any))"""
        l_res = []
        for child in self.branch:
            res = child.ldict_rec()
            l_res = l_res + res
        return l_res
            
    
    def ldict_rec(self):
        """ create a list of all element with the current node value 
        everywere (deep recursive exploration)
        @return: a list of list of elements in each branch and this node
        @rtype: list(list(any))
        """
        lres = []
        for child in self.branch:
            res = child.ldict_rec()
            for elem in res:
                eltmp = copy.deepcopy(elem)
                eltmp.append(self.value)
                lres.append(eltmp)
        lres.append([self.value])
        return lres

    def dict_len(self):
        """create a dict with a key for each len 
        and a list of all combinaision of elem for the len
        @return: a dict of len with list of combinaison of elems
        @rtype: dict(int: list(any))"""
        dres = {}
        lin = self.start_dict()
        for elem in lin:
            dl = dres.get(len(elem), [])
            dl.append(elem)
            dres[len(elem)] = dl
        return dres
            

if __name__ == "__main__":
    #~ TREE = NTree(1)
    
    RTREE = RecTree(None, ["a1", "a2", "a3"], None, 4)
    print RTREE.dict_len()
    #~ print TREE.value
    #~ print TREE.branch
    
