#!/usr/bin/env python
# encoding: utf-8
"""genetic_learning.py read the data in the leaning data csv 
@author: Vezin Aurelien
@license: CECILL-B"""

import array, random
import os
import sys
import pickle
import copy
import time

from pymultievolve.evaluation import GenericEvaluation
from pymultievolve.crossover import CX
from pymultievolve.mutation import Mut
from pymultievolve.individual import Population
#for the exemple
from pymultievolve.reduction import SelTournamentNoRemise
from pymultievolve.algorithm import GeneticAlgorithm

from pwaa.analyser.enrich_graph import EnrichGraph, WeightGraph
from pwaa.basicknowledge.melemscreate import (fill_all_link, 
                                              fill_all_aa, 
                                              fill_all_residue)
from pwaa.basicknowledge.mcombielemscreate import (reason_all_doublelinks, 
                                                   reason_all_aaresidue)
from pwaa.analyser.valid_graph import ValidGraph
from pwaa.analyser.mztom import MassSpectrum
from pwaa.analyser.mass_elems import masses_bins_elems
from pwaa.analyser.residue_search import ResidueSearch
from pwaa.evaluation.subsequence_probability import EdgeWeight, PeakWeight
from pwaa.learning.csv_data import get_metadata
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.analyser.theorical_spectrum import TheoricalSpectrum
from pwaa.evaluation.matching_spectrum import GAScore
from pwaa.basicknowledge.mcombielemsoperation import LinksOperations


def sequences_translation(sequences):
    """ transform elems sequences in str sequences
    @param sequences: list of sequence of elems
    @type sequences: list(list(MSingleAA or MSingleAAPTM))
    @return: list of sequences in str
    @rtype: list(str) """
    tsequences = []
    for sequence in sequences:
        tsequence = ""
        for elem in sequence:
            tsequence = tsequence + elem.aa #elem.aa because we don't learn on sequence with ptm
        tsequences.append(tsequence)
    return tsequences
 
def strseq_to_listseq(strseq, elems):
    """ transform a sequence in str in a sequence of elems
    @param strseq: a sequence in str
    @type strseq: str
    @param elems: a list of elems (MSingleAA or MSingleAAPTM)
    @type elems: list(MSingleAA or MSingleAAPTM)
    @return: a sequence of elems
    @rtype: list(MSingleAA or MSingleAAPTM)"""
    listseq = []
    for aakey in strseq:
        for elem in elems:
            if elem.aa == aakey:
                listseq.append(elem)
    return listseq

def create_all_valid_graph(fcsv, fmgf, massposbins=None, atoms=None, to_open=True):
    """create all the valid graph
    @param fcsv: the path to the csv file
    @type fcsv: str
    @param fmgf: the path to the mgf file
    @type fmgf:str
    @return: the dict of the graph and peaks with the position as key 
    @rtype: dict(int: (Valid_Graph, list()))
    """
    valid_graphs = {}
    links = fill_all_link()
    dlinks = reason_all_doublelinks(links)
    if massposbins == None:
        massposbins = pickle.load(open("massbinposs", "rb"))
    dcsv = get_metadata(fcsv, to_open=to_open)
    mass_spectrum = MassSpectrum(fmgf, to_open=to_open, atoms=atoms)
    for key in dcsv:
        if dcsv[key]["Modifications"] == '':
            mass_spectrum.set_ion(int(key)-1)
            peaks = mass_spectrum.converted_peak()
            valid_graph = ValidGraph(dlinks, peaks, 600.0, atoms=atoms)
            valid_graph.insert_peakgraph_nodes()
            #avoid learning on too big spectrum
            #~ if valid_graph.peakgraph.number_of_nodes() <= 1250:
            valid_graph.insert_edges(massposbins)
            valid_graph.delete_no_path()
            valid_graph.delete_solitary_nodes()
            valid_graphs[key] = (valid_graph, peaks)
    return valid_graphs
            
def create_all_simple_gaph(fcsv, fmgf, links, atoms=None, to_open=False):
    """ create the graph nodes
    @param fcsv: the path to the csv file
    @type fcsv: str
    @param fmgf: the path to the mgf file
    @type fmgf:str
    @param links: a list of links
    @type links: list(Links)
    @param atoms: a list of atoms
    @type atoms: list(Atoms)
    @param to_open: if we have to open the files
    @type to_open: Bool
    @return: the dict of the graph and peaks with the position as key 
    @rtype: dict(int: (ValidGraph, list()))
    """
    simple_graphs = {}
    dlinks = reason_all_doublelinks(links)
    dcsv = get_metadata(fcsv, to_open=to_open)
    mass_spectrum = MassSpectrum(fmgf, to_open=to_open, atoms=atoms)
    for key in dcsv:
        if dcsv[key]["Modifications"] == '':
            mass_spectrum.set_ion(int(key)-1)
            peaks = mass_spectrum.converted_peak()
            simple_graph = ValidGraph(dlinks, peaks, 600.0, atoms=atoms)
            simple_graph.insert_peakgraph_nodes()
            simple_graphs[key] = (simple_graph, peaks)
    return simple_graphs
    

def uniformseq(minimal, maximal, step):
    """ create a list of possibility 
    @param minimal: the minimal value
    @type minimal: float
    @param maximal: the maximal value
    @type maximal: float
    @param step: the step value
    @type step: float
    @return: list of possibily
    @rtype: list(float)
    """
    res = [minimal]
    cur = minimal  
    while cur < maximal:
        cur += step
        res.append(cur)
    return res

class EvaluationGlobal(GenericEvaluation):
    
    
    def __init__(self, simple_graphs, aas, links, residues, 
                 proteasecuts=None, atoms=None, nb_peaks=750, 
                 nb_paths=200, massposbins=None, ppm=50):
        """ init of the genetic algorithm with save of every 
        @param valid_graphs: a dict with peaks and ValidGraph
        @type valid_graphs: dict(int: (Valid_Graph, list()))
        """
        if massposbins == None:
            self.massposbins = pickle.load(open("massbinposs", "rb"))
        else:
            self.massposbins = massposbins
        self.simple_graphs = simple_graphs
        self.mbinelems = masses_bins_elems(aas, 1, 0.01)
        self.lop = LinksOperations(links)
        self.dlinks = reason_all_doublelinks(links)
        self.dcsv = None
        self.atoms = atoms
        self.nb_peaks = nb_peaks
        self.nb_paths = nb_paths
        self.ppm = ppm
        #~ self.to_open = to_open
        if proteasecuts == None:
            self.proteasecuts = []
        else:
            self.proteasecuts = proteasecuts
        self.aas = aas
        self.residues = reason_all_aaresidue(self.aas, residues)
        
    
    def open_csv(self, fcsv, to_open=True):
        """ open the csv file
        @param fcsv: path to the csv
        @type fcsv: str"""
        self.dcsv = get_metadata(fcsv, to_open=to_open)
    
    def get_ldistance(self, indice, individual_dna, knowed_seq):
        """ compute the score of an individual on a spectrum"""
        simple_graph = copy.deepcopy(self.simple_graphs[indice][0])
        #search the immoniums
        rs = ResidueSearch(self.simple_graphs[indice][1], self.ppm, #ptm 
                           self.residues, atoms=self.atoms)
        immoniums = rs.get_residue_masses()
        #add weight on the peaks
        eg = EnrichGraph(simple_graph.get_peakgraph(), self.ppm, #ptm
                         atoms=self.atoms) 
        eg.enrich_peaks()
        
        pw = PeakWeight()
        pw.peakweight(individual_dna["peakselection"][0],
                      individual_dna["peakselection"][1],
                      individual_dna["peakselection"][2],
                      individual_dna["peakselection"][3],
                      individual_dna["peakselection"][4],
                      individual_dna["cutselection"],
                      individual_dna["peakselection"][5])
        wg = WeightGraph(eg.peakgraph, peakweight=pw)
        wg.compute_peak_weight()
        #select the best peaks
        graph = wg.select_best_peak(lim=self.nb_peaks) #limit
        #add edge and weight on the edges
        valid_graph = ValidGraph(self.dlinks, 
                                 self.simple_graphs[indice][1], 600.0, #massbinpos limit
                                 graph, atoms=self.atoms)
        #~ print valid_graph.get_peakgraph().nodes()
        valid_graph.insert_edges(self.massposbins)
        valid_graph.delete_no_path()
        valid_graph.delete_solitary_nodes()
        #~ print valid_graph.get_peakgraph().edges()
        eg = EnrichGraph(valid_graph.get_peakgraph(), self.ppm, atoms=self.atoms) #ptm
        eg.enrich_edges(immoniums, self.proteasecuts)
        ew = EdgeWeight()
        ew.peakweight(individual_dna["peakweight"][0],
                      individual_dna["peakweight"][1],
                      individual_dna["peakweight"][2],
                      individual_dna["peakweight"][3],
                      individual_dna["peakweight"][4],
                      individual_dna["cutpeak"],
                      individual_dna["peakweight"][5])
        ew.edgeweight(individual_dna["edgeweight"][0],
                      individual_dna["edgeweight"][1],
                      individual_dna["edgeweight"][2])
        wg = WeightGraph(eg.peakgraph, edgeweight=ew)
        wg.compute_edge_weight()
        #keep the best paths
        graph = wg.select_best_path(lim=self.nb_paths) #select only the best paths
        #insert the possibilites
        valid_graph = ValidGraph(self.dlinks, 
                                 self.simple_graphs[indice][1], 
                                 600.0, graph, atoms=self.atoms) #limit for the massbinpos
        valid_graph.insert_possibility(self.mbinelems, 0.01, self.ppm) #50 ppm, step of 0.01
        #~ print valid_graph.get_peakgraph().nodes(), valid_graph.get_peakgraph().edges()
        #get all the spectrums
        #~ print valid_graph.get_peakgraph().edges()
        strong_spectrums = valid_graph.get_simple_spectrums(r_intensity=True)
        weak_spectrums = valid_graph.get_simple_spectrums2(r_intensity=True)
        #get the theorical spectrum
        knowed_seq = strseq_to_listseq(knowed_seq, self.aas)
        tsverified = TheoricalSpectrum(knowed_seq, self.lop, atoms=self.atoms)
        verified_spectrum = tsverified.compute_all_peaks(intensity=True)
        #~ print verified_spectrum
        #compute weak score
        weak_score = 0.
        for individual_spectrum in weak_spectrums:
            #~ print individual_spectrum
            #~ tsevaluated = TheoricalSpectrum(knowed_seq, self.lop)
            #~ individual_spectrum = tsevaluated.compute_all_peaks(True)
            #compute the distance between the real chaine and the 
            gascore = GAScore(individual_spectrum, verified_spectrum)
            weak_score = min(weak_score, gascore.compute_score(self.ppm)) #gascore is negativ
        #compute strong score
        strong_score = 0.
        for individual_spectrum in strong_spectrums:
            #~ print individual_spectrum
            #~ tsevaluated = TheoricalSpectrum(knowed_seq, self.lop)
            #~ individual_spectrum = tsevaluated.compute_all_peaks(True)
            #compute the distance between the real chaine and the 
            gascore = GAScore(individual_spectrum, verified_spectrum)
            strong_score = min(strong_score, gascore.compute_score(self.ppm)) #gascore is negativ
        score = 0.9 * strong_score + 0.1 * weak_score
        return score
    
    def evaluate(self, individual_dna):
        """ compute the score of an individual 
        @param individual: the individual genome
        @type individal: dict(str: dict(str: float) or list(float))
        @return: the score
        @rtype: (float,)
        @rtype: (float,)
        """
        cumulated_dist = 0.
        print "NEW INDIVIDUAL"
        for key in self.dcsv:
            if self.dcsv[key]["Modifications"] == '':
                knowed_seq = self.dcsv[key]["Sequence"] #dans le bon sens normalement
                #~ knowed_seq = knowed_seq.split('.')[1]
                #temporary test
                #~ print "New Spectrums"
                #~ print key
                #~ print knowed_seq
                #~ knowed_seq = knowed_seq[::-1]
                #~ simple_graph = copy.deepcopy(self.simple_graphs[key][0])
                #~ vtmpspectrum = simple_graph.peaks.keys()
                #~ vvtmpspectrum = [ (mass, 1.) for mass in vtmpspectrum ]
                #~ knowed_seq = strseq_to_listseq(knowed_seq, self.aas)
                #~ print self.atoms
                #~ tsverified = TheoricalSpectrum(knowed_seq, self.lop, atoms=self.atoms) #lop or atoms not good
                #~ verified_spectrum = tsverified.compute_all_peaks(intensity=True)
                #~ print sorted(vvtmpspectrum)
                #~ print sorted(verified_spectrum)
                #~ gascore = GAScore(vvtmpspectrum, verified_spectrum)
                #~ print gascore.compute_score(self.ppm)
                #end temporary test
                cumulated_dist += self.get_ldistance(key, individual_dna, knowed_seq)
        #~ raise Exception('because the end')
        return cumulated_dist
    

class ParameterPopulation(Population):
    
    def dna_generation(self):
        dna = {}
        dna["cutpeak"] = self.cut_parameter()
        dna["edgeweight"] = self.edge_parameter()
        dna["peakweight"] = self.peak_parameter()
        dna["cutselection"] = self.cut_parameter()
        dna["peakselection"] = self.peak_parameter()
        return dna
    
    def cut_parameter(self):
        dres = {}
        mini = -1
        maxi = 1
        step = 0.01
        dres["n-term"] = random.choice(uniformseq(mini, maxi, step))
        dres["c-term"] = random.choice(uniformseq(mini, maxi, step))
        dres["c-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["b-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["a-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["x-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["y-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["z-cut"] = random.choice(uniformseq(mini, maxi, step))
        dres["None"] = random.choice(uniformseq(mini, maxi, step))
        return dres
    
    def edge_parameter(self):
        return [ random.choice(uniformseq(-10, 10 ,0.05)) for i in range(0, 3) ]
    
    def peak_parameter(self):
        return [ random.choice(uniformseq(-10, 10 ,0.05)) for i in range(0, 6) ]
    
class CrossOver(CX):
    
    def crossover(self, individual1, individual2):
        ind1 = individual1.dna
        ind2 = individual2.dna
        nind1 = ind1
        nind2 = ind2
        chromosomes = ["cutpeak", "edgeweight", "peakweight", 
                       "peakselection", "cutselection"]
        for chromosome in chromosomes:
            if self.indcross <= random.random():
                    nind1[chromosome] = ind2[chromosome]
                    nind2[chromosome] = ind1[chromosome]
        individual1.dna = nind1
        individual2.dna = nind2
        return (individual1, individual2)

class Mutation(Mut):
    
    def mute(self, individual):
        """ mutation of an individual
        @param individual: an individual genome 
        @type individual: Individual      
        @return: the modified individual
        @rtype: individual
        """
        muted = False
        chromosomes = ["cutpeak", "edgeweight", "peakweight", 
                       "peakselection", "cutselection"]
        for chromosome in chromosomes:
            if chromosome == "cutpeak" or chromosome == "cutselection":
                if random.random() < self.indpb:
                    for elem in individual.dna[chromosome]:
                        individual.dna[chromosome][elem] = random.choice(uniformseq(-1, 1, 0.01))
                        muted = True
            else:
                for i in range(0, len(individual.dna[chromosome])):
                    if random.random() < self.indpb:
                        individual.dna[chromosome][i] = random.choice(uniformseq(-10, 10 ,0.02))
                        muted = True
        return individual, muted


if __name__ == "__main__":
    aas = fill_all_aa()
    links = fill_all_link()
    residues = fill_all_residue()
    valid_graphs = create_all_simple_gaph(os.path.join(".", "file", sys.argv[1]+".csv"),
                                          os.path.join(".", "file", sys.argv[1]+".mgf"),
                                          links=links,  to_open=True)
                                        
    population = ParameterPopulation()
    poptosend = population.run(10)

    evaluation = EvaluationGlobal(valid_graphs, aas, links, residues)
    evaluation.open_csv(os.path.join(".", "file", sys.argv[1]+".csv"))
    reduction = SelTournamentNoRemise(goal="minimize", tournsize=2)
    crossover = CrossOver()
    mutation = Mutation(indpb=0.1)
    
    ga = GeneticAlgorithm(mutation=mutation, crossover=crossover, 
                          reduction=reduction, evaluation=evaluation, 
                          population=poptosend, population_size=10, nb_generation=4,
                          mutation_probability=0.2, crossover_probability=0.5,
                          nb_process=1, do_stats=True)
    ga.run()

    

