#!/usr/bin/env python
# encoding: utf-8
""" test_csv_data.py is a class to test csv_data.py"""


import unittest
import os, sys, copy

from pymultievolve.algorithm import GeneticAlgorithm
from pymultievolve.reduction import SelTournamentNoRemise


from pwaa.learning.genetic_learning import (EvaluationGlobal, 
                                            ParameterPopulation, 
                                            CrossOver, 
                                            Mutation,
                                            create_all_simple_gaph)
from pwaa.basicknowledge.melemscreate import fill_all_aa, fill_all_link 


class TestGeneticLearnig(unittest.TestCase):
    
    
    def test_10_geneticlearning(self):
        """ """
        links = fill_all_link()
        valid_graphs = create_all_simple_gaph(open(os.path.join(".","learning", 
                                                                "file", "exemple.csv")),
                                              open(os.path.join(".", "learning", 
                                                                 "file", "exemple.mgf")),
                                              links=links, to_open=False)
        
        population = ParameterPopulation()
        poptosend = population.run(2)
        #~ print population
        evaluation = EvaluationGlobal(valid_graphs, fill_all_aa(), fill_all_link(), [], [])
        evaluation.open_csv(open(os.path.join(".", "learning", "file", "exemple.csv")), to_open=False)
        reduction = SelTournamentNoRemise(goal="minimize", tournsize=7)
        crossover = CrossOver(indcross=0.01)
        mutation = Mutation(indpb=0.01)
        
        ga = GeneticAlgorithm(mutation=mutation, crossover=crossover, 
                              reduction=reduction, evaluation=evaluation, 
                              population=poptosend, population_size=2, nb_generation=4,
                              mutation_probability=1., crossover_probability=1.,
                              nb_process=1, do_stats=True)
        ga.run()


if __name__ == '__main__':
    unittest.main()
