#!/usr/bin/env python
# encoding: utf-8
"""csv_data.py read the data in the leaning data csv 
@author: Vezin Aurelien
@license: CECILL-B"""

import csv
import os
import sys

def get_metadata(csvfile, to_open=False):
    """ """
    if to_open:
        csvfile = open(csvfile)
    csvfile = csv.DictReader(csvfile)
    result = {}
    for key in csvfile:
        result[key["Row"]] = key
    return result
        
