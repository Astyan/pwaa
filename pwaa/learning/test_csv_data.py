#!/usr/bin/env python
# encoding: utf-8
""" test_csv_data.py is a class to test csv_data.py"""

import unittest
import os, sys
#~ print os.getcwd()

from pwaa.learning.csv_data import get_metadata


class TestLearningMetaData(unittest.TestCase):
    
    
    def test_10_get_metadata(self):
        """ """
        print get_metadata(open(os.path.join(".", "learning", 
                                             "file", "exemple.csv")))


if __name__ == '__main__':
    unittest.main()
