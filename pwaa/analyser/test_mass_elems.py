#!/usr/bin/env python
# encoding: utf-8
""" test_mass_elem is used test mass_elems file 
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys


from pwaa.analyser.mass_elems import masses_bins_elems
from pwaa.basicknowledge.melemscreate import fill_all_aa


class TestMassesBinsElems(unittest.TestCase):
    """ The class to test masses_bins_elems"""
    
    def test_masses_bins_elems(self):
        """ verify the theorical size of a value"""
        laa = fill_all_aa()
        res = masses_bins_elems(laa, 3, 0.01)
        self.assertEqual(len(res[224.15]), 3)
