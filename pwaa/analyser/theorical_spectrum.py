#!/usr/bin/env python
# encoding: utf-8
"""theorical_spectrum.py contain everything to construct a 
theorical spectrum from a sequence of aaptm
there is also something to give a matching percent between
2 spectrums
@author: Vezin Aurelien
@license: CECILL-B"""

import sys
from sets import Set

#use from mcombielems aaptm
#use from mcombielemscreate links
from pwaa.utils.masses import combine_compo, mult_compo, dif_compo
from pwaa.utils.atoms import MAtomsMassOp
        


class TheoricalSpectrum(object):
    """ TheoricalSpectrum is abble to create a theorical spectrum
    from a sequence.
    It's create all cuts peak and the peptide peak.
    @ivar sequence: the sequence to create the spectrum
    @type sequence: list(MSingleAAPTM)
    @ivar llinks: a class to do operations on link
    @type llinks: LinksOperations
    @ivar mtype: the type of masse (mono, avg, iso)
    @type mtype: str
    @ivar peaks: a list of simulated peak mass
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp
    """
    #don't compute the weird cut (at the end of the sequence close to
    #the n-term or c-term
    
    def __init__(self, sequence, llinks, mtype="mono", atoms=None):
        """ Save all the param in class attribut and init 
        other attribute
        @param sequence: the sequence to create the spectrum
        @type sequence: list(MSingleAAPTM)
        @param llinks: a class to do operations on link
        @type llinks: LinksOperations
        @param mtype: the type of masse (mono, avg, iso)
        @type mtype: str
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(MAtoms)"""
        self.mamo = MAtomsMassOp(atoms)
        self.sequence = sequence
        self.mtype = mtype
        self.llinks = llinks
        self.peaks = []
        
    def mass_links_subsequence(self, subsequence, side, intensity=False):
        """create mass link for a subsequence
        @param subsequence: a subsequence of a sequence 
        @type subsequence: list(ChimicalElement)
        @param side: the side with the one we start ("c-term" or "n-term")
        @type side: str
        @param intensity: use the intensity or not in the created spectrum
        @type intensity: Boolean
        @return: the list of mass found for each cut
        @rtype: list(float)"""
        masses = []
        basiccompo = self.llinks.middle
        #~ print termcompo
        if side == "c-term":
            links = self.llinks.clinks
            termcompo = self.llinks.get_link("n-term")
        else:
            links = self.llinks.nlinks
            termcompo = self.llinks.get_link("c-term")
        links_compo = mult_compo(basiccompo, len(subsequence) - 1)
        compowithoutcut = combine_compo(links_compo, termcompo)
        #do the compo for all cut with 
        for link in links:
            masstmp = 0.
            cutcompo = combine_compo(link.composition, 
                                     dif_compo(link.composition, 
                                               link.unprot))
            compotmp = combine_compo(compowithoutcut, cutcompo)
            for elem in subsequence:
                #adding of subsequences but nothing if there is no composition
                try:
                    compotmp = combine_compo(compotmp, elem.composition)
                except Exception:
                    masstmp += elem
            masstmp += self.mamo.atomlist_to_masses(compotmp, self.mtype)
            if intensity == False:
                masses.append(masstmp)
            else:
                if link.link == "y-cut":
                    masses.append((masstmp, 0.9))
                elif link.link == "b-cut":
                    masses.append((masstmp, 0.8))
                elif link.link == "a-cut":
                    masses.append((masstmp, 0.5))
                else:
                    masses.append((masstmp, 0.1))
        return masses

    def mass_sequence(self, intensity=False):
        """ the mass of the full sequence from n-term to c-term
        @param intensity: use the intensity or not in the created spectrum
        @type intensity: Boolean
        @return: the mass of the full sequence in a list
        @rtype: list(float)"""
        masses = []
        basiccompo = self.llinks.middle
        ntermcompo = self.llinks.get_link("n-term")
        ctermcompo = self.llinks.get_link("c-term")
        links_compo = mult_compo(basiccompo, len(self.sequence) - 1)
        compotmp = combine_compo(links_compo, ntermcompo)
        compotmp = combine_compo(compotmp, ctermcompo)
        masstmp = 0.
        for elem in self.sequence:
            # try here
            try:
                compotmp = combine_compo(compotmp, elem.composition)
            except Exception:
                masstmp += elem
        masstmp += self.mamo.atomlist_to_masses(compotmp, self.mtype)
        if intensity == False:
            masses.append(masstmp)
        else:
            masses.append((masstmp, 1.))
        return masses
    
        
    def compute_all_peaks(self, intensity=False):
        """compute all possible peak we could see
        ! right now there is no weird cut (the cut in c-term or n-term)
        @param intensity: use the intensity or not in the created spectrum
        @type intensity: Boolean
        @return: the list of the masses of all possible cuts
        @rtype: list(mass)"""
        self.peaks = []
        if intensity == False:
            for i in range(1, len(self.sequence)):
                nside = self.sequence[0:i]
                cside = self.sequence[i: len(self.sequence)]
                cside = list(reversed(cside))
                massestmp = self.mass_links_subsequence(cside, "n-term")
                self.peaks += massestmp
                massestmp = self.mass_links_subsequence(nside, "c-term")
                self.peaks += massestmp
            masstmp = self.mass_sequence()
            self.peaks += masstmp
            return self.peaks
        else:
            #with the intensity
            for i in range(1, len(self.sequence)):
                nside = self.sequence[0:i]
                cside = self.sequence[i: len(self.sequence)]
                cside = list(reversed(cside))
                massestmp = self.mass_links_subsequence(cside, "n-term", intensity)
                self.peaks += massestmp
                massestmp = self.mass_links_subsequence(nside, "c-term", intensity)
                self.peaks += massestmp
            masstmp = self.mass_sequence(intensity)
            self.peaks += masstmp
            return self.peaks




