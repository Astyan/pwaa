#!/usr/bin/env python
# encoding: utf-8
"""test_mztom.py unittest for models in mztom.py 
@author: Vezin Aurelien
@license: CECILL-B"""
import unittest
import os, sys

from pwaa.analyser.mztom import MassSpectrum

INITPATH = os.path.join("..", "filestest")


class TestMassSpectrum(unittest.TestCase):
    """test for MassSpectrum class"""
    
    def setUp(self):
        """init the MassSpectrum class"""
        self.massspectrum = MassSpectrum(os.path.join("analyser", 
                                                      "files", 
                                                      "bsa.mgf"))
        self.massfactice = MassSpectrum(os.path.join("analyser", 
                                                      "files", 
                                                      "factice.mgf"))
        
        
    def tearDown(self):
        """Clear after each tests"""
        pass
    
    def test_10_ion_charges(self):
        """test of _ions_charges"""
        self.assertEqual(self.massspectrum._ion_charges(), [1, 2])
        self.massspectrum.next_ion()
        self.assertEqual(self.massspectrum._ion_charges(), [1, 2])
        
    def test_11_converted_peak(self):
        """test the convertion of a peak"""
        converted = self.massspectrum.converted_peak()
        self.assertEqual(converted[0.0], {'intensity': 423, 
                                          'nbcharge': 1, 'charge': 1, 
                                          'begin_or_end': True,
                                          'rmz': 0})
        converted = self.massfactice.converted_peak()
        lres = []
        for k in converted:
            lres.append(k)
        lres = sorted(lres)
        self.assertEqual(lres, [0.0, 44.05007, 72.04498, 89.07153, 
                                105.00107, 122.02762, 147.05925, 
                                148.00688, 175.05417, 176.03818, 
                                192.056905, 193.06473])
    
    def test_12_get_sequence(self):
        """ test of get_sequence """
        self.assertEqual([], self.massspectrum.get_sequence())
        self.massspectrum.next_ion()
        self.assertEqual(['A', 'Z', 'E', 'R', 'T', 'Y'], 
                         self.massspectrum.get_sequence())

if __name__ == '__main__':
    unittest.main()
