#!/usr/bin/env python
#encoding: utf-8
#pylint: disable=multiple-statements, too-many-statements
""" 
This file contain everything to do a search from a list of mass
to a partial sequence
@author: Vezin Aurelien
@license: CECILL-B
"""
import sys

from swiglpk import (intArray, doubleArray, glp_create_prob, 
                     glp_set_obj_dir, glp_set_prob_name, glp_add_rows,
                     glp_set_row_name, glp_set_row_bnds, glp_add_cols,
                     glp_set_col_name, glp_set_col_bnds, 
                     glp_set_col_kind, glp_set_obj_coef,
                     glp_load_matrix, glp_simplex, glp_intopt,
                     glp_delete_prob, glp_mip_col_val, glp_term_out,
                     #~ lpx_set_real_parm, LPX_K_TMLIM, GLP_UP,
                     GLP_OFF, GLP_MIN, GLP_MAX, GLP_DB, GLP_IV, GLP_UP,
                     GLP_LO, GLP_FX)

#~ from utils.masses import float_round

class GLPKException(Exception):
    """ A special exception for GLPK """
    pass

class IntegerSearch(object):
    """ This class is used to give a possible solution in bounderies
    given by a mass and a ppm.
    It's used an integer programing librairy
    @ivar masseslist: list of masses to use as coef of the lineare 
    function
    @type masseslist: list(float)
    @ivar mass: a modify mass to reach (modify to search 
    with links we want (+(CONH) - the two link we want)
    @type mass: float
    @ivar ppm: the precision we want (in ppm)
    @type ppm: int
    @ivar sizelim: the limite size of the sequence
    @type sizelim: int
    @ivar ia: the row indice array (indice of constraint function) 
    of the matrix (like the librairy is a wrapper 
    it's an array of int in C)
    @type ia: intArray
    @ivar ja: the column indice array (indice of coef to multiply
    with masses) of the matrix (like the librairy is a wrapper 
    it's an array of int in C)
    @type ja: intArray
    @ivar ar: a array of masses (like the librairy is a wrapper it's
    an array of double in C)
    @type ar: doubleArray
    @ivar mip: the interger programming problem equation and metadata
    @type mip: GLPK structure (C)
    """ 
    
    def __init__(self, masseslist, mass, cutmass=43.08504, 
        ppm=None, sizelim=None):
        """ Create the array for the matrice and save param in
        instance var
        @param masseslist: list of masses we want consider
        @type masseslist: list(float)
        @param mass: the mass we want reach
        @type mass: float
        @param ppm: the precision we want (in ppm)
        @type ppm: int
        @param sizelim: the limit size of the sequence
        @type sizelim: int"""
        glp_term_out(GLP_OFF)
        self.masseslist = masseslist
        self.mass = mass
        self.ppm = ppm
        self.sizelim = sizelim
        self.cutmass = cutmass
        #int indice for optimisations function (row)
        self.ia = intArray(1+10000) 
        #int indice for param (masses)
        self.ja = intArray(1+10000)
        #mutiplicateur value for the arry of masses
        self.ar = doubleArray(1+10000)
        self.mip = glp_create_prob()
        
    def create_problem(self, min_or_max, delta=0.):
        """Initialisation of the problem
        @param min_or_max: choose if we try to minimize of maximize
        to have the best fitting value (little stupid work arround
        because no GLP_CLOSETO0 avaible) ("MIN" or "MAX" or CHECK)
        @type min_or_max: str
        @param delta: the mass delta to apply in case of CHECK
        @type delta: float
        """
        #have to find an other way than minimise because it's search
        #arround the minimal boundary we want to search arround 
        #the center
        if min_or_max == "MIN":
            glp_set_obj_dir(self.mip, GLP_MIN)
        else: 
            glp_set_obj_dir(self.mip, GLP_MAX)
        glp_set_prob_name(self.mip, "sequencesolver")
        #limit the time
        #~ lpx_set_real_parm(self.mip, LPX_K_TMLIM, 10.)
        self.set_equate(min_or_max, delta)
        self.set_var()
        self.create_matrix()
        
    def set_equate(self, min_or_max, delta):
        """ create the equation of the problem. 
        it's the create of rows of the matrix
        @param min_or_max: choose if we try to minimize of maximize
        to have the best fitting value (little stupid work arround
        because no GLP_CLOSETO0 avaible) ("MIN" or "MAX" or CHECK)
        @type min_or_max: str
        @param delta: the mass delta to apply in case of CHECK
        @type delta: float"""
        glp_add_rows(self.mip, 3)
        if min_or_max == "MIN":
            boundmin = self.mass - self.mass * 1./1000000.
            boundmax = self.mass + self.mass * self.ppm/1000000.
        elif min_or_max == "MAX":
            boundmin = self.mass - self.mass * self.ppm/1000000.
            boundmax = self.mass + self.mass * 1/1000000.
        elif min_or_max == "CHECK":
            boundmin = self.mass
            #~ boundmin = float_round(boundmin, delta)
            boundmax = self.mass + delta
            #~ boundmax = float_round(boundmax, delta)
        #create the equation to get a good mass
        glp_set_row_name(self.mip, 1, "respect_ppm_limits")
        glp_set_row_bnds(self.mip, 1, GLP_DB, boundmin, boundmax)
        glp_set_row_name(self.mip, 2, "respect_seqsize_limits")
        if self.sizelim != None:
            glp_set_row_bnds(self.mip, 2, GLP_DB, 0, self.sizelim)
        else: 
            glp_set_row_bnds(self.mip, 2, GLP_LO, 0, 0)
        glp_set_row_name(self.mip, 3, "link_constraint")
        glp_set_row_bnds(self.mip, 3, GLP_FX, 1, 1)
        
    def set_var(self):
        """ create mutiplicateur limites and type and name.
        It's the creation of column of the matrix
        Set of the columns nothing to do with row"""
        glp_add_cols(self.mip, len(self.masseslist)+1)
        #creation de tout les x 
        for i in range(1, len(self.masseslist)+2):
            glp_set_col_name(self.mip, i, str(i))
            #limit from 0 to 3
            if self.sizelim != None:
                glp_set_col_bnds(self.mip, i, GLP_DB, 0, self.sizelim)
            else:
                #to not have negative solution
                glp_set_col_bnds(self.mip, i, GLP_LO, 0, 0) 
            #type of the multiplicator : integer
            glp_set_col_kind(self.mip, i, GLP_IV)
            #coef in the main equation
            glp_set_obj_coef(self.mip, i, 1)  

    
    def create_matrix(self):
        """ create matrix for all equation exept the main"""
        sizeelems = len(self.masseslist)+1
        for i in range(1, sizeelems+1):
            #normal equation
            self.ia[i] = 1 #ligne (equation)
            self.ja[i] = i #column (x)
            if i != sizeelems:
                self.ar[i] = self.masseslist[i-1] #coef mult (mass)
            else:
                self.ar[i] = self.cutmass
            #limit equation
            self.ia[sizeelems+i] = 2 #line (equation)
            self.ja[sizeelems+i] = i #column (x)
            if i != sizeelems:
                self.ar[sizeelems+i] = 1. #value (coef mult of x)
            else:
                self.ar[sizeelems+i] = 0 #0 because it's the number of cutmass
            #number of cutmass+1 == number of elems
            self.ia[sizeelems*2+i] = 3 #line (equation)
            self.ja[sizeelems*2+i] = i #column (x)
            if i != sizeelems:
                self.ar[sizeelems*2+i] = 1
            else:
                self.ar[sizeelems*2+i] = -1
        
        
    def compute_int_result(self):
        """Compute the integer search result
        @return: the result composition
        @rtype: list(int) """
        retval = glp_intopt(self.mip, None)
        return retval
        
    def resolve_problem(self):
        """lauch the solver of the problem and return it
        @return: if the probleme is solve a list of coeficent 
        to apply to masses. if not 1
        @rtype: list(float) | int """
        glp_load_matrix(self.mip, (len(self.masseslist)+1)*3, 
                        self.ia, self.ja, self.ar)
        #lp solver                
        retval = glp_simplex(self.mip, None)
        
        if retval != 0:
            raise GLPKException("Error in the search of the float result")
        retval = self.compute_int_result()
        if retval != 0:
            raise GLPKException("Error in the search of the int result")
        #get data value
        
        res = []
        for i in range(1, len(self.masseslist)+1):
            res.append(glp_mip_col_val(self.mip, i))
        glp_delete_prob(self.mip)
        return res        
        
class IntegerCheck(object):
    """ Check if a mass is biologicaly possible 
    @ivar datoms: for each atom give it's order 
    @type datoms: dict(str : int)
    @ivar atomsmasses: a list of masses in the same order than
    datoms
    @type atomsmasses: list(float)
    @ivar constraintes: list of constraintes a constrainte is 
    define with 2 list of atoms and a multiplicator factor. The list
    supcompo is sup to mult time the infcompo
    @type constraintes: dict(str : list, str : int, str : list)
    @ivar mass: the mass to check
    @type mass: float
    @ivar delta: the delta to apply to the mass
    @type delta: float
    @ivar ppm: the ppm to apply to the mass
    @type ppm: float
    @ivar ia: an array with a matrice row as value
    @ivar ja: an array with a matrice column as value
    @ivar ar: an array with float as value. The value represent
    a multiplying factor for the matrice compartment. The id represent
    the id we can find in ia and ja to know the matrix row and column
    @ivar mip: the interger programming problem equation and metadata
    @type mip: GLPK structure (C)
    """

    
    def __init__(self, datomsmasses, mass, delta, constraintes, ppm=0):
        """init the IntegerCheck, this class is used to check if a mass
        is biologicaly possible or not
        @param damtomsmass: a dict with for each atom key the mass to used
        (should be the monoisotopic mass)
        @type datomsmass: dict(str: float)
        @param mass: the mass to analysed
        @type mass: float
        @param delta: the delta to apply to the mass (in dalton)
        @type delta: float
        @param ppm: the error rate in ppm
        @type ppm: int
        """
        glp_term_out(GLP_OFF)
        self.datoms = {}
        self.atomsmasses = []
        self.constraintes = constraintes
        i = 1
        for key in datomsmasses:
            self.datoms[key] = i
            self.atomsmasses.append(datomsmasses[key])
            i += 1
        self.mass = mass
        self.delta = delta
        self.ppm = ppm
        self.ia = intArray(1+10000) 
        #int indice for param (masses)
        self.ja = intArray(1+10000)
        #mutiplicateur value for the arry of masses
        self.ar = doubleArray(1+10000)
        self.mip = glp_create_prob()

    def create_problem(self):
        """ give a name to the problem and use all classes function
        to initialise it """
        glp_set_obj_dir(self.mip, GLP_MIN)
        glp_set_prob_name(self.mip, "biologychecker")
        self.set_equate()
        self.set_var()
        self.create_matrix()
        
    def set_equate(self):
        """ create all the constraintes equations of the problem
        Equation are represent per line of a matrix"""
        #globale bound equation
        glp_add_rows(self.mip, len(self.constraintes)+1)
        #check the delta
        boundmin = self.mass
        boundmax = self.mass + self.delta
        boundmin = boundmin - boundmin * self.ppm/1000000.
        boundmax = boundmax + boundmax * self.ppm/1000000.
        glp_set_row_name(self.mip, 1, "respect_bound")
        glp_set_row_bnds(self.mip, 1, GLP_DB, boundmin, boundmax)
        for i in range(0, len(self.constraintes)):
            glp_set_row_name(self.mip, i+2, str(i+2))
            glp_set_row_bnds(self.mip, i+2, GLP_LO, 0, 0)
        
    def set_var(self):
        """ create all constaintes on variable (multiply factor on
        atoms masses.
        This variables are represent per row in a matrix"""
        glp_add_cols(self.mip, len(self.atomsmasses))
        for i in range(1, len(self.atomsmasses)+1):
            glp_set_col_name(self.mip, i, str(i))
            glp_set_col_bnds(self.mip, i, GLP_LO, 0, 0)
            #type of the multiplicator : integer
            glp_set_col_kind(self.mip, i, GLP_IV)
            #coef in the main equation
            #the coef is 0 because of a trick we don't want to minimise
            #but only know if it's possible. Don't making the minimisation
            #make if ~4 time faster
            glp_set_obj_coef(self.mip, i, 0)
    
    def create_matrix(self):
        """ Create the matrix of constrainte equation"""
        #~ for i in glp_get_num_rows(self.mip):
        #row; colunm; mult
        #the number of colunm is the number of equation
        #the number of colunm is the number of var
        #check if really between bound
        self.ia[1] = 1; self.ja[1] = 1; self.ar[1] = self.atomsmasses[0]
        self.ia[2] = 1; self.ja[2] = 2; self.ar[2] = self.atomsmasses[1]
        self.ia[3] = 1; self.ja[3] = 3; self.ar[3] = self.atomsmasses[2]
        self.ia[4] = 1; self.ja[4] = 4; self.ar[4] = self.atomsmasses[3]
        self.ia[5] = 1; self.ja[5] = 5; self.ar[5] = self.atomsmasses[4]
        self.ia[6] = 1; self.ja[6] = 6; self.ar[6] = self.atomsmasses[5]
        self.ia[7] = 1; self.ja[7] = 7; self.ar[7] = self.atomsmasses[6]
        self.ia[8] = 1; self.ja[8] = 8; self.ar[8] = self.atomsmasses[7]
        # constraintes
        for i in range(0, len(self.constraintes)):
            for key in self.datoms:
                indice = (i+1)*8+self.datoms[key]
                self.ia[indice] = i+2; self.ja[indice] = self.datoms[key]
                if key in self.constraintes[i]["supcompo"]:
                    self.ar[indice] = 1
                elif key in self.constraintes[i]["infcompo"]:
                    self.ar[indice] = -self.constraintes[i]["mult"]
                else:
                    self.ar[indice] = 0
    
    def resolve_problem(self):
        """lauch the solver of the problem and return it
        @return: if the probleme is solve a list of coeficent 
        to apply to masses. if not 1
        @rtype: list(float) | int """
        glp_load_matrix(self.mip, len(self.atomsmasses)*
                        (len(self.constraintes)+1), 
                        self.ia, self.ja, self.ar)
        #lp solver                
        retval = glp_simplex(self.mip, None)
        
        if retval != 0:
            raise GLPKException("Error in the search of the float result")
        #mip solver, need to be solve with lp before
        retval = glp_intopt(self.mip, None)
        if retval != 0:
            raise GLPKException("Error in the search of the int result")
        #get data value
        
        res = []
        for i in range(1, len(self.atomsmasses)+1):
            res.append(glp_mip_col_val(self.mip, i))
        glp_delete_prob(self.mip)
        return res

            
if __name__ == "__main__":
    pass

    
    
