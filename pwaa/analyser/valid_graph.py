#!/usr/bin/env python
# encoding: utf-8
""" valid_graph is used to create a graph from the peaks
@author: Vezin Aurelien
@license: CECILL-B"""
import sys
import networkx as nx
import matplotlib.pyplot as plt
import itertools


from pwaa.analyser.mass_search import (select_join_side, 
                                       select_complement_side)
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.masses import float_round, mult_compo, combine_compo
from pwaa.evaluation.subsequence_probability import SubsequenceInsertionEval
from pwaa.basicknowledge.melems import ChimicalElement

def get_compo(dlinks, complement, join, cut):
    """Get the composition of a MDoubleLink  
    @param dlinks: a list of MDoubleLink
    @type dlinks: list(MDoubleLink)
    @param complement: the complement side 'c-term', 'x-cut', None, etc ...
    @type complement: str
    @param join: the join side 'c-term', 'x-cut', None, etc ...
    @type join: str
    @param cut: the cut side 'c-term', 'x-cut', None, etc
    @type cut: str
    @return: the composition of the dlink
    @rtype: dict{str: int}
    """
    for dlink in dlinks:
        if (dlink.lcomplement == complement and 
                dlink.joinside == join and dlink.cutside == cut):
            return dlink.composition 
        
#~ def nodedict(peakgraph):
    #~ print peakgraph.node(data=True)
    #~ dres = {}
    #~ for node, data in peakgraph.node(data=True):
        #~ dres[node] = data
    #~ return dres

def all_permut(permuted):
    """ return all the possible permutation of unordered list
    @param permuted:  a list of elem
    @type permuted: list(list(list(MSingleAAPTM)))
    @return: all possible permutation
    @rtype: list(list(MSingleAAPTM))"""
    res = [[]]
    for l1 in permuted:
        newres = []
        for elem in l1:
            eleml = elem
            for lres in res:
                newres.append(lres+eleml)
        res = newres
    return res
    
class IndexedSequences(object):
    """ Object done to index sequences 
    @ivar dindex: a dict of the index
    @type dindex: dict()
    @ivar dlist: a list of sequences
    @type dlist: list()
    """
    
    def __init__(self):
        """ init of the IndexedSequences"""
        self.dindex = {}
        self.dlist = []
        
    def try_to_insert(self, sequence):
        """ Try to insert a sequence and insert it if it's not 
        in the object already
        @param sequence: a sequence
        @type sequence: list(MSingleAAPTM)"""
        strhash = ""
        for elem in sequence:
            strhash += str(elem.__hash__())
        to_insert = not self.dindex.has_key(strhash)
        if to_insert:
            self.dindex[strhash] = True
            self.dlist.append(sequence)



class ValidGraph(object):
    """ Create a graph with X main links made from X major peaks in
    a Spectrum
    @ivar max_mass: the maximal mass of a link
    @type max_mass: float
    @ivar dlinks: a list of MDoubleLink
    @type dlinks: list(MDoubleLink)
    @ivar peaks: the list of all mass, intensity possibility
    @type peaks: list((int/float, int/float)) 
    @ivar peakgraph: a directed acyclique graph with peaks as node
    @type peakgraph: DiGraph
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp """
    
    def __init__(self, dlinks, peaks, max_mass, peakgraph=None, atoms=None):
        """init all the class param 
        @param dlinks: a list of MDoubleLink
        @type dlinks: list(MDoubleLink)
        @param peaks: the list of all mass, intensity possibility
        @type peaks: list((int/float, int/float)) 
        @param max_mass: the maximal mass of a link
        @type max_mass: float
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(MAtoms) """
        #max_mass 400 or 600
        self.mamo = MAtomsMassOp(atoms)
        self.max_mass = max_mass
        self.dlinks = dlinks
        self.peaks = peaks
        if peakgraph == None:
            self.peakgraph = nx.DiGraph()
        else:
            self.peakgraph = peakgraph
        
        
    def insert_peakgraph_nodes(self):
        """insert all major peaks as node in the graph with 
        all possibles cuts"""
        for mass in self.peaks:
            elems = []
            if self.peaks[mass]["begin_or_end"] == True:
                if mass == 0.0:
                    elems += select_join_side(self.dlinks, "c-term")
                    elems += select_join_side(self.dlinks, "n-term")
                else:
                    self.peakgraph.add_node((mass, "c-term", None, None), 
                                            self.peaks[mass])
                    self.peakgraph.add_node((mass, "n-term", None, None), 
                                            self.peaks[mass])
            else:

                for cut in select_join_side(self.dlinks, "c-term"):
                    elems += select_complement_side(self.dlinks, cut.cutside)
                for cut in select_join_side(self.dlinks, "n-term"):
                    elems += select_complement_side(self.dlinks, cut.cutside)
            for elem in elems:
                self.peakgraph.add_node((mass, elem.lcomplement, 
                                         elem.joinside, elem.cutside), self.peaks[mass])
    
    def insert_edges(self, lmass_possible):
        """ insert the major edges that are biologicaly possible
        @param lmass_possible: a dict with the mass as key and if
        it's possible or not as value
        @type lmass_possible: dict(float: bool)  """
        #generate lot of uselesslinks
        nodes = sorted(self.peakgraph.nodes())
        for i in range(0, len(nodes)):
            #do the interger possibility search for all links 
            #(joinside, cutside)
            #for each other peaks
            for j in range(i + 1, len(nodes)):
                if (nodes[i][0] != nodes[j][0] and 
                        nodes[i][3] == nodes[j][1] and 
                        (len(self.peakgraph.predecessors(nodes[i])) > 0
                         or nodes[i][0] == 0.0) 
                        and nodes[i][0] + self.max_mass - nodes[j][0] > 0): 
                    #save it in the graph
                    compo = get_compo(self.dlinks, nodes[i][1], 
                                      nodes[i][2], nodes[i][3])
                    mass_search = nodes[j][0] - nodes[i][0] - \
                        self.mamo.atomlist_to_masses(compo, "mono")
                    if mass_search < 0 or mass_search > self.max_mass:
                        possible = False
                    else:
                        possible = lmass_possible[round(mass_search, 2)]
                    if possible:
                        self.peakgraph.add_edge(nodes[i], nodes[j],
                                                mass=mass_search, 
                                                pos=[])


    def insert_possibility(self, mbinselems, step, ppm):
        """ insert possibilities for each edges if the masse of the edge
        is close enough to be the same object.
        @param mbinselems: is a dict with binarised mass as key and a list
        of list of peptide as value
        @type mbinselems: dict(float: list(list(ChimicalElement))
        @param step: the step between binarised mass 
        @type step: float
        @param ppm: the ppm
        @type ppm: int"""
        for node1, node2 in self.peakgraph.edges():
            edgecontent = self.peakgraph[node1][node2]
            #~ print self.peakgraph[node1], edgecontent, "lol"
            mass = edgecontent['mass']
            peptides = []
            mass_min = mass - mass * ppm/1000000
            mass_max = mass + mass * ppm/1000000
            mass_min = float_round(mass_min, 0.01)
            mass_max = float_round(mass_max, 0.01)
            m = mass_min
            while m <= mass_max:
                lmass_to_peptides = mbinselems.get(m, [])
                peptides += lmass_to_peptides
                m += step
                m = round(m, 2)
            edgecontent["pos"] += peptides
                                                    
    def delete_empty_pos_edge(self):
        """ delete edges were pos (possibilities) is empty """
        for node1, node2 in self.peakgraph.edges():
             if self.peakgraph[node1][node2]["pos"] == []:
                 self.peakgraph.remove_edge(node1, node2)

    def get_begin_end(self):
        """ get the node at the begining and the nodes at the end
        @return: the nodes at the begining and the nodes at the end
        @rtype : (list(node), list(node))"""
        masslist = []
        for node, data in self.peakgraph.nodes(data=True):
            if data["begin_or_end"] == True:
                masslist.append(node[0])
        start = []
        end = [] 
        nodes = self.peakgraph.nodes()
        for mass in masslist:
            if mass == 0.0:
                start += [node for node in nodes if node[0] == mass]
            else:
                end += [node for node in nodes if node[0] == mass]
        return start, end
        
    
    def has_path_begin_to_end(self, src, begins, ends):
        """ check if a path exist from one begining to src and
        src to one end
        @param begins: a list of begin nodes
        @type begins: list((str, str, str, str))
        @param ends: a list of end nodes
        @type ends: list((str, str, str, str))
        @param src: a peak to go throught
        @type src: list((str, str, str, str))
        @return: True if a path exist, False if not
        @rtype: bool"""
        for end in ends:
            if nx.has_path(self.peakgraph, src, end):
                for begin in begins:
                    if nx.has_path(self.peakgraph, begin, src):
                        return True
        return False
    
    def delete_no_path(self):
        """ delete every edge with no path from begin to the end"""
        starts, ends = self.get_begin_end()
        for src in self.peakgraph.nodes():
            path = self.has_path_begin_to_end(src, starts, ends)
            if path == False:
                #deleting all successors edges
                for des in self.peakgraph.successors(src):
                    self.peakgraph.remove_edge(src, des)
                #deleting all predecessors edges
                for aes in self.peakgraph.predecessors(src):
                    self.peakgraph.remove_edge(aes, src)
        #~ print len(self.peakgraph.edges())
    
    def delete_solitary_nodes(self):
        """ delete the nodes without edges"""
        solitary=[n for n, d in self.peakgraph.degree_iter() if d == 0]
        self.peakgraph.remove_nodes_from(solitary)
     
    def get_peakgraph(self):
        """ return the graph 
        @return: the graph representing the peaks
        @rtype: DiGraph"""
        return self.peakgraph
    
    #create all simple sequences if no chimical elem send a mass
    def get_all_simple_sequences(self):
        """ allow to get mass of elems in the sequence 
        Get the mass if cannot get the possibility (used in analysis in 
        django ?)
        @return: a list of sequences
        @rtype: list(list(float or MSingleAAPTM))"""
        iseq = IndexedSequences()
        begin, end = self.get_begin_end()
        pathslist = []
        for start in begin:
            for stop in end:
                pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                pathslist = pathslist + pathstmp
        seqslist = []
        for path in pathslist:
            edgelist = []
            for i in range(1, len(path)):
                edgepos = []
                possibilities = self.peakgraph.edge[path[i-1]][path[i]]["pos"]
                if possibilities == []:
                    #get the mass
                    edgepos.append([self.peakgraph.edge[path[i-1]][path[i]]["mass"]])
                else:
                    #get only the more general
                    minl = min([len(elem) for elem in possibilities])
                    for elemlist in possibilities:
                        if len(elemlist) == minl:
                            edgepos.append(elemlist)
                edgelist.append(edgepos)
            permuted = all_permut(edgelist)
            if path[-1][1] == "n-term":
                #to get the good order
                for l in permuted:
                    l.reverse()
            #clever insertion here
            for l in permuted:
                iseq.try_to_insert(l)
        return iseq.dlist
    
    def get_simple_spectrums(self, r_intensity=True):
        """ get all simple spectrums to do the evaluation.
        A spectrum is composed of masses and possibly intensity.
        The masses are saved only when there is at 
        least one sequence to support it
        @param r_intensity: saving the intensity or not
        @type r_intensity: bool
        @return: a list of sequences
        @rtype: list(list(float)) or list(list((float, float))) 
        """
        iseq = IndexedSequences()
        begin, end = self.get_begin_end()
        pathslist = []
        #get all the path from begin to end
        for start in begin:
            for stop in end:
                pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                pathslist = pathslist + pathstmp
        seqslist = []
        #for every path save the meaning peak
        for path in pathslist:
            edgelist = set([])
            for i in range(1, len(path)):
                #get the list of possibility
                possibilities = self.peakgraph.edge[path[i-1]][path[i]]["pos"]
                #if the edge have a knowed sequence then save the mass of the cut
                if possibilities != []:
                    #save the mass with the relative intensity
                    #save the mass in i-1 too ?
                    if r_intensity:
                        edgepos0 = (path[i-1][0],
                                    self.peakgraph.node[path[i-1]]["relative_intensity"])
                        edgepos1 = (path[i][0], 
                                    self.peakgraph.node[path[i]]["relative_intensity"])
                    #save only the mass
                    else:
                        edgepos0 = path[i-1][0]
                        edgepos1 = path[i][0]
                    edgelist.add(edgepos0)
                    edgelist.add(edgepos1)
            #insert the sequence of masses
            iseq.try_to_insert(list(edgelist))
        return iseq.dlist

    def get_simple_spectrums2(self, r_intensity=True):
        """ An alternative method to get all peaks in a path.
        Theses peaks are see as mass or mass + relative intensity
        @param r_intensity: saving the intensity or not
        @type r_intensity: bool
        @return: a list of sequences
        @rtype: list(list(float)) or list(list((float, float))) 
        """
        iseq = IndexedSequences()
        begin, end = self.get_begin_end()
        pathslist = []
        #get all the path from begin to end
        for start in begin:
            for stop in end:
                pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                pathslist = pathslist + pathstmp
        seqslist = []
        #for every path save the meaning peak
        for path in pathslist:
            edgelist = []
            for i in range(0, len(path)):
                #get the list of possibility
                if r_intensity:
                    edgepos = (path[i][0],
                               self.peakgraph.node[path[i]]["relative_intensity"])
                else:
                    edgepos = path[i][0]
                edgelist.append(edgepos)
            #insert the sequence of masses
            iseq.try_to_insert(edgelist)
        return iseq.dlist


    
    def get_all_sequences(self):
        """ return all the sequence with the elements as themself
        @return: a list of sequences
        @rtype: list(list(MSingleAAPTM))  """
        iseq = IndexedSequences()
        begin, end = self.get_begin_end()
        pathslist = []
        for start in begin:
            for stop in end:
                pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                pathslist = pathslist + pathstmp
        seqslist = []
        for path in pathslist:
            edgelist = []
            for i in range(1, len(path)):
                edgepos = []
                possibilities = self.peakgraph.edge[path[i-1]][path[i]]["pos"]
                #way to do all posibility (cost too much in time)
                #--------
                for elemlist in possibilities:
                    permuted = list(itertools.permutations(elemlist))
                    for elems in permuted:
                        edgepos.append(list(elems))
                #-------
                #get only the more general or the smaller one
                minl = min([len(elem) for elem in possibilities])
                for elemlist in possibilities:
                    if len(elemlist) == minl:
                        edgepos.append(elemlist)
                edgelist.append(edgepos)
            permuted = all_permut(edgelist)
            if path[-1][1] == "n-term":
                #to get the good order
                for l in permuted:
                    l.reverse()
            #clever insertion here
            for l in permuted:
                iseq.try_to_insert(l)
        return iseq.dlist
        
    
    def get_chimical_sequences(self):
        """ get the chimical sequence. 
        A chimical sequence is a sequence with only simple 
        ChimicalElement
        @return: all the chimical sequence
        @rtype: list(list(ChimicalElement))"""
        iseq = IndexedSequences()
        begin, end = self.get_begin_end()
        pathslist = []
        #get all path
        for start in begin:
            for stop in end:
                pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                pathslist = pathslist + pathstmp
        seqslist = []
        for path in pathslist:
            edgelist = []
            for i in range(1, len(path)):
                #get all the possibilities
                possibilities = self.peakgraph.edge[path[i-1]][path[i]]["pos"]
                #take one possibility because it's a valid chimical sequence
                exempleposs = possibilities[0]
                #add the link between each aa
                compo = mult_compo({"H":1, "C":1, "O":1, "N":1}, len(exempleposs) -1)
                #add the composition of each aa
                for elem in exempleposs:
                    compo = combine_compo(compo, elem.composition)
                #add the chimical composition to the list in a ChimicalElement
                edgelist.append(ChimicalElement(compo))
            if path[-1][1] == "n-term":
                #to get the good order
                edgelist.reverse()
            iseq.try_to_insert(edgelist)
        return iseq.dlist
            
    def show_graph_visu(self):
        """ show the graph"""
        #delete useless node
        nodes = sorted(self.peakgraph.nodes())
        #delete when there is no successor and no predecessor 
        #it means where dots are alone
        for node in nodes:
            if (len(self.peakgraph.successors(node)) == 0 and 
                    len(self.peakgraph.predecessors(node)) == 0):
                self.peakgraph.remove_node(node)
        #draw
        nx.draw_networkx(self.peakgraph)
        #~ nx.draw(self.peakgraph)
        plt.show()
        #~ for n1, n2 in self.peakgraph.edges():
            #~ self.peakgraph.edge[n1][n2]
    
    def show_graph_info(self):
        """show the graph info (nbedge and nbnode) """
        #~ self.delete_solitary_nodes()
        print self.peakgraph.number_of_nodes()
        print self.peakgraph.number_of_edges()
            
