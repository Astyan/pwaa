#!/usr/bin/env python
# encoding: utf-8
""" relative_amount.py is used to search all quantites 
rules betweens atoms """
#dead code

from copy import deepcopy

class RelativeAmount(object):
    """RelativeAmount is used to search all quantities rules
    between atoms with an exemple of set of elements 
    @ivar atoms: a dict of atoms with symbols as keys and masses
    as values
    @type atoms: dict(str:float)
    @ivar lelems: a list of elems can be aa aaptm etc ...
    @type lelems: Elems
    @ivar atomslist: a list of all combinaison of atoms
    @type atomslist: list(frozenset(str))
    @ivar matrix: a matrix with sets of atoms as row and colunms
    for each case there is the mult factor
    @type matrix: dict(frozenset(str): dict(frozenset(str): float))
    @ivar rt: the relation tab, each elem has 2 set of atoms and a mult 
    relation between this 2 sets 
    @type rt: list(dict(str: frozenset(str),str: frozenset(str),
    str: float))
    """ 
    
    def __init__(self, atoms, lelems):
        """ init of the class     
        @param atoms: a dict of atoms with symbols as keys and masses
        as values
        @type atoms: dict(str:float)
        @param lelems: a list of elems can be aa aaptm etc ...
        @type lelems: Elems"""
        self.atoms = atoms
        self.lelems = lelems
        self.atomlist = []
        self.matrix = {}
        self.rt = []        

    def construct_atoms_list(self):
        """ create all possible set of atoms (with no double) """
        #get the list of atoms
        atoms_list = [[atom] for atom in self.atoms]
        values = deepcopy(atoms_list)
        values_tmp = deepcopy(atoms_list)
        new_values_tmp = []
        for i in range(0, len(atoms_list)):
            for alist in atoms_list:
                for vallist in values_tmp:
                    new_values_tmp.append(frozenset(alist+list(vallist)))
            new_values_tmp = list(set(new_values_tmp))
            values = new_values_tmp
            values_tmp = new_values_tmp
            new_values_tmp = []
        self.atomslist = values
    
    def construct_matrix(self):
        """ create the matrix to fill"""
        for l1 in self.atomslist:
            self.matrix[l1] = {}
            for l2 in self.atomslist:
                if l1 != l2:
                    tpass = True
                    for elem in l1:
                        if elem in l2:
                            tpass = False
                    if tpass:
                        self.matrix[l1][l2] = None
        
    def fill_matrix(self):
        """ fill the matrix with mult value"""
        for key1 in self.matrix:
            for key2 in self.matrix[key1]:
                for elem in self.lelems:
                    quantity1 = 0
                    quantity2 = 0
                    for atom in key1:
                        quantity1 += elem.composition.get(atom, 0)
                    for atom in key2:
                        quantity2 += elem.composition.get(atom, 0)
                    if quantity2 != 0:
                        mult = (quantity1*10)/quantity2
                        if (self.matrix[key1][key2] == None or
                                mult/10. < self.matrix[key1][key2]):
                            if mult/10. <= 0.:
                                self.matrix[key1][key2] = 0.
                            else:
                                self.matrix[key1][key2] = mult/10.
    
    def relation_tab(self):
        """ with the matrix fill the relation tab"""
        for key1 in self.matrix:
            for key2 in self.matrix[key1]:
                if self.matrix[key1][key2] > 0:
                    mod = False
                    for elem in self.rt:
                        if self.matrix[key1][key2] == elem["mult"]:
                            if (key1.issubset(elem["supcompo"]) and 
                                    key2.issubset(elem["infcompo"])):
                                elem["supcompo"] = key1
                                elem["infcompo"] = key2
                                mod = True
                            if (elem["supcompo"].issubset(key1) 
                                    and elem["infcompo"].issubset(key2)):
                                mod = True
                    if mod == False:
                        self.rt.append({"mult" : self.matrix[key1][key2],
                                        "supcompo" : key1, 
                                        "infcompo" : key2})
        #delete not unique
        lrt = []
        for elem in self.rt:
            if elem not in lrt:
                lrt.append(elem)
        self.rt = lrt
        
