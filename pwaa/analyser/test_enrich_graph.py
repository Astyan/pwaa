#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods, protected-access
"""test_enrich_graph.py unittest for models in enrich_graph.py 
@author: Vezin Aurelien
@license: CECILL-B"""
import unittest
import os, sys

from pwaa.analyser.enrich_graph import EnrichGraph, WeightGraph
from pwaa.basicknowledge.melemscreate import fill_all_link, fill_all_aa
from pwaa.basicknowledge.mcombielemscreate import reason_all_doublelinks
from pwaa.analyser.valid_graph import ValidGraph
from pwaa.analyser.mass_search import MassPossibleDic
from pwaa.analyser.mztom import MassSpectrum
from pwaa.analyser.mass_elems import masses_bins_elems
from pwaa.evaluation.subsequence_probability import EdgeWeight, PeakWeight


INITPATH = os.path.join("..", "filestest")



class TestEnrichGraph(unittest.TestCase):
    """test for MassSpectrum class"""

    @classmethod
    def setUpClass(cls):
        """init the MassSpectrum class"""
        massspectrum = MassSpectrum(os.path.join("analyser", 
                                                 "files", 
                                                 "factice.mgf"))
        converted = massspectrum.converted_peak()
        links = fill_all_link()
        dlinks = reason_all_doublelinks(links)
        vg = ValidGraph(dlinks, converted, 195.0)
        vg.insert_peakgraph_nodes()
        mpd = MassPossibleDic(50, 0.005, [], 0.0, 195.0, 2)
        mpd.create_jobs()
        vg.insert_edges(mpd.result)
        vg.delete_no_path()
        vg.delete_solitary_nodes()
        cls.val = fill_all_aa()
        pos = masses_bins_elems(cls.val, 2, 0.01)
        vg.insert_possibility(pos , 0.01, 50)
        vg.delete_empty_pos_edge()
        vg.delete_no_path()
        vg.delete_solitary_nodes()
        cls.mjp = EnrichGraph(vg.get_peakgraph(), 22)
    

    def test_11_get_max_intensity(self):
        """ test get_max_intensity """
        self.assertEqual(self.mjp.get_max_intensity(), 423)
        
    def test_12_knowed_charge(self):
        """ test knowed_charge """
        self.mjp.knowed_charge()
        #test if there is the field knowed_charge everywhere    
    
    def test_13_relative_intensity(self):
        """ test relative intensity """
        self.mjp.relative_intensity()
        
    
    def test_14_get_max_charge(self):
        """ test of get_max_charge """
        m_charge = self.mjp.get_max_charge()
        #self.assertEqual(m_charge, 2)        
    
    #~ def test_15_is_max_charge(self):
        #~ """ test of is_max_charge"""
        #~ self.mjp.is_max_charge()
        #~ #test some value 
        
    def test_16_other_peak(self):
        """ test other_peak """
        self.mjp.other_peaks()
        #testing some value
    
    def test_18_save_symmetric_masses(self):
        """ test save_symmetric_masses """
        self.mjp.save_symmetric_masses()
    
    #~ def test_19_save_symmetric_max_charge(self):
        #~ """ test save_symmetric_max_charge"""
        #~ self.mjp.save_symmetric_max_charge("c-term")
        #~ 
    def test_20_diff_mass(self):
        """ test diff_mass """
        self.mjp.diff_mass()
    
    def test_21_charge_diff(self):
        """ test charge_diff"""
        self.mjp.charge_diff()
        
    def test_22_found_immonium(self):
        """ test the found immonium """
        self.mjp.found_immonium(self.val[1:2])
    
    def test_23_compute_all_data(self):
        """ """
        self.mjp.compute_all_data(self.val[1:2], self.val[4:5])


class TestWeightGraph(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        """init the MassSpectrum class"""
        massspectrum = MassSpectrum(os.path.join("analyser", 
                                                 "files", 
                                                 "factice.mgf"))
        converted = massspectrum.converted_peak()
        links = fill_all_link()
        dlinks = reason_all_doublelinks(links)
        vg = ValidGraph(dlinks, converted, 195.0)
        vg.insert_peakgraph_nodes()
        
        mpd = MassPossibleDic(50, 0.005, [], 0.0, 195.0, 2)
        mpd.create_jobs()
        vg.insert_edges(mpd.result)
        vg.delete_no_path()
        vg.delete_solitary_nodes()
        val = fill_all_aa()
        pos = masses_bins_elems(val, 2, 0.01)
        vg.insert_possibility(pos , 0.01, 50)
        vg.delete_empty_pos_edge()
        vg.delete_no_path()
        vg.delete_solitary_nodes()
        mjp = EnrichGraph(vg.get_peakgraph(), 22)
        mjp.compute_all_data([], [])
        ew = EdgeWeight()
        cutval = {"n-term": 0.1, "c-term": 0.1, "c-cut": 0.1, 
                  "b-cut": 0.1, "a-cut": 0.1, "x-cut": 0.1,
                  "y-cut": 0.1, "z-cut": 0.1, 'None': 0.1} 
        ew.peakweight(1., 1., 1., 1., 1., cutval, 1.)
        ew.edgeweight(1., 1., 1.)
        pw = PeakWeight()
        pw.peakweight(1., 1., 1., 1., 1., cutval, 1.)
        cls.wg = WeightGraph(mjp.peakgraph, ew, pw)
        
    def test_10_compute_edge_weight(self):
        """ """
        self.wg.compute_edge_weight()
    
    def test_11_best_edges(self):
        """ """
        self.wg.get_best_edges()
        
    def test_12_select_best_path(self):
        """ """
        self.wg.select_best_path()
    
    def test_13_compute_peak_weight(self):
        """ """
        self.wg.compute_peak_weight()
    
    def test_14_select_best_peak(self):
        """ """
        self.wg.compute_peak_weight()
        self.wg.select_best_peak(lim=10)
        
    #~ def test_13_get_all_sequences(self):
        #~ """ """
        #~ sequences = self.wg.get_all_sequences()
        #~ print sequences
        
