#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test of relative_amount.py file"""

import sys
import unittest
from random import randint


from pwaa.basicknowledge.melemscreate import fill_all_aa
from pwaa.utils.atoms import MAtoms
from pwaa.analyser.relative_amount import RelativeAmount
from pwaa.analyser.integersearch import IntegerCheck


class TestRelativeAmount(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        laa = fill_all_aa()
        matoms = MAtoms()
        cls.datoms = matoms.get_mono_masses_dictatoms()
        laa = laa[0:2]
        cls.ra = RelativeAmount(cls.datoms, laa)
    
    def test_10_construct_atoms_list(self):
        """ test of construct_atoms_list """
        self.ra.construct_atoms_list()
        self.assertEqual(len(self.ra.atomslist), 255)
    
    def test_11_construct_matrix(self):
        """ test of construct_matrix"""
        self.ra.construct_matrix()
        self.assertEqual(len(self.ra.matrix), 255)
    
    def test_12_fill_matrix(self):
        """ test of construct_matrix"""
        self.ra.fill_matrix()
        self.assertEqual(len(self.ra.matrix), 255)
        
    def test_13_relation_tab(self):
        """ test of relation_tab """
        self.ra.relation_tab()
        self.assertEqual(len(self.ra.rt), 7)

        
        
        
if __name__ == '__main__':
    unittest.main()
