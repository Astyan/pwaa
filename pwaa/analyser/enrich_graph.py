#!/usr/bin/env python
# encoding: utf-8
""" This file is used to enrich the graph with metadata, theses
metadata will be used in the computation of the fragmentation
consistency score
@author: Vezin Aurelien
@license: CECILL-B"""

import sys
import networkx as nx

from pwaa.utils.knowledgeoperation import get_diff_link_mass
from pwaa.basicknowledge.melemscreate import fill_all_link
from pwaa.basicknowledge.mcombielemscreate import reason_all_doublelinks
from pwaa.utils.masses import dif_compo, combine_compo
from pwaa.utils.atoms import MAtomsMassOp



def copy_nodes(graph):
    """ copy the node of the graph into a new graph
    @param graph: a graph from networkx
    @type graph: Graph or DiGraph
    @return: a new graph with the node of the other graph 
    @rtype: Graph or DiGraph"""
    new_graph = nx.DiGraph()
    for node, infos in graph.nodes(data=True):
        new_graph.add_node(node)
        for key in infos:
            new_graph.node[node][key] = infos[key]
    return new_graph

def shortest_path_none(graph, node1, node2):
    """ Get the shortest path if there is one,
    if there is none return None
    @param graph: a graph from networkx
    @type graph: Graph or DiGraph
    @param node1: the source node 
    @type node1: hashable
    @param node2: the well node
    @type node2: hashable
    @return: the shortest path or None
    @rtype: list or None
    """
    try:
        spath = nx.shortest_path(graph, node1, node2)
    except nx.NetworkXNoPath:
        spath = None
    return spath

def copy_edges_data(graphin, graphout):
    """ from 2 similare graph (the graphout must be a 
    subgraph of graphin) fill edges data of graphout with edges data
    of graphin
    @param graphin: the main graph with all edges
    @type graphin: Graph or DiGraph
    @param graphout: the graph to obtain as output
    @type graphout: Graph or DiGraph """
    for edge in graphout.edges():
        graphout[edge[0]][edge[1]] = graphin[edge[0]][edge[1]]
    return graphout

class EnrichGraph(object):
    """ EnrichGraph add information on the nodes and the edges of a
    graph
    @ivar nbpeak: the number of peak we want
    @type nbpeak: int
    @ivar peakdict: a dictionnary with peaks
    @type peakdict: dict(float : dict(str : any))
    @ivar mainspeaks: a dictionnary with only the mainspeaks
    @type mainspeaks: dict(float : dict(str : any))
    @ivar min_intensity: the minimal intensity
    @type min_intensity: float
    @ivar ppm: the precision in ppm
    @type ppm: int
    @ivar links: a list of MSingleLink
    @type links: list(MSingleLink)
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp"""
    
    def __init__(self, peakgraph, ppm, atoms=None):
        """ fill the class variables 
        @param nbpeaks: the number of peak we want
        @type nbpeaks: int
        @param peakdict: a dictionnary with peaks
        @type peakdict: dict(float : dict(str : any))
        @param ppm: the precision in ppm
        @type ppm: int
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(MAtoms) 
        """
        self.mamo = MAtomsMassOp(atoms)
        self.atoms = atoms
        self.peakgraph = peakgraph
        self.min_intensity = 0.
        self.ppm = ppm
        self.links = fill_all_link()
        self.dlinks = reason_all_doublelinks(self.links)
    
    def get_max_intensity(self):
        """ return the maximal intensity of the spectrum
        @return: the maximal intensity
        @rtype: float or int"""
        intensity = 0
        for key in self.peakgraph.nodes():
            if self.peakgraph.node[key]['intensity'] > intensity:
                intensity = self.peakgraph.node[key]['intensity']
        return intensity

    def knowed_charge(self):
        """ add a field knowed_charge to the peaks in peakdict elems"""
        for key in self.peakgraph.nodes():
            if self.peakgraph.node[key]['nbcharge'] == 1:
                self.peakgraph.node[key]['knowed_charge'] = 1
            else: 
                self.peakgraph.node[key]['knowed_charge'] = 0
    
    def relative_intensity(self):
        """ add the relative intensity field to the peak in peakdict
         elems"""
        max_intensity = self.get_max_intensity()
        for key in self.peakgraph.nodes():
            intensity = self.peakgraph.node[key]["intensity"]
            rel_intensity = intensity / max_intensity
            self.peakgraph.node[key]["relative_intensity"] = rel_intensity
            
    def get_max_charge(self):
        """ return the maximal charge of peakdict 
        !!!! negative charge not implemented yet
        @return : the maximal charge
        @rtype : int"""
        m_charge = 0
        for key in self.peakgraph.nodes():
            if self.peakgraph.node[key]["charge"] > m_charge:
                m_charge = self.peakgraph.node[key]["charge"]
        return m_charge
    
    def other_peaks(self):
        """ add the number other peaks on the same side"""
        key_name = "other_peaks"
        for key in self.peakgraph.nodes():
            cut = key[1]
            if cut != "n-term" and cut != "c-term":
                mass_diff = get_diff_link_mass(self.links, cut, atoms=self.atoms)
                nbmatch = 0
                for d_mass in mass_diff:
                    massmin = (key[0] + d_mass) - (key[0] + d_mass) \
                    * self.ppm / 1000000
                    massmax = (key[0] + d_mass) + (key[0] + d_mass) \
                    * self.ppm / 1000000
                    lres = [m for m in self.peakgraph.node 
                            if m > massmin and m < massmax]
                    nbmatch += len(lres)
                self.peakgraph.node[key][key_name] = nbmatch
            else:
                self.peakgraph.node[key][key_name] = 0
 
    def symmetric_masses(self, side, masse):
        """ compute the symetrique masses (it means possible masses
        for the other side).
        Use always the protonated composition
        pepmass - term - (mass_given - term - cut1 + midcompo) + cut2
        @param side: the original side (c-term or n-term)
        @type side: str
        @param masse: the original masse
        @type masse: float
        @return: the list of opposite masse
        @rtype: list(float)
        """
        midcompo = {'C' : 1, 'O' : 1, 'H' : 1, 'N' : 1} 
        #get the opposite side
        if side == "c-term":
            opposite_side = "n-term"
        else:
            opposite_side = "c-term"
        #get all masses of the peptides
        pepmasses = []
        for key in self.peakgraph.nodes():
            if (key[0] != 0.0  and 
                    self.peakgraph.node[key]["begin_or_end"] == True):
                pepmasses.append(key[0])
        #get the compositions on the side
        compos_side = []
        for link in self.links:
            if link.opposite == side and link.link != opposite_side:
                compos_side.append(link.composition)
        #get the compositions on the opposite side
        compos_oppo = []
        for link in self.links:
            if link.opposite == opposite_side and link.link != side:
                compos_oppo.append(link.composition)
        #compute all masses
        masses_dif = []
        for pepmass in pepmasses:
            for sidelink  in compos_side:
                for oppolink in compos_oppo:
                    mass_temp = pepmass - masse
                    compo = combine_compo(sidelink, oppolink)
                    compo = dif_compo(compo, midcompo)
                    m_compo = self.mamo.atomlist_to_masses(compo, "mono")
                    m_compute = mass_temp + m_compo
                    masses_dif.append(m_compute)
        #return the list of masses
        return masses_dif
                    
     
    def save_symmetric_masses(self):
        """save the number of symetrique masses 
        from the original masses do the computation for all possibility"""
        for key in self.peakgraph.nodes():
            if key[1] == "c-term" or "a-cut" or "b-cut" or "c-cut":
                side = "c-term"
            else:
                side = "n-term"
            sym_masses = self.symmetric_masses(side, key[0])
            n_sym = 0
            for mass in sym_masses:
                massmin = mass - mass * self.ppm / 1000000.
                massmax = mass + mass * self.ppm / 1000000.
                lres = [mtmp[0] for mtmp in self.peakgraph.node 
                        if mtmp[0] > massmin and mtmp[0] < massmax]
                n_sym += len(lres)
            self.peakgraph.node[key]["symmetric"] = n_sym
    
    def diff_mass(self):
        """ save the ratio of the diff of masse from the middle mass
        for each peak. The save is in peakdict"""
        #because there is staticaly more peaks at the middle so 
        #we need to keep most of the peaks at the edge
        pepmasses = []
        for key in self.peakgraph.nodes():
            if (key[0] != 0.0  
                    and self.peakgraph.node[key]["begin_or_end"] == True):
                pepmasses.append(key[0])
        for key in self.peakgraph.node:
            dif_min = 1.
            for mass in pepmasses:
                mass = mass / 2.
                mkey = abs(key[0] - mass)
                
                dif_tmp = mkey / mass
                if dif_tmp < dif_min:
                    dif_min = dif_tmp
            self.peakgraph.node[key]["diff_masses"] = dif_min
            
    def charge_diff(self):
        """ fill the charge diff for each edge """
        #diff 0 : coherence + 1
        #charge_min_mass_peak < charge_max_mass_peak : coherence 0
        #charge_min_mass_peak > charge_max_mass_peak : coherence -1
        for peak1, peak2 in self.peakgraph.edges():
            charge_peak1 = self.peakgraph.node[peak1]["charge"]
            charge_peak2 = self.peakgraph.node[peak2]["charge"] 
            diff = charge_peak2 - charge_peak1
            self.peakgraph.edge[peak1][peak2]["charge_diff"] = diff
        
    def found_immonium(self, immonium_list, list_type=1):
        """ search if we found an immonium if the list_type is 1
        then search with MSingleAA or MSingleAAPTM, if the list_type is 
        of type 2 search by mass. And save the amount of
        immunium found for each edge
        @param proteasebreaklist: a list of immonium
        @type proteasebreaklist: list(float) or list(MSingleAA) or list(MSingleAAPTM)
        @param list_type: the type of list (for a list of elems or a list
        of mass)
        @type list_type: int"""
        #or immonium in pos (better !!! to change and change the residuesearch too)
        #immonium_list from get_residue
        if list_type == 1:
            for peak1, peak2 in self.peakgraph.edges():
                immoniums = 0.
                for elemgroup in self.peakgraph.edge[peak1][peak2]["pos"]:
                    for elem in elemgroup:
                        if elem in immonium_list:
                            immoniums += 1.
                self.peakgraph.edge[peak1][peak2]["immonium_found"] = immoniums
        elif list_type == 2:
            for peak1, peak2 in self.peakgraph.edges():
                immoniums = 0.
                mass = self.peakgraph.edge[peak1][peak2]["mass"]
                mass_min = mass - mass * self.ppm/1000000.
                mass_max = mass + mass * self.ppm/1000000.
                for immonium_mass in immonium_list:
                    if immonium_mass > mass_min and immonium_mass < mass_max:
                        immoniums += 1
                self.peakgraph.edge[peak1][peak2]["immonium_found"] = immoniums
                
    
    def found_proteasebreak(self, proteasebreaklist, list_type=1):
        """ search if we found proteasebreak if the list_type is 1
        then search with MSingleAA or MSingleAAPTM, if the list_type is 
        of type 2 search by mass. And save this amount in the edge
        @param proteasebreaklist: a list of proteasebreak
        @type proteasebreaklist: list(float) or list(MSingleAA) or list(MSingleAAPTM)
        @param list_type: the type of list (for a list of elems or a list
        of mass)
        @type list_type: int"""
        if list_type == 1:
            for peak1, peak2 in self.peakgraph.edges():
                proteasebreak = 0.
                for elem in self.peakgraph.edge[peak1][peak2]["pos"]:
                    if elem in proteasebreaklist:
                        proteasebreak += 1.
                        break
                if peak2[3] != "n-term" and peak1[2] != "n-term":
                    proteasebreak = -proteasebreak 
                self.peakgraph.edge[peak1][peak2]["proteasebreak"] = proteasebreak
        elif list_type ==2:
            for peak1, peak2 in self.peakgraph.edges():
                proteasebreak = 0.
                mass = self.peakgraph.edge[peak1][peak2]["mass"]
                mass_min = mass - mass * self.ppm/1000000.
                mass_max = mass + mass * self.ppm/1000000.
                for proteasebreak_mass in proteasebreaklist:
                    if proteasebreak_mass > mass_min and proteasebreak_mass < mass_max:
                        proteasebreak += 1.
                        break
                if peak2[3] != "n-term" and peak1[2] != "n-term":
                    proteasebreak = -proteasebreak 
                self.peakgraph.edge[peak1][peak2]["proteasebreak"] = proteasebreak

    #~ def number_of_elems(self):
        #~ """ 
        #~ Set a number representing the number of elems
        #~ Cannot be use if the sequence is not fill
        #~ """
        #~ for peak1, peak2 in self.peakgraph.edges():
            #~ nelems = len(self.peakgraph.edge[peak1][peak2]["pos"])
            #~ self.peakgraph.edge[peak1][peak2]["nbelems"] = 1./nelems
    
    #~ def match_subsequence_expemass(self):
        #~ """ 
        #~ Set the matching score between the best subsequence and the 
        #~ experimental mass
        #~ Cannot be use if the sequence is not fill
        #~ """
        #~ for peak1, peak2 in self.peakgraph.edges():
            #~ subseq = self.peakgraph.edge[peak1][peak2]["pos"]
            #~ subseqmass = self.mamo.subsequence_masse(subseq, "mono")
            #~ expemass = self.peakgraph.edge[peak1][peak2]["mass"]
            #~ massd = abs(subseqmass - expemass)
            #~ massppm = expemass*self.ppm/1000000.
            #~ deltamass = max(0., (massppm - massd) / massppm)
            #~ self.peakgraph.edge[peak1][peak2]["deltamass"] = deltamass 
                #~ 
    def enrich_peaks(self):
        """ enrich only the peaks"""
        self.relative_intensity()
        self.knowed_charge()
        self.save_symmetric_masses()
        self.diff_mass()
        self.other_peaks()
    
    def enrich_edges(self, immonium_list, proteasebreaklist):
        """ enrich the edges 
        @param immonium_list: list of masses corresponding of immoniums
        @type immonium_list: list(float)
        @param proteasebreaklist: list of mass corresponding of amino acid
        of the digest of the protease
        @type proteasebreaklist: list(float)"""
        self.charge_diff()
        self.found_immonium(immonium_list, 2) 
        self.found_proteasebreak(proteasebreaklist, 2) 
    
    def compute_all_data(self, immonium_list, proteasebreaklist):
        """ compute all data except score 
        @param immonium_list: a list of immonium masses found 
        in the spectrum
        @type immonium_list: list(float)"""
        self.enrich_peaks()
        self.enrich_edges(immonium_list, proteasebreaklist)
        #~ self.number_of_elems()
        #~ self.match_subsequence_expemass()


class WeightGraph(object):
    """ WeightGraph is used to set the weight of each edge of the 
    peakgraph
    @ivar peakgraph: The graph from the peak with tags fills
    @type peakgraph: Graph
    @ivar edgeweight: A EdgeWeight class
    @type edgeweight: EdgeWeight
    @ivar peakweight: A PeakWeight class
    """
    
    def __init__(self, peakgraph, edgeweight=None, peakweight=None):
        """ Init of WeightGraph
        @param peakgraph: The graph from the peak with tags fills
        @type peakgraph: Graph
        @param edgeweight: A EdgeWeight class
        @type edgeweight: EdgeWeight"""
        self.peakgraph = peakgraph
        self.edgeweight = edgeweight
        self.peakweight = peakweight
        
    def compute_edge_weight(self):
        """ Compute the weight of each edge"""
        for peak1, peak2 in self.peakgraph.edges():
            edgedata = self.peakgraph.edge[peak1][peak2]
            peak1data = self.peakgraph.node[peak1]
            peak2data = self.peakgraph.node[peak2]
            weight = self.edgeweight.compute_score(peak1, peak2, 
                                                   peak1data, 
                                                   peak2data,
                                                   edgedata)
            self.peakgraph.edge[peak1][peak2]["weight"] = weight
    
    def compute_peak_weight(self):
        """ Compute the weight for each peaks"""
        for peak in self.peakgraph.nodes():
            peakdata = self.peakgraph.node[peak]
            weight = self.peakweight.compute_score(peak, peakdata)
            self.peakgraph.node[peak]["weight"] = weight
            
    def get_begin_end(self):
        """ get the node at the begining and the nodes at the end
        @return: the nodes at the begining and the nodes at the end
        @rtype : (list(node), list(node))"""
        peaklist = []
        for key in self.peakgraph.nodes():
            if self.peakgraph.node[key]["begin_or_end"] == True:
                peaklist.append(key)
        start = []
        end = [] 
        nodes = self.peakgraph.nodes()
        for peak in peaklist:
            if peak[0] == 0.0:
                start.append(peak)
            else:
                end.append(peak)
        return start, end
    
    def get_best_edges(self):
        """ get the edges sorted by weight
        @return: the edge weight list sorted by weight and
        the best edge dict with for all weight a list of edge
        @rtype: list(float), dict(float: list(dict(str : dict(str: any)))"""
        best_edge = {}
        weight_set = set([])
        for edge in self.peakgraph.edges():
            edge_data = self.peakgraph[edge[0]][edge[1]]
            d_data = best_edge.get(edge_data["weight"], [])
            d_data.append({"node1" : edge[0], "node2" : edge[1]})
            weight_set.add(edge_data["weight"])
            best_edge[edge_data["weight"]] = d_data
        return sorted(weight_set), best_edge
    
    
    def select_best_path(self, lim=200):
        """ select all best path until the limit of edge is reach
        and save it in the peakgraph
        @param lim: the limit of the number of paths
        @type lim: int"""
        #get the node of the beginning and the end
        starts, ends = self.get_begin_end()
        new_graph = copy_nodes(self.peakgraph)
        weight_set, best_edge = self.get_best_edges()
        for weight in weight_set:
            for edges in best_edge[weight]:
                new_graph.add_edge(edges["node1"], edges["node2"])
                for start in starts:
                    path = shortest_path_none(self.peakgraph, 
                                              start, edges["node1"])
                    if path != None:
                        new_graph.add_path(path)
                for end in ends:
                    path = shortest_path_none(self.peakgraph, 
                                              edges["node2"],
                                              end)
                    if path != None:
                        new_graph.add_path(path)
            if len(new_graph.edges()) > lim:
                break
        self.peakgraph = copy_edges_data(self.peakgraph, new_graph)
        return self.peakgraph


    def select_best_peak(self, lim=1000):
        """ select the best peaks until the limit of peak is reach
        and save it in the peakgraph
        @param lim: the limit of the number of peaks
        @type lim: int
        """
        new_graph = nx.DiGraph()
        data = [(infos["weight"], node, infos) for node, infos in self.peakgraph.nodes(data=True)]
        sorted_data = sorted(data, reverse=True)
        for w, node, infos in sorted_data[0:lim]:
            new_graph.add_node(node)
            for key in infos:
                new_graph.node[node][key] = infos[key]
        for w, node, infos in sorted_data[lim:]:
            #keep the one on c-term and n-term
            if infos["begin_or_end"] == True:
                new_graph.add_node(node)
                for key in infos:
                    new_graph.node[node][key] = infos[key]
        return new_graph

    #~ def get_all_sequences(self, limnbpos=3, limnbsize=3):
        #~ """ return all the sequence """
        #~ iseq = IndexedSequences()
        #~ begin, end = self.get_begin_end()
        #~ pathslist = []
        #~ for start in begin:
            #~ for stop in end:
                #~ pathstmp = list(nx.all_simple_paths(self.peakgraph, start, stop))
                #~ pathslist = pathslist + pathstmp
        #~ seqslist = []
        #~ for path in pathslist:
            #~ edgelist = []
            #~ for i in range(1, len(path)):
                #~ possibilities = self.peakgraph.edge[path[i-1]][path[i]]["pos"]
                #~ for possibilitie in possibilities:
                    #~ 
                #~ 
                #~ edgelist += possibilitie
                #~ 
                #~ 
                #~ 
            #~ if path[-1][1] == "n-term":
                #~ #to get the good order
                #~ edgelist.reverse()
            #~ #clever insertion here
            #~ iseq.try_to_insert(edgelist)
        #~ return iseq.dlist
