#!/usr/bin/env python
# encoding: utf-8
"""test_residue_search.py is used to test the residue_search.py file
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys
import os


from pwaa.analyser.mztom import MassSpectrum
from pwaa.analyser.residue_search import ResidueSearch
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_residue)
from pwaa.basicknowledge.mcombielemscreate import (reason_all_aaptm,
                                                   reason_all_aaptmresidue)


INITPATH = os.path.join("..", "filestest")

class TestResidueSearch(unittest.TestCase):
    """Test for ResidueSearch class"""
    
    @classmethod
    def setUpClass(cls):
        """init the MassSpectrum class"""
        cls.massspectrum = MassSpectrum(os.path.join("analyser", 
                                                     "files", 
                                                     "bsatest.mgf"))
        cls.mamo = MAtomsMassOp()
        l_all_aa = fill_all_aa()
        l_all_ptm = fill_all_ptm()
        l_all_residue = fill_all_residue()
        l_all_aaptm = reason_all_aaptm(l_all_aa, l_all_ptm)
        l_all_aaptmresidue = reason_all_aaptmresidue(l_all_aaptm, 
                                                     l_all_residue)
        cls.l_all_aaptmresidue = l_all_aaptmresidue
    
    def test_10_get_residue(self):
        """ test get_residue method"""
        dmassmol = self.mamo.l_mol_to_dmassmol(self.l_all_aaptmresidue, 
                                                  "mono")
        converted = self.massspectrum.converted_peak()
        rs = ResidueSearch(converted, 50, dmassmol)
        #to test here when file to test on
        self.assertEqual(len(rs.get_residue()), 0)
        #~ for elem in rs.get_residue():
            #~ for el in elem:
                #~ print el.composition, el.aa, el.ptm, el.position
    
    def test_11_get_residue_masses(self):
        """ test get_residue_masses """
        dmassmol = self.mamo.l_mol_to_dmassmol(self.l_all_aaptmresidue, 
                                                  "mono")
        converted = self.massspectrum.converted_peak()
        rs = ResidueSearch(converted, 50, dmassmol)
        masses = rs.get_residue_masses()
        self.assertEqual(masses, [])
            

if __name__ == '__main__':
    unittest.main()
