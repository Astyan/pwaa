#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test_integerseart.py unittest for the integer programming in
integersearch.py
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys

from pwaa.analyser.integersearch import IntegerSearch, IntegerCheck
from pwaa.utils.atoms import MAtoms


class TestIntegerSearch(unittest.TestCase):
    """ test the IntegerSearch class"""

    def test_10_integersearch(self):
        """ test the integersearch with 6 value"""
        laa = [0.5, 0.7, 0.3, 1.1, 0.4, 0.6]
        raa = IntegerSearch(laa, 3., 0, 50, sizelim=None)
        raa.create_problem("MIN")
        res = raa.resolve_problem()
        res_sum = 0.
        for i in range(0, len(laa)):
            res_sum += res[i]*laa[i]
        self.assertEqual(res_sum, 3)
        raa = IntegerSearch(laa, 3., 0, 50,sizelim=3)
        raa.create_problem("MIN")
        res = raa.resolve_problem()
        self.assertEqual(res, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        
    def test_11_protonneutron(self):
        """ test the intergersearch with 2 value"""
        #~ protonandelec = 1.007825057
        proton = 1.0072765
        neutron = 1.0086649
        laa = [proton, neutron]
        raa = IntegerSearch(laa, 302.46063, 0, 50, sizelim=None)
        raa.create_problem("MIN")
        raa2 = IntegerSearch(laa, 302.46063, 0, 50, sizelim=None)
        res = raa.resolve_problem()
        raa2.create_problem("MAX")
        res2 = raa2.resolve_problem()
        self.assertEqual(res, [100.0, 200.0])
        self.assertEqual(res2, [110.0, 190.0])
    
    def test_12_withcutmass(self):
        """ test if using another cutmass than 0 work """
        laa = [0.7, 0.3, 1.1, 0.4, 0.6]
        raa = IntegerSearch(laa, 1.5, 0.5, 50, sizelim=None)
        raa.create_problem("MIN")
        res = raa.resolve_problem()
        self.assertEqual(res, [1.0, 1.0, 0.0, 0.0, 0.0])

class TestIntegerCheck(unittest.TestCase):
    """ test the IntegerCheck class"""

    @classmethod
    def setUpClass(cls):
        """ setup of the class"""
        matoms = MAtoms()
        cls.datoms = matoms.get_mono_masses_dictatoms()
        
    def test_10_no_constraintes(self):
        """ test the intergersearch with no constraintes"""
        ip = IntegerCheck(self.datoms, 3., 0.05, [])
        ip.create_problem()
        self.assertEqual(ip.resolve_problem(), [0.0, 3.0, 0.0, 0.0,
                                                0.0, 0.0, 0.0, 0.0])
    
    def test_10_hconstrainte(self):
        """ test with a constrainte on the H at least
        1 another atoms per H"""
        constraintes = [{"supcompo" : frozenset(["C", "N", "O", "P", "S",
                                                 "As", "Se"]), 
                         "mult" : 1, "infcompo" : frozenset(["H"])}]
        ip = IntegerCheck(self.datoms, 1., 0.05, constraintes)
        ip.create_problem()
        self.assertEqual(ip.resolve_problem(), [0.0, 0.0, 0.0, 0.0,
                                                0.0, 0.0, 0.0, 0.0])
        ip = IntegerCheck(self.datoms, 2., 0.05, constraintes)
        ip.create_problem()
        self.assertEqual(ip.resolve_problem(), [0.0, 0.0, 0.0, 0.0,
                                                0.0, 0.0, 0.0, 0.0])
    

if __name__ == '__main__':
    unittest.main()
