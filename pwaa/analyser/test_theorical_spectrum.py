#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods, unused-variable
"""test_theorical_spectrum.py unittest for all 
theorical_spectrum class and relative function
@author: Vezin Aurelien
@license: CECILL-B"""

import unittest
import sys
import random


from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_link,
                                              fill_all_neutral_loss)
from pwaa.basicknowledge.mcombielemscreate import reason_all_aaptm
from pwaa.basicknowledge.mcombielemsoperation import LinksOperations

from pwaa.analyser.theorical_spectrum import TheoricalSpectrum

class TestTheoricalSpectrum(unittest.TestCase):
    """the class to test TheoricalSpectrum """
    
    @classmethod
    def setUpClass(cls):
        """ the setup to do only once"""
        cls.aas = fill_all_aa()
        links = fill_all_link()
        cls.lop = LinksOperations(links)
    
    def test_10_nter_to_cter(self):
        """ test of nter_to_cter method """
        seq = [self.aas[0], self.aas[1]] #should be A and C
        res = TheoricalSpectrum(seq, self.lop)
        res1 = sorted(res.compute_all_peaks())
        self.assertEqual(res1, [44.050024, 72.044939, 89.071488, 
                                105.001026, 122.027575, 148.00684, 
                                192.056864])
        res2 = sorted(res.compute_all_peaks(True))
        self.assertEqual(res2, [(44.050024, 0.5), (72.044939, 0.8), 
                                (89.071488, 0.1), (105.001026, 0.1), 
                                (122.027575, 0.9), (148.00684, 0.1), 
                                (192.056864, 1.0)])
    
    def test_10_LAADDFR(self):
        seq = [ self.aas[10], self.aas[0], self.aas[0], self.aas[3], self.aas[3], self.aas[5], self.aas[16]]
        res = TheoricalSpectrum(seq, self.lop)
        res1 = sorted(res.compute_all_peaks(True))
        print res1
        
        

if __name__ == '__main__':
    unittest.main()

