#!/usr/bin/env python
# encoding: utf-8
"""mztoz.py contain everything to read a mgffile and get the 
mass spectrum from a m/z spectrum. 
Result should be the same than fragment ion calculator.
PepMass : (M) and no (M+H)+
@author: Vezin Aurelien
@license: CECILL-B"""

from plymgf.mgfreader import MGFReader
import sys

from pwaa.utils.atoms import MAtoms

class MassSpectrum(object):
    """this class take a mgf file to use it with MGFReader and compute 
    all m/z peak to transform it in m peak
    @ivar mgf: the MGFReader class with a mgf open
    @type mgf: MGFReader class
    @ivar matom:  a MAtoms class
    @type matom: MAtoms"""
    
    def __init__(self, pathtomgf, to_open=True, atoms=None):
        """open the mgf file and save it in a MGFReader file
        @param pathtomgf: the path to the MGF file
        @type pathtomgf: string
        @param to_open: must open the mgf or the mgf is already open
        @type to_open: bool
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(MAtoms) """
        self.mgf = MGFReader(pathtomgf, to_open=to_open)
        if atoms==None:
            self._matoms = MAtoms()
        else: 
            self._matoms = MAtoms(atoms)
    
    def next_ion(self):
        """change the peak we are looking at to the next
        @return: the next_ion if not exist return None
        @rtype: None or the next ion"""
        res = self.mgf.next_ion()
        return res
    
    def set_ion(self, indice):
        """ change the peak we are looking at"""
        self.mgf.set_ion(indice)
    
    def get_nb_ions(self):
        """ return the total number of ions
        @return: the number of ions
        @rtype: int"""
        return self.mgf.get_nb_ions()
        
    def get_sequence(self):
        """get the sequence in a list
        @return: a list of aa
        @rtype: list(str)"""
        lchar = []
        try:
            for char in self.mgf.get_ion_seq():
                lchar.append(char)
        except KeyError:
            pass
        return lchar
        
    
    def _ion_charges(self):
        """create the list of the possibles charges for each peak
        @return: the list of the possibles charges for each peak 
        @rtype: list(int)"""
        chargeslist = []
        # get the charge of the ion before fragmentation
        try:
            charges = self.mgf.get_ion_charge()
        except KeyError:
            try:
                charges = self.mgf.get_charge()
            except KeyError:
                charges = [1]
        # if this charge is positive create all charge possible for 
        # unknow positive ions
        if charges[0] > 0:
            maxcharge = max(charges)
            for i in range(1, maxcharge+1, 1):
                chargeslist.append(i)
        # if this charge is negative create all charge possible for 
        # unknow negative ions
        else:
            mincharge = min(charges)
            for i in range(-1, mincharge-1, -1):
                chargeslist.append(i) 
        return chargeslist
        
    def converted_peak(self):
        """convert the m/z peak list to a m peak list and return it
        @return: the list of all mass, intensity possibility
        @rtype: list((int/float, int/float)) 
        """
        peakdict = dict()
        pmz, pintensity = self.mgf.get_ion_pepmass()
        massh = self._matoms.get_mass_atom('H')
        chargeslist = self._ion_charges()
        peakdict[0.0] = {"intensity" : pintensity, 
                         "nbcharge" : 1,
                         "charge" : 1,
                         "begin_or_end" : True,
                         "rmz" : 0.}
        for mz, intensity, charge in self.mgf.get_ion_peaks():
            if charge == 0:
                charges = chargeslist
            else:
                charges = [charge]
            #compute all Mmol possible
            for charge in charges:
                #charge - 1 to put the mass at the theorical mass
                #of charge 1
                mmol = mz * (charge) - (charge - 1) * massh
                #~ mmol = mz * (charge) - charge * massh should put this version too ?
                if mz/pmz > 0.5:
                    smz = (pmz-mz)
                else:
                    smz = mz
                peakdict[mmol] = {"intensity" : intensity, 
                                  "nbcharge" : len(charges),
                                  "charge" : charge,
                                  "begin_or_end" : False,
                                  "rmz": smz/pmz}
        #adding the mass of the peptide
        mz, intensity = self.mgf.get_ion_pepmass()
        try:
            charges = self.mgf.get_ion_charge()
        except KeyError:
            charges = self._ion_charges()
        for charge in charges:
            #not charge - 1 to have a difference between nterm et ycut
            #for exemple
            mmol = mz * charge - charge * massh
            peakdict[mmol] = {"intensity" : intensity, 
                              "nbcharge" : len(charges),
                              "charge" : charge,
                              "begin_or_end" : True,
                              "rmz" : 0.}
        return peakdict

    def get_ion_name(self):
        return self.mgf.get_ion_title()
