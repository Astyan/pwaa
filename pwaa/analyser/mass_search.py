#!/usr/bin/env python
# encoding: utf-8
""" mass_search.py is used to search all matching sequences 
with a given mass and a starting cut on the joinside """

import sys
import multiprocessing
import math

from multiprocessing import Process, Queue
from multiprocesspool.multiprocesspool import MultiProcessPool

from pwaa.analyser.integersearch import (IntegerSearch, IntegerCheck, 
                                         GLPKException)
from pwaa.utils.atoms import MAtomsMassOp, MAtoms



def compute_mass(lcouple):
    """ compute a mass from a list of couple
    @param lcouple: a list of couple where the fisrt elem is a mass and
    the second elem is a mult factor
    @type lcouple: list((float, float))
    @return: a mass
    @rtype: float """
    res = 0
    for mass, nb in lcouple:
        res = res + mass*nb
    return res

def select_join_side(dlinks, joinside):
    """keep only the double links where the joinside is the 
    given joinside 
    @param dlinks: a list of MDoubleLinks
    @type dlinks: list(MDoubleLinks)
    @param joinside: the joinside we want matching with the joinside
    of dlinks
    @type joinside: str
    @return: the MDoubleLinks matching
    @rtype: list(MDoubleLinks)"""
    linksres = [dlink for dlink in dlinks if dlink.joinside == joinside]
    return linksres

def select_complement_side(dlinks, complementside):
    """keep only the double links where the complementside is the 
    given complementside 
    @param dlinks: a list of MDoubleLinks
    @type dlinks: list(MDoubleLinks)
    @param complementside: the complementside we want matching with the 
    compelmentside of dlinks
    @type complementside: str
    @return: the MDoubleLinks matching
    @rtype: list(MDoubleLinks)"""
    linksres = [dlink for dlink in dlinks 
                if dlink.lcomplement == complementside]
    return linksres

def select_cut_side(dlinks, cutside):
    """keep only the double links where the cutside is the 
    given cutside 
    @param dlinks: a list of MDoubleLinks
    @type dlinks: list(MDoubleLinks)
    @param cutside: the cutside we want matching with the 
    cutside of dlinks
    @type cutside: str
    @return: the MDoubleLinks matching
    @rtype: list(MDoubleLinks)"""
    linksres = [dlink for dlink in dlinks if dlink.cutside == cutside]
    return linksres
    
def mass_bins(elems, mass, delta, sizelim=None):
    """ Do an integerSearch an a binarise masse 
    the binairise masse and the delta are related for exemple for a
    delta of 0.25 the masse have to finish with .0 or .25 or .50
    or .75.
    For a delta of .1 and a masse of 50.2 the IntergerSearch will
    search a combinaison of elems for a masse between 50.2 and 50.3
    @param elems: a list of masses:
    @type elems: list(float)
    @param mass: the binarised mass
    @type mass: float
    @param delta: the delta used to search if there is a solution until
    mass+delta
    @type delta: float
    @param sizelim: if we search a combinaison of elem of a len inf
    or equal to sizelim
    @return: a solution (combinaison of elems) or a list of 0 in the order
    of the lighter elem to the heavier
    @rtype: list(float)
    """
    int_search = IntegerSearch(elems, mass, sizelim=sizelim)
    int_search.create_problem("CHECK", delta)
    try:
        res = int_search.resolve_problem()
    except GLPKException:
        res = [0.0] * (len(elems)+1)
    return res
    
class MassSearch(object):
    """ used to give a mass to do computation of possible sequences
    @ivar dlinks: list of doublelinks
    @type dlinks: list(MDoubleLinks)
    @ivar ppm: the precision value in ppm
    @type ppm: int
    @ivar boundmass: the mass of a boundlink
    @type boundmass: float
    @ivar priormasses: masses of all elem with priority
    @type priormasses: list(float)
    @ivar secondarymasses: masses of all secondary elements
    @type secondarymasses: list(float)
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp"""
    
    def __init__(self, priorelems, secondaryelems, ppm, dlinks, atoms=None):
        """ init of every instance var if the class
        @param priorelems: elements to check first(must have 
        a composition)
        @type priorelems: list of anything with a composition
        @param secondaryelems: elements to check in a
        second time
        @type secondaryelems: list of anything with a composition
        @param ppm: the precision in ppm
        @type ppm: int
        @param dlinks: list of MDoubleLinks
        @type dlinks: list(MDoubleLinks)
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(Atoms) """
        self.mamo = MAtomsMassOp(atoms)
        self.dlinks = dlinks
        self.ppm = ppm
        self.boundmass = self.mamo.atomlist_to_masses({"H" :1, 
                                                       "O" :1,
                                                       "C" :1, 
                                                       "N" :1},
                                                      "mono")
        self.priormasses = self.create_masses(priorelems)
        self.secondarymasses = self.create_masses(secondaryelems)
    
    def create_masses(self, elems):
        """ create all mass of a list of element with composition.
        We add a boundlink mass to each of this elems
        @param elems: list of anything with a composition
        @type elems: list of anything with a composition
        @return: a list of mass
        @rtype: list(float)"""
        l_mass = []
        for elem in elems:
            m_tmp = self.mamo.atomlist_to_masses(elem.composition,
                                                 "mono")
            m_tmp = m_tmp + self.boundmass
            l_mass.append(m_tmp)
        return l_mass
    
    
    def problem_processing(self, masses, mass, min_max):
        """ create the parallele process to get the
        composition. The time to get the composition is limited
        to 1 second
        @param masses: a list of masses
        @type masses: list(float)
        @param mass: a mass
        @type mass: float
        @param min_max: if we maximize of minimize "MIN" or "MAX"
        @type min_max: str
        @return: the composition if it has been compute in time or 1
        @rtype: int or list(int)"""
        queue = Queue()
        #could be a thread to avoid the problem with celery
        proc = Process(target=self.problem_process, 
                       args=(masses, mass, min_max, queue,))
        proc.start()
        try:
            ressearch = queue.get(timeout=1)
            proc.join()
        except:
            proc.terminate()
            ressearch = 1
        
        return ressearch
    
    def problem_process(self, masses, mass, min_max, queue):
        """ process to compute the composition
        @param masses: a list of masses
        @type masses: list(float)
        @param mass: a mass
        @type mass: float
        @param min_max: if we maximize of minimize "MIN" or "MAX"
        @type min_max: str
        @param queue: the queue to put the result of the computation
        @type queue: Queue
        """
        ip = IntegerSearch(sorted(masses), mass, ppm=self.ppm)
        ip.create_problem(min_max)
        try:
            ressearch = ip.resolve_problem()
        except GLPKException:
            ressearch = [0.0] * (len(masses)+1)
        queue.put(ressearch)
    
    def search_sequence(self, mass):
        """Search the best sequence for given mass. 
        first in priorelems and if nothing have benn found then in 
        secondaryelems
        @param mass: the mass to reach
        @type mass: float
        @return: the list of coef to apply at masses and if it
        have been found in priorelems or secondaryelems
        @rtype: list(int/float), str
        """
        minsearch = self.problem_processing(self.priormasses, mass, "MIN")
        maxsearch = self.problem_processing(self.priormasses, mass, "MAX")      
        
        massmin = compute_mass(zip(self.priormasses, minsearch))
        massmax = compute_mass(zip(self.priormasses, maxsearch))
        
        #computation with the priormasses
        if minsearch != [0.0] * len(minsearch) and maxsearch != [0.0] * len(maxsearch):
            if abs(massmin-mass) < abs(massmax-mass):
                return (minsearch, "prior")
            else:
                return (maxsearch, "prior")
        elif minsearch != [0.0] * len(minsearch):
            return (minsearch, "prior")
        elif maxsearch != [0.0] * len(maxsearch):
            return (maxsearch, "prior")
        
        #computation with the secondarymasses
        minsearch = self.problem_processing(self.secondarymasses, mass, "MIN")
        if minsearch != [0.0]*len(minsearch):
            return (minsearch, "secondary")
        maxsearch = self.problem_processing(self.secondarymasses, mass, "MAX")
        if maxsearch != [0.0]*len(maxsearch):
            return (maxsearch, "secondary")
        
        #default return 
        return (None, "None")

    def get_all_sequences(self, mass, joinside):
        """ get all sequences starting with the given joinside in 
        param for the mass given in parameter
        @param mass: a mass
        @type mass: float
        @param joinside: a joinside (x-cut, c-cut, etc...)
        @type joinside: str
        @return: a dict with one sequence possible per cutside link
        @rtype: dict(str:(list(int/float), str))"""
        dres = {}
        links = select_join_side(self.dlinks, joinside)   
        for link in links:
            mlink = self.mamo.atomlist_to_masses(link.composition,
                                                    "mono")
            #- mlink because the iteger programming don't use the links
            #+ self.boundmass because the integer programming will find 
            # a mass with one more boundlink than in real
            mreal = mass - mlink + self.boundmass
            dres[link.cutside] = self.search_sequence(mreal)
        return dres



class MassPossible(object):
    """ check if a mass is biologicaly possible
    @ivar ppm: the precision in ppm
    @type ppm: int
    @ivar delta: a delta value to apply masses
    @type delta: float
    @ivar atoms: a dict of atom with there masses
    @type atoms: dict(str:float)"""
    
    def __init__(self, ppm, delta, constraintes, atoms=None):
        """ create all instance variables : ppm, delta, atoms, 
        constraintes 
        @param ppm: the precision in ppm
        @type ppm: int
        @param delta: a delta value to apply masses
        @type delta: float
        @param constraintes: a list of constraintes,
        a constrainte is a dict with an infcompo, supcompo and a mult
        @type constraintes: dict(str:list(str)), str:float, str:list(str)
        @param atoms: the atoms to give to MAtoms
        @type atoms: list(Atoms)"""
        self.delta = delta
        self.ppm = ppm
        self.constraintes = constraintes
        matoms = MAtoms(atoms=atoms)
        self.atoms = matoms.get_mono_masses_dictatoms()
    
    def check_possible(self, mass):
        """check if the mass could be biologicaly possible ie a 
        combinaison between atoms respecting the constraintes exist
        between the mass and the mass+delta. The ppm are respected.
        This check is done withinterger programming
        @param mass: the mass to check
        @type mass: float
        @return: True if it's possible, False if not
        @rtype: bool"""
        ip = IntegerCheck(self.atoms, mass, self.delta, 
                          self.constraintes, self.ppm)
        ip.create_problem()
        try:
            search = ip.resolve_problem()
        except GLPKException:
            return False
        if search == [0.0] * len(search):
            return False
        return True


class MassPossibleDic(object):
    """ construction of a dict with all mass possible 
    from mass_min to mass_max with a step of delta by using multiple
    process
    @ivar mp: a masspossible class init witu ppm, delta and constraintes
    @type mp: MassPossible
    @ivar mass_min: the mass to start
    @type mass_min: float
    @ivar mass_max: the mass to end
    @type mass_max: float
    @ivar floatingnumbers: the number of number after the floating point
    @type floatingnumbers: int
    @ivar result: the result store
    @type result: dict(float: bool)
    @ivar celery: using celery or not
    @type celery:bool
    """
    
    def __init__(self, ppm, delta, constraintes, mass_min, mass_max, 
                 floatingnumbers, atoms=None, celery=False):
        """init of the MassPossibleDic
        @param ppm: the number of ppm
        @type ppm: int
        @param delta: the mass delta
        @type delta: float
        @param constraintes: a list of constraintes,
        a constrainte is a dict with an infcompo, supcompo and a mult
        @type constraintes: dict(str:list(str)), str:float, str:list(str)
        @param mass_min: the mass to start
        @type mass_min: float
        @param mass_max: the mass to end
        @type mass_max: float
        @param floatingnumbers: the number of number after the floating point
        @type floatingnumbers: int
        @param atoms: the atoms to give to MassPossible
        @type atoms: list(Atoms)
        @param celery: using celery or not
        @type celery:bool
        """
        self.mp = MassPossible(ppm, delta, constraintes, atoms=atoms)
        self.mass_min = mass_min
        self.mass_max = mass_max
        self.floatingnumbers = floatingnumbers
        self.masses = self._init_masses()
        self.celery = celery
        self.result = {}
    
    def _init_masses(self):
        """ init the masses bins
        @return: a list of masses bins
        @rtype: list(float)"""
        res = []
        j = self.mass_min
        while j <= self.mass_max:
            res.append(j)
            j += 10**-self.floatingnumbers
            j = round(j, self.floatingnumbers)
        return res
    
    def value_to_res(self, value, valdicres):
        """ for each value add it to the dict
        @param value: a value is a list associated to a mass
        @type value: dict(float: list(float))
        @param valdicres: a dictionnary of other masses associated to
        a elem combinaison
        @type valdicres: dict(float: list(float))
        @return: the combinason of the 2 dict
        @rtype: dict(float: list(float))"""
        valdicres.update(value)
        return valdicres
    
    def get_matching_sequence(self, mass):
        """ get the matching sequence of a mass
        @param mass: a mass binarised
        @type mass: float
        @return: the result sequence with the mass in a dict
        @rtype: dict(float: list(float))"""
        massdic = {}
        res = self.mp.check_possible(mass)
        massdic[mass] = res
        return massdic
            
    def create_jobs(self, nbjobs=None):
        """ create the jobs and save the result"""
        if nbjobs == None:
            nbjobs = multiprocessing.cpu_count()
        mpp = MultiProcessPool(self.get_matching_sequence, self.masses, 
                               self.value_to_res, self.result, celery=self.celery)
        self.result = mpp.run(nbjobs)


class ValidMassesBins(object):
    """ construction of a dict with all valid masses 
    from 0 to mass_max with a step of delta by using multiple
    process
    @ivar elems: a list of masses of all elems to used (aa, aaptm, etc..)
    @type elems: list(float)
    @ivar step: the mass delta
    @type step: float
    @ivar mass_max: the mass to end
    @type mass_max: float
    @ivar result: the result store
    @type result: dict(float: list(float))
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp
    @ivar celery: using celery or not
    @type celery:bool"""
    
    def __init__(self, elems, step, mass_max, atoms=None, celery=False):
        """ elems only number, one per value, must match with something
        on the outside
        @param elems: a list of masses of all elems to used (aa, aaptm, etc..)
        @type elems: list(float)
        @param step: the mass delta
        @type step: float
        @param mass_max: the mass to end
        @type mass_max: float
        @param atoms: the atoms to give to MassPossible
        @type atoms: list(Atoms)
        @param celery: using celery or not
        @type celery:bool"""
        self.bin_masses = self.init_array(step, mass_max)
        self.mamo = MAtomsMassOp(atoms)
        self.masse_elems = [self.mamo.atomlist_to_masses(elem.composition, 
                                                         "mono") for
                            elem in elems]        
        self.elems = elems
        self.result = {}
        self.celery = celery
        self.step = step
        
    def init_array(self, step, mass_max):
        """ init of the binmass array
        @param step: the step of the bins
        @type step: float
        @param mass_max: the max lim of masses to add in the list
        @type mass_max: float
        @return: a list of masses
        @rtype: list(float)"""
        values_masses = []
        nbres = int(mass_max/step)
        for i in range(0, nbres+1):
            values_masses.append(round(i*step, 4)) #4 because larger enough
        return values_masses
    
    def get_matching_sequence(self, mass):
        """ get the maching sequence from a mass
        @param mass: the mass to check
        @type mass: float
        @return: the matching sequence associated with the mass
        @rtype: dict(float: list(float))"""
        res = {}
        reslist = mass_bins(self.masse_elems, mass, self.step)
        res[mass] = reslist
        return res
    
    def value_to_res(self, value, valdicres):
        """ for each value add it to the dict
        @param value: a value is a list associated to a mass
        @type value: dict(float: list(float))
        @param valdicres: a dictionnary of other masses associated to
        a elem combinaison
        @type valdicres: dict(float: list(float))
        @return: the combinason of the 2 dict
        @rtype: dict(float: list(float))"""
        valdicres.update(value)
        return valdicres
    
    def create_all_results(self):
        """ create all result using MultiProcessPool """
        mpp = MultiProcessPool(self.get_matching_sequence, self.bin_masses, 
                               self.value_to_res, self.result, celery=self.celery)
        self.result = mpp.run(multiprocessing.cpu_count())
        
        
    def formated_result(self):
        """ Compute a formated result to be used with insert_possibility
        in ValidGraph.
        @return:return the result with an other format
        the format is for each valid key a list of a list of a 
        ChimicalElement
        @rtype: dict(float: list(list(ChimicalElement)))"""
        formated_result = {}
        for key in self.result:
            l = self.result[key]
            lresult = []
            for i in range(0, len(l)):
                for j in range(0, int(l[i])):
                    lresult.append(self.elems[i])
            if lresult != []:
                formated_result[key] = [lresult]
        return formated_result
                    

