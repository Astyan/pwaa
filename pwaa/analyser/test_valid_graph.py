#!/usr/bin/env python
# encoding: utf-8
""" test_valid_graph.py is a class to test valid_graph.py"""

import unittest
import os, sys

from pwaa.analyser.mztom import MassSpectrum
from pwaa.analyser.mass_search import MassPossibleDic
from pwaa.analyser.valid_graph import ValidGraph, get_compo
from pwaa.analyser.mass_elems import masses_bins_elems
from pwaa.analyser.mass_search import (select_join_side, 
                                       select_complement_side)
from pwaa.analyser.relative_amount import RelativeAmount

from pwaa.basicknowledge.melemscreate import (fill_all_link, fill_all_aa, 
                                              fill_all_ptm, fill_all_residue)
from pwaa.basicknowledge.mcombielemscreate import (reason_all_doublelinks,
                                                   reason_all_aaptm,
                                                   reason_all_aaptm_dlink,
                                                   reason_all_aaptmresidue)

from pwaa.utils.atoms import MAtomsMassOp, MAtoms

class TestValidGraphFunctions(unittest.TestCase):
    """ test of functions in valid_graph_file """
    
    def test_10_get_compo(self):
        """ test of get_compo """
        links = fill_all_link()
        dlinks = reason_all_doublelinks(links)
        compo = get_compo(dlinks, "x-cut", "a-cut", "y-cut")
        #~ print compo
        #~ the compo must be the same than the MDoubleLinks
        

class TestValidGraph(unittest.TestCase):
    """ Test of the class ValidGraph"""
    
    @classmethod
    def setUpClass(cls):
        """ Create the class MainGraph to use aget"""
        massspectrum = MassSpectrum(os.path.join("analyser", 
                                                 "files", 
                                                 "facticeaaa.mgf"))
        converted_peaks = massspectrum.converted_peak()
        links = fill_all_link()
        cls.dlinks = reason_all_doublelinks(links)
        cls.mgraph = ValidGraph(cls.dlinks, converted_peaks, 200.0)
    
    def test_10_insert_peakgraph_nodes(self):
        """test the insert of the major nodes """
        self.mgraph.insert_peakgraph_nodes()
        #~ self.assertEqual(len(self.mgraph.peakgraph.nodes()), 1265)
        #~ self.assertEqual(len(self.mgraph.peakgraph.nodes()), 1265*2)
        self.assertEqual(len(self.mgraph.peakgraph.nodes()), 298)
        
    def test_11_insert_edges(self):
        """ test the insert of all possible edges """
        #all the step to get the good values (theoricaly)
        #~ laa = fill_all_aa()
        #~ lptm = fill_all_ptm()
        #~ laaptm = reason_all_aaptm(laa, lptm)
        #~ l_all_link = fill_all_link()
        #~ dlinks = reason_all_doublelinks(l_all_link)
        #~ laaptmdlink = reason_all_aaptm_dlink(laaptm, dlinks)
        #~ matoms = MAtoms()
        #~ atoms = matoms.get_mono_masses_dictatoms()
        #~ ra = RelativeAmount(atoms, laaptm)
        #~ ra.construct_atoms_list()
        #~ ra.construct_matrix()
        #~ ra.fill_matrix()
        #~ ra.relation_tab()
        #~ mpd = MassPossibleDic(50, 0.01, ra.rt, 0.0, 600.0, 2)
        mpd = MassPossibleDic(50, 0.01, [], 0.0, 200.0, 2)
        mpd.create_jobs()            
        self.mgraph.insert_edges(mpd.result)
        #~ print len(self.mgraph.peakgraph.edges()) # 56909
        #~ self.assertEqual(len(self.mgraph.peakgraph.edges()), 9189)
        #~ self.assertEqual(len(self.mgraph.peakgraph.edges()), 1504) #with the real data
        #~ self.assertEqual(len(self.mgraph.peakgraph.edges()), 28307)
        #~ i = 0
        #~ for node1, node2 in self.mgraph.peakgraph.edges():
            #~ print node1, node2
            #~ i += 1
            #~ if i==30:
                #~ break
    
    def test_12_get_begin_end(self):
        """ test the getting of the node at the begining and at the end"""
        beg, end = self.mgraph.get_begin_end()
        #~ self.assertEqual(beg, [(0.0, 'None', u'c-term', u'z-cut'), 
                               #~ (0.0, 'None', u'c-term', u'n-term'), 
                               #~ (0.0, 'None', u'c-term', u'y-cut'), 
                               #~ (0.0, 'None', u'c-term', u'x-cut')])
        #~ self.assertEqual(end, [(1442.6120600000002, 'n-term', 
                                #~ None, None)])
    
    def test_13_delete_no_path(self):
        """ test the delete of edge without path from begin to the end """
        self.mgraph.delete_no_path()
        
    def test_14_delete_solitary_nodes(self):
        """ test the deletion of the solitary nodes"""
        self.mgraph.delete_solitary_nodes()
    
    def test_15_insert_possibility(self):
        """ """
        res = []
        val = fill_all_aa()
        #~ for elem in val:
            #~ if elem.aa == "A":
                #~ res.append(elem)
        pos = masses_bins_elems(val, 3, 0.01)
        self.mgraph.insert_possibility(pos , 0.01, 50)
        self.mgraph.delete_empty_pos_edge()
    
    def test_16_get_all_sequences(self):
        """ """
        sequences = self.mgraph.get_all_sequences()
        #~ print sequences
    
    def test_16_get_chimical_sequences(self):
        """ """
        sequences = self.mgraph.get_chimical_sequences()
        #~ print sequences
        
    def test_16_get_all_simple_sequences(self):
        sequences = self.mgraph.get_all_simple_sequences()
        #~ print sequences
    
    def test_16_get_simple_spectrums(self):
        sequences = self.mgraph.get_simple_spectrums(r_intensity=False)
        #~ print sequences
    
    def test_16_get_simple_spectrums2(self):
        sequences = self.mgraph.get_simple_spectrums2(r_intensity=False)
        #~ print sequences
        #~ print sequences

    #~ def test_42_show_graph(self):
        #~ """ """
        #~ pass
        #~ self.mgraph.show_graph_info()
        #~ self.mgraph.show_graph_visu()

