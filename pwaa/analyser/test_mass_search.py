#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-many-public-methods
"""test of mass_search.py file"""

import sys
import unittest

#~ from random import randint
sys.path.append("../")

from pwaa.analyser.mass_search import (MassSearch, MassPossible, 
                                       MassPossibleDic, mass_bins, 
                                       select_join_side, ValidMassesBins)

from pwaa.basicknowledge.melems import ChimicalElement
from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_link)
from pwaa.basicknowledge.mcombielemscreate import (reason_all_doublelinks,
                                                   reason_all_aaptm)


class TestMassFunction(unittest.TestCase):
    """ Test for all masses functions """
    
    def test_10_select_join_side(self):
        """ test if the function selection_join_side works"""
        l_all_link = fill_all_link()
        l_all_dlinks = reason_all_doublelinks(l_all_link)
        res = select_join_side(l_all_dlinks, "x-cut")
        self.assertEqual(res[0].cutside, "c-term")
        self.assertEqual(res[0].joinside, "x-cut")
        self.assertEqual(res[0].lcomplement, "a-cut")
        
    def test_11_mass_bins(self):
        """ test of mass_bins """
        self.assertEqual(mass_bins([10.7, 11.], 21.5+43.1, 0.5), [1.0, 1.0])
        self.assertEqual(mass_bins([10.7, 11.1], 22.+43.1, 0.5), [0.0, 2.0])
        self.assertEqual(mass_bins([10.3, 11.], 20.+43.1, 0.5), [0.0, 0.0])        
        
class TestMassSearch(unittest.TestCase):
    """test of all reason in mcombielemscreate"""
    
    @classmethod
    def setUpClass(cls):
        """ to run only once """
        l_all_aa = fill_all_aa()
        l_all_ptm = fill_all_ptm()
        l_all_link = fill_all_link()
        l_all_dlinks = reason_all_doublelinks(l_all_link)
        l_all_aaptm = reason_all_aaptm(l_all_aa, l_all_ptm)
        cls.ms = MassSearch(l_all_aa, l_all_aaptm, 50, l_all_dlinks)
    
    def test_10_search(self):
        """ test of different searchs values"""
        r1 = self.ms.get_all_sequences(40.1, "n-term")
        self.assertEqual(r1, {u'b-cut': (None, 'None'), 
                              u'c-cut': (None, 'None'), 
                              u'a-cut': (None, 'None'), 
                              u'c-term': (None, 'None')})
        
        r2 = self.ms.get_all_sequences(150.1, "n-term")
        self.assertEqual(r2, {u'b-cut': (None, 'None'), 
                              u'c-cut': (None, 'None'), 
                              u'a-cut': ([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                          0.0], 'secondary'), 
                              u'c-term': (None, 'None')})


class TestMassPossible(unittest.TestCase):
    """ Test the class MassPossible used to know 
    if a mass is biologicaly possible """
    
    @classmethod
    def setUpClass(cls):
        """ to run only once """
        constraintes = [{"supcompo" : frozenset(["C", "N", "O", "P", "S",
                                                 "As", "Se"]), 
                         "mult" : 0.5, "infcompo" : frozenset(["H"])},
                        {"supcompo" : frozenset(["C"]), "mult" : 1,
                         "infcompo" : frozenset(["S"])}
                       ]
        cls.mp = MassPossible(50., 0.0, constraintes)
    
    def test_10_check_possible(self):
        """ test of check_possible"""
        self.assertEqual(self.mp.check_possible(0.), False)
        self.assertEqual(self.mp.check_possible(71.037), True)
        self.assertEqual(self.mp.check_possible(129.042), True)
        self.assertEqual(self.mp.check_possible(163.063), True)
        self.assertEqual(self.mp.check_possible(186.079), True)
        self.assertEqual(self.mp.check_possible(147.068), True)
        self.assertEqual(self.mp.check_possible(129.042), True)
        self.assertEqual(self.mp.check_possible(400.9), True)
        self.assertEqual(self.mp.check_possible(140.847673388), False)
        self.assertEqual(self.mp.check_possible(40.), False)



class TestMassPossibleDic(unittest.TestCase):
    """ """
    
    @classmethod
    def setUpClass(cls):
        constraintes = [{"supcompo" : frozenset(["C", "N", "O", "P", "S",
                                                 "As", "Se"]), 
                         "mult" : 0.5, "infcompo" : frozenset(["H"])},
                        {"supcompo" : frozenset(["C"]), "mult" : 1,
                         "infcompo" : frozenset(["S"])}
                       ]
        cls.mpd = MassPossibleDic(50., 0.0, constraintes, 0.0, 30., 2)
        
    def test_10_create_jobs(self):
        """ do the computation and check the sizeof the result"""
        self.mpd.create_jobs()
        self.assertEqual(3001, len(self.mpd.result))
        
    
    #~ def test_11_masse_possiblesave(self):
        #~ """ do the computation and save the result """
        #~ #great exemple of how to do the chaine
        #~ from analyser.relative_amount import RelativeAmount
        #~ from basicknowledge.melems import ChimicalElement
        #~ from utils.atoms import MAtoms
        #~ import pickle
        #~ 
        #~ aa = fill_all_aa()
        #~ ptm = fill_all_ptm()
        #~ aaptm = reason_all_aaptm(aa, ptm)
        #~ link = ChimicalElement({"C": 1, "H": 1, "N": 1, "O": 1})
        #~ aaptm.append(link)
        #~ ma = MAtoms()
        #~ atoms = ma.get_mono_masses_dictatoms()
        #~ ra = RelativeAmount(atoms, aaptm)
        #~ ra.construct_atoms_list()
        #~ ra.construct_matrix()
        #~ ra.fill_matrix()
        #~ ra.relation_tab()
        #~ mpd2 = MassPossibleDic(0, 0.01, ra.rt, 0.0, 600., 2)
        #~ mpd2.create_jobs()
        #~ pickle.dump(mpd2.result, open("massbinposs", "wb"))
    
    #~ 
    #~ def test_11_masse_possibleanalisys(self):
        #~ """ """
        #~ import pickle
        #~ result = pickle.load(open("massbinposs", "rb"))
        #~ i = 0.
        #~ for j in range(0, 60000):
            #~ if i in [x/2. for x in range(0, 1200)]:
                #~ print i
            #~ print result[i]
            #~ i += 0.01
            #~ i = round(i,2)
        #~ print result
        

class TestValidMassBins(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        """ setup of the class """
        elems = [ChimicalElement({'O':1}), ChimicalElement({'O':1}),
                 ChimicalElement({'O':1}), ChimicalElement({'O':1}),
                 ChimicalElement({'O':1}), ChimicalElement({'O':1})]
        step = 0.25
        max_mass = 200.
        cls.vb = ValidMassesBins(elems, step, max_mass)

    #~ def test_10_get_procs_given_values(self):
        #~ """ check if the repartition of masses is good """
        #~ r1 = self.vb.get_procs_given_values(1)
        #~ self.assertEqual(len(r1[0]), 801)
        #~ r2 = self.vb.get_procs_given_values(2)
        #~ self.assertEqual(len(r2[0]), 401)
        #~ r3 = self.vb.get_procs_given_values(3)
        #~ self.assertEqual(len(r3[0])+len(r3[1])+len(r3[2]), 801)
        #~ r4 = self.vb.get_procs_given_values(4)
        #~ self.assertEqual(len(r4[0]), 201)
    
    def test_11_create_all_results(self):
        """ check if the number of masses is good"""
        self.vb.create_all_results()
        self.assertEqual(len(self.vb.result), 801)
        
    def test_12_formated_result(self):
        """ test the formated result"""
        fresult = self.vb.formated_result()
        self.assertEqual(len(fresult[15.75][0]), 1)
        self.assertEqual(len(fresult[75.0][0]), 2)
        self.assertEqual(len(fresult[134.0][0]), 3)
        #no key because of the new method
        #~ self.assertEqual(len(fresult[193.25][0]), 4) 




if __name__ == '__main__':
    unittest.main()
