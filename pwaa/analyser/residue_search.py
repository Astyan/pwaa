#!/usr/bin/env python
# encoding: utf-8
"""residue_search.py is used to search residue in a peaklist
@author: Vezin Aurelien
@license: CECILL-B"""

import sys

from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.masses import dif_compo

class ResidueSearch(object):
    """This class is used to search all residue from a peaklist
    @ivar peaklist: a list of peak (mass and intensity)
    @type peaklist: list((float, int/float))
    @ivar ppm: the presision in ppm
    @type ppm: int
    @ivar dmass_lres: the list of residue identify by there mass
    @type dmass_lres: dict(float: list(residue))
    @ivar mamo: MAtomsMassOp to do the usual operations on atoms
    @type mamo: MAtomsMassOp"""

    
    def __init__(self, peaklist, ppm, dmass_lres, atoms=None):
        """ store the peaklist, the ppm and the dict of residue mass
        in ivar
        @param peaklist: a list of peak (mass and intensity
        @type peaklist: list((float, int/float))
        @param ppm: the presision in ppm
        @type ppm: int
        @param dmass_lres: the list of residue identify by there mass
        @type dmass_lres: dict(float: list(residue))
        @param atoms: the atoms to give to MAtomsMassOp
        @type atoms: list(MAtoms) """
        self.mamo = MAtomsMassOp(atoms)
        self.peaklist = peaklist
        self.ppm = ppm
        self.dmass_lres = dmass_lres
    
    
    def get_residue(self):
        """ get the residue matching with peaks in the peaklist
        a list of matching residue. This matching residue are store in 
        a list in case there is more than one element for a peak 
        @return: a list of list of matching residue
        @rtype: list(list(Residue))"""
        lres = []
        for elem in self.peaklist:
            minmass = elem - (elem * self.ppm) / 1000000
            maxmass = elem + (elem * self.ppm) / 1000000
            lres_tmp = [] 
            for key in self.dmass_lres:
                if key > minmass and key < maxmass:
                    for mol in self.dmass_lres[key]:
                        lres_tmp.append(mol)
            if lres_tmp != []:
                lres.append(lres_tmp)
        return lres
    
    def get_residue_masses(self):
        """ Get the unique theorical masses of residue (no diff between
        term or center right now !!!!)
        @return: the list of residue mass
        @rtype: list(float)
        """
        mass_list = []
        residue_list = self.get_residue()
        for residue_type in residue_list:
            compo = dif_compo(residue_type[0].composition, 
                              residue_type[0].residue_compo)
            mass_list.append(self.mamo.atomlist_to_masses(compo, "mono"))
        return mass_list
            
        
        
