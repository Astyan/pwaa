#!/usr/bin/env python
# encoding: utf-8
""" mass_elems is used to create binarised mass of elems and store it 
@author: Vezin Aurelien
@license: CECILL-B"""

import sys

from pwaa.utils.tree import RecTree
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.masses import float_round


def masses_bins_elems(elems, deep, delta, mtype="mono", atoms=None):
    """ compute the masses for all elems group of the max_size deep 
    generate with the list of elems, then store it in a list with 
    a binarised mass as key
    @param elems: a list of elements
    @type elems: Elems
    @param deep: the max_size for the group of elems
    @type deep: int
    @param delta: the delta to binarised with
    @type delta: float
    @param mtype: the type of masse to use
    @type mtype: str
    @param atoms: the atoms to give to MAtomsMassOp
    @type atoms: list(MAtoms) 
    @return: a dict with binarised mass as key and a list of list
    of elems as value
    @rtype: dict(float:list(list(Elems)))"""
    mamo = MAtomsMassOp(atoms=atoms)
    tree = RecTree(None, elems, None, deep)
    dmass = {}
    dtree = tree.dict_len()
    for key in dtree:
        for elemslist in dtree[key]:
            #adding the mass of the link
            mass = mamo.atomlist_to_masses({"H":1, "C":1, 
                                            "O":1, "N":1},
                                            mtype) * (len(elemslist)-1)
            #adding the mass of each elem of the list
            for elem in elemslist:
                mass += mamo.atomlist_to_masses(elem.composition, mtype)
            mass = float_round(mass, delta)
            dlist = dmass.get(mass, [])
            dlist.append(elemslist)
            dmass[mass] = dlist
    return dmass


        
            

        
    

