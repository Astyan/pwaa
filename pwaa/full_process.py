import pickle
from basicknowledge.melemscreate import fill_all_aa, fill_all_ptm, fill_all_link
from basicknowledge.mcombielemscreate import reason_all_aaptm, reason_all_doublelinks

massposbins = pickle.load(open("massbinposs", "rb"))

#create the masselem for the simple aa
from analyser.mass_elems import masses_bins_elems
mbinelems = masses_bins_elems(fill_all_aa(), 4, 0.01)

#read and create peaks from the mgf file
from analyser.mztom import MassSpectrum
import os

mass_spectrum = MassSpectrum(os.path.join("mgffiles", "NDCEILRmonocharged.mgf"))
#~ mass_spectrum = MassSpectrum(os.path.join("mgffiles", "test.mgf"))
peaks = mass_spectrum.converted_peak()

from analyser.valid_graph import ValidGraph

links = fill_all_link()
dlinks = reason_all_doublelinks(links)

#this first part is to keep only possible path

#create the graph
valid_graph = ValidGraph(dlinks, peaks, 600.0, [], [])
#insert the node (every peaks is a node)
valid_graph.insert_peakgraph_nodes()
#insert the edges (for every possibility of diff of mass insert an edge)
valid_graph.insert_edges(massposbins)
#delete every edge which are not a part of a path from begin to the end
valid_graph.delete_no_path()
#delete every solitary nodes
valid_graph.delete_solitary_nodes()

#insert the real values
valid_graph.insert_possibility(mbinelems, 0.01, 50, {"mass_dif" : 4., "immonium" : 1., "digest" : 2., "nbelems": 4.})
#delete the edge without real values
valid_graph.delete_empty_pos_edge()
#delete every edge which are not a part of a path from begin to the end
valid_graph.delete_no_path()
#delete every solitary nodes
valid_graph.delete_solitary_nodes()

from analyser.enrich_graph import EnrichGraph, WeightGraph

eg = EnrichGraph(valid_graph.get_peakgraph(), 22)
eg.compute_all_data([], [])


from evaluation.subsequence_probability import EdgeWeight

ew = EdgeWeight()
cutval = {"n-term": 0.1, "c-term": 0.1, "c-cut": 0.1, 
          "b-cut": 0.1, "a-cut": 0.1, "x-cut": 0.1,
          "y-cut": 0.1, "z-cut": 0.1, 'None': 0.1} 
ew.peak1weight(1., 1., 1., 1., 1., cutval)
ew.peak2weight(1., 1., 1., 1., 1., cutval)
ew.edgeweight(1., 1., 1., 1., 1.)

wg = WeightGraph(eg.peakgraph, ew)
wg.compute_weight()
wg.select_best_path()
sequences = wg.get_all_sequences()

def sequences_translation(sequences):
    tsequences = []
    for sequence in sequences:
        tsequence = ""
        for elem in sequence:
            tsequence = tsequence + elem.aa
        tsequences.append(tsequence)
    return tsequences

from basicknowledge.mcombielemsoperation import LinksOperations
from analyser.theorical_spectrum import TheoricalSpectrum
from evaluation.matching_spectrum import MatchingSpectrum
lop = LinksOperations(links)
dres = {}
lres = set([])


for sequence in sequences:
    ts = TheoricalSpectrum(sequence, lop)
    tpeaks = ts.compute_all_peaks()
    ms = MatchingSpectrum(peaks, tpeaks)
    ms.set_max_intensity()
    score = ms.compute_score(50)
    tmpl = dres.get(score.__hash__(), [])
    tmpl.append(sequence)
    dres[score.__hash__()] = tmpl
    lres.add(score)
    
lressort = list(lres)
lressort = sorted(lressort, reverse=True)
print lressort[0:10]

for score in lressort[0:10]:
    print score
    seqs = dres[score.__hash__()]
    for seq in seqs:
        print "--------"
        for elem in seq:
            print elem.aa
    print "#########"
