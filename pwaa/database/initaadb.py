#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=no-self-use, unexpected-keyword-arg, no-value-for-parameter
"""initaadb.py the base data to insert into the database 
@author: Vezin Aurelien
@license: CECILL-B"""

from pony.orm import select, commit, Database, db_session
import csv
import os
import sys


from pwaa.database.maadb import (Atom, IsoAtom, Protease, AminoAcid, Link, 
                                 AtomNB, PTMPos, PTM, Organism, AAOrganism, 
                                 PTMOrganism, SmallResidue, NeutralLoss)
                            
from pwaa.utils.dbop import get_anything, get_or_set
    

class InsertAtoms(object):
    """The class is used to get atoms from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsv=None):
        """Open the csv file and extract Atoms informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.atomdict = {}
        if fcsv == None:
            fcsv = os.path.join(".", "files", "atoms.csv")
        atomsfile = open(fcsv)
        for atom in csv.DictReader(atomsfile, delimiter=b','):
            self.atomdict[atom['symbol']] = atom['name']
        atomsfile.close()

    @db_session
    def cleanall(self):
        """ delete all atoms of the database """
        allatoms = select(a for a in Atom)
        for atom in allatoms:
            atom.delete()
    
    @db_session
    def insertall(self):
        """insert all atoms in the database"""
        for symbol in self.atomdict:
            Atom(symbol=symbol, name=self.atomdict[symbol])
        commit()

class InsertIsoAtoms(object):
    """The class is used to get atoms isotop from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsv=None):
        """Open the csv file and extract Isotop informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.isodict = {}
        if fcsv == None:
            fcsv = os.path.join(".", "files", "isotops.csv")
        isosfile = open(fcsv)
        for iso in csv.DictReader(isosfile, delimiter=b','):
            self.isodict[iso['symbol']] = []
        isosfile.close()
        #need another opening 
        isosfile = open(fcsv)
        for iso in csv.DictReader(isosfile, delimiter=b','):
            self.isodict[iso['symbol']].append({"isotop":iso['iso'],
                                                "mass":iso['mass'], 
                                                "abound":iso['abound']})
        isosfile.close()
        
    @db_session
    def cleanall(self):
        """delete all isotops of the database """
        allisotop = select(a for a in IsoAtom)
        for isotop in allisotop:
            isotop.delete()
    
    @db_session
    def insertall(self):
        """insert all isotops in the database"""
        for atom in self.isodict:
            for isotop in self.isodict[atom]:
                IsoAtom(f_atom=atom, isotop=isotop["isotop"],
                        mass=isotop["mass"], 
                        abound=isotop["abound"])
        commit()

class InsertAminoAcid(object):
    """The class is used to get amino acid from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvaaname=None, fcsvaacomposition=None):
        """Open the csv file and extract Atoms informations
        @param fcsvaaname: the path to the csv of aa symbol with name
        @type fcsvaaname: string
        @param fcsvaacomposition: the path to the csv of aa symbol with 
        composition
        @type fcsvaacomposition: string"""
        self.aanamedict = {}
        self.aacompodict = {}
        if fcsvaaname == None:
            fcsvaaname = os.path.join(".", "files", "aaname.csv")
        if fcsvaacomposition == None:
            fcsvaacomposition = \
            os.path.join(".", "files", "aacomposition.csv")
        #store aa symbol and name
        aasfile = open(fcsvaaname)
        for aa in csv.DictReader(aasfile, delimiter=b','):
            self.aanamedict[aa['symbol']] = aa['name']
        aasfile.close()
        #store aa composition
        aasfile = open(fcsvaacomposition)
        for aa in csv.DictReader(aasfile, delimiter=b','):
            self.aacompodict[aa['symbol']] = {}
            for key in aa:
                if key != 'symbol':
                    self.aacompodict[aa['symbol']][key] = aa[key]
        aasfile.close()
    
    @db_session
    def cleanall(self):
        """delete all amino acids of the database """
        allaa = select(a for a in AminoAcid)
        for aa in allaa:
            aa.delete()
    
    @db_session
    def insertallaa(self):
        """insert all amino acids in the database"""
        for symbol in self.aanamedict:
            AminoAcid(symbol=symbol, name=self.aanamedict[symbol])
        commit()
        
    @db_session
    def insertallaatoms(self):
        """insert all atoms for each amino acid in the bdd"""
        for amino_acid in self.aacompodict:
            for atom in self.aacompodict[amino_acid]:
                if self.aacompodict[amino_acid][atom] != 0:
                    atomnb = get_or_set(AtomNB, f_atom=atom, 
                                        occurences=self.aacompodict
                                        [amino_acid][atom])
                    atomnb.amino_acid += \
                    AminoAcid.get(symbol=amino_acid)
        commit()
        
    @db_session
    def insertall(self):
        """insertall all amino acid and there composition"""
        self.insertallaa()
        self.insertallaatoms()
        
class InsertProtease(object):
    """The class is used to get protease from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsv=None):
        """Open the csv file and extract Proteases informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.proteaseslist = []
        self.proteasesdict = {}
        if fcsv == None:
            fcsv = os.path.join(".", "files", "proteases.csv")
        proteasesfile = open(fcsv)
        for protease in csv.DictReader(proteasesfile, delimiter=b','):
            self.proteasesdict = {}
            self.proteasesdict[protease['protease']] = \
            str(protease['cleave'])
            self.proteaseslist.append(self.proteasesdict)
        proteasesfile.close()
    
    @db_session
    def cleanall(self):
        """ delete all proteases of the database """
        allproteases = select(p for p in Protease)
        for protease in allproteases:
            protease.delete()
    
    @db_session
    def insertall(self):
        """insert all proteases in the database"""
        for protease in self.proteaseslist:
            for pname in protease:
                protid = get_or_set(Protease, name=pname)
                aa = AminoAcid.get(symbol=protease[pname])
                protid.cleave += aa
        commit()
        
class InsertLink(object):
    """The class is used to get links from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvcompo=None, fcsvrelation=None, 
                 fcsvuprotcompo=None):
        """Open the csv file and extract Links informations
        @param fcsvcompo: the path to the csv file with the composition
        in amino acid
        @type fcsvcompo: string
        @param fcsvrelation: the path to the csv file with relation
        between links
        @type fcsvrelation: string
        @param fcsvuprotcompo: the path to the csv file with the
        unprotoned composition in amino acid"""
        self.linkcompodict = {}
        self.linkunprotcompodict = {}
        self.linkrelationdict = {}
        if fcsvuprotcompo == None:
            fcsvuprotcompo = os.path.join(".", "files", 
                                          "unprotlinkscompo.csv")
        if fcsvcompo == None:
            fcsvcompo = os.path.join(".", "files", "linkscompo.csv")
        if fcsvrelation == None:
            fcsvrelation = os.path.join(".", "files", 
                                        "linksrelation.csv")
        #save the compo
        linksfile = open(fcsvcompo)
        for link in csv.DictReader(linksfile, delimiter=b','):
            self.linkcompodict[link['link']] = {}
            for key in link:
                if key != 'link':
                    self.linkcompodict[link['link']][key] = link[key]
                    self.linkrelationdict[link['link']] = {}
        linksfile.close()
        #save unprot compo
        linksfile = open(fcsvuprotcompo)
        for link in csv.DictReader(linksfile, delimiter=b','):
            self.linkunprotcompodict[link['link']] = {}
            for key in link:
                if key != 'link':
                    self.linkunprotcompodict[link['link']][key] = \
                    link[key]
        linksfile.close()
        #save relation
        linksfile = open(fcsvrelation)
        for relation in csv.DictReader(linksfile, delimiter=b','):
            for key in relation:
                if key != 'link':
                    data = self.linkrelationdict[relation
                                                 ['link']].get(key, [])
                    if key == "complementary":
                        data = relation[key]
                    else:
                        data.append(relation[key])
                    self.linkrelationdict[relation['link']][key] = data
        linksfile.close()
    
    
    @db_session
    def cleanall(self):
        """ delete all link of the database """
        alllinks = select(l for l in Link)
        for link in alllinks:
            link.delete()
    
    @db_session
    def insertall(self):
        """insert all links in the database"""
        for link in self.linkcompodict:
            new_link = get_or_set(Link, name=link)
            for elem in self.linkrelationdict[link]:
                #adding the complementary
                if elem == "complementary":
                    if self.linkrelationdict[link][elem] != "None":
                        lname = self.linkrelationdict[link][elem]
                        new_link.complementary = get_or_set(Link, 
                                                            name=lname)
                #adding the opposite
                if elem == "opposite":
                    linklist = self.linkrelationdict[link][elem]
                    for cut in linklist:
                        new_link.opposite += get_or_set(Link, name=cut)
            #adding the composition
            for atom in self.linkcompodict[link]:
                occ = self.linkcompodict[link][atom]
                if occ != 0:
                    new_link.atoms += \
                    get_or_set(AtomNB, f_atom=atom, occurences=occ)
            #adding the unprot composition
            for atom in self.linkunprotcompodict[link]:
                occ = self.linkunprotcompodict[link][atom]
                if occ != 0:
                    new_link.unprotatoms += \
                    get_or_set(AtomNB, f_atom=atom, occurences=occ)
            commit()
        
class InsertPTM(object):
    """The class is used to get ptms from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvptm=None, fcsvptmpos=None):
        """Open the csv file and extract Links informations
        @param fcsvptm: the path to the csv ptm file
        @type fcsvptm: string
        @param fcsvptmpos: the path to the csv ptmpos file
        @type fcsvptmpos: string"""
        self.ptmsdict = {}
        self.ptmsposlist = []
        if fcsvptm == None:
            fcsvptm = os.path.join(".", "files", "ptms.csv")
        if fcsvptmpos == None:
            fcsvptmpos = os.path.join(".", "files", "ptmspos.csv")
        ptmsfile = open(fcsvptm)
        for ptm in csv.DictReader(ptmsfile, delimiter=b','):
            self.ptmsdict[ptm['name']] = {}
            for key in ptm:
                if key != 'name':
                    self.ptmsdict[ptm['name']][key] = ptm[key]
        ptmsfile.close()
        ptmsposfile = open(fcsvptmpos)
        for ptmpos in csv.DictReader(ptmsposfile, delimiter=b','):
            self.ptmsposlist.append(ptmpos)
        ptmsposfile.close()
    
    @db_session
    def cleanall(self):
        """ delete all ptms of the database """        
        allptms = select(p for p in PTM)
        for ptm in allptms:
            ptm.delete()
        
    @db_session
    def insert_all_ptm(self):
        """insert all ptm in the database"""
        for ptm in self.ptmsdict:
            for atom in self.ptmsdict[ptm]:
                if self.ptmsdict[ptm][atom] != 0:
                    atomnb = get_or_set(AtomNB, f_atom=atom,
                                        occurences=self.ptmsdict
                                        [ptm][atom])
                    atomnb.ptm += get_or_set(PTM, name=ptm)
        commit()
    
    @db_session
    def insert_all_ptmpos(self):
        """ insert all ptmpos in the database"""
        for elem in self.ptmsposlist:
            get_or_set(PTMPos, position=elem['position'], 
                       ptm=get_or_set(PTM, name=elem['ptm']), 
                       amino_acid=elem['aa'])
        commit()
    
    @db_session
    def insertall(self):
        """insert all ptms and ptmspos in the database """
        self.insert_all_ptm()
        self.insert_all_ptmpos()

class InsertOrganisms(object):
    """This class is used to get Organism from a csv and save it
    in the database"""
    
    def __init__(self, fcsvorga=None):
        """Open the csv file and extract Organisms informations
        @param fcsvorga: the path to the csv ptm file
        @type fcsvorga: string"""
        self.orgalist = []
        if fcsvorga == None:
            fcsvorga = os.path.join(".", "files", "organisms.csv")
        orgafile = open(fcsvorga)
        for orga in csv.DictReader(orgafile, delimiter=b','):
            if orga['parent'] == 'None':
                orga['parent'] = None
            self.orgalist.append(orga)
        orgafile.close()
    
    @db_session
    def cleanall(self):
        """delete all organisms"""
        allorganisms = select(o for o in Organism)
        for orga in allorganisms:
            orga.delete()
            #~ orga.cascade_delete()
    
    @db_session
    def insertall(self):
        """insert all organism in the database"""
        for child_par in self.orgalist:
            if child_par['parent'] == None:
                get_or_set(Organism, name=child_par['organism'])
            else:
                get_or_set(Organism, name=child_par['organism'],
                           parent=get_or_set(Organism, 
                                             name=child_par['parent']))
            commit()

class InsertAAOrganisms(object):
    """This class is used to get Organism for each aa from a csv
    and save it in the database
    @precondition: The AminoAcid and the Organism and the PTMs must
    be already in the database"""
    
    def __init__(self, fcsvaaorga=None, fcsvptmorga=None):
        """Open the csv file and extract Organisms and AA link
        @param fcsvaaorga: the path to the csv file with aa and 
        organisms
        @type fcsvaaorga: string
        @param fcsvptmorga: the path to the csv file with ptms and
        organism
        @type fcsvptmorga: string"""
        self.aaorgalist = []
        self.ptmorgalist = []
        if fcsvaaorga == None:
            fcsvaaorga = os.path.join(".", "files", "aaorganisms.csv")
        if fcsvptmorga == None:
            fcsvptmorga = os.path.join(".", "files", "ptmsorganisms.csv")
        aaorgafile = open(fcsvaaorga)
        for aaorga in csv.DictReader(aaorgafile, delimiter=b','):
            self.aaorgalist.append(aaorga)
        aaorgafile.close()
        ptmorgafile = open(fcsvptmorga)
        for ptmorga in csv.DictReader(ptmorgafile, delimiter=b','):
            self.ptmorgalist.append(ptmorga)
        ptmorgafile.close()
    
    @db_session
    def cleanall(self):
        """delete all organism aa"""
        allaaorganism = select(aao for aao in AAOrganism)
        for aaorga in allaaorganism:
            aaorga.delete()
        allptmorganism = select(ptmo for ptmo in PTMOrganism)
        for ptmorga in allptmorganism:
            ptmorga.delete()
    
    @db_session
    def insert_aaorga(self):
        """insert all aaorganism in the database"""
        for aa_orga in self.aaorgalist:
            get_or_set(AAOrganism, 
                       amino_acid=aa_orga["amino_acid"],
                       organism=aa_orga["organism"])
    
    @db_session
    def insert_ptmorga(self):
        """insert all ptmorganism in the database"""
        for ptm_orga in self.ptmorgalist:
            get_or_set(PTMOrganism, 
                       ptm=PTM.get(name=ptm_orga["ptm"]),
                       organism=ptm_orga["organism"])
    
    @db_session
    def insertall(self):
        """insert ptmorganism and aaorganism in the database"""
        self.insert_aaorga()
        self.insert_ptmorga()
        
class InsertSmallResidue(object):
    """ this class is used to add Small residues in the database"""
    
    def __init__(self, fcsvsr=None, fcsvsrc=None):
        """Open the csv file and extract small residues information
        @param fcsvsr: the path to the csv file with the small residu
        type the amino acids it can goes on and if it can goes on 
        all ptms of the amino acid
        @type fcsvsr: string
        @param fcsvsrc: the path to the csv file small residue name
        with the composition
        @type fcsvsrc: string"""
        self.srlist = [] 
        self.srcdict = {}
        if fcsvsr == None:
            fcsvsr = os.path.join(".", "files", "smallresidu.csv")
        if fcsvsrc == None:
            fcsvsrc = os.path.join(".", "files", "smallresiducompo.csv")
        fcsvrsfile = open(fcsvsr)
        for smallresidue in csv.DictReader(fcsvrsfile, delimiter=b','):
            self.srlist.append(smallresidue)
        fcsvrsfile.close()
        fcsvsrcfile = open(fcsvsrc)
        for srcompo in csv.DictReader(fcsvsrcfile, delimiter=b','):
            self.srcdict[srcompo['name']] = {}
            for key in srcompo:
                if key != 'name':
                    self.srcdict[srcompo['name']][key] = srcompo[key]
        fcsvsrcfile.close()
        
    @db_session
    def cleanall(self):
        """delete all small residue"""
        allsr = select(sr for sr in SmallResidue)
        for sr in allsr:
            sr.delete()
        commit()
    
    @db_session
    def insert_composition(self):
        """insert all small residue composition"""
        for key in self.srcdict:
            get_or_set(SmallResidue, name=key)
            for atom in self.srcdict[key]:
                atomnb = get_or_set(AtomNB, f_atom=atom,
                                    occurences=self.srcdict
                                    [key][atom])
                atomnb.small_residue += get_or_set(SmallResidue,
                                                   name=key)
                commit()
    
    @db_session
    def insert_ptmpos(self):
        """insert all amino acid and if it can have an action on the 
        ptm of each amino acid"""
        for elem in self.srlist:
            ptmpos = get_or_set(PTMPos, 
                                ptm=get_or_set(PTM, name=elem['ptm']), 
                                position=elem['position'], 
                                amino_acid=elem['aa'])
            ptmpos.small_residue += get_or_set(SmallResidue,
                                               name=elem['name'])
    
    def insertall(self):
        """insert all small residue"""
        self.insert_composition()
        self.insert_ptmpos()
        
class InsertNeutralLoss(object):
    """this class is used to add NeutralLoss in the database"""
    
    def __init__(self, fcsvsr=None, fcsvsrc=None):
        """Open the csv file and extract neutral loss informations
        @param fcsvsr: the path to the csv file with the neutral loss
        type the amino acids it can goes on and if it can goes on 
        all ptms of the amino acid
        @type fcsvsr: string
        @param fcsvsrc: the path to the csv file small neutral loss
        with the composition
        @type fcsvsrc: string"""
        self.srlist = [] 
        self.srcdict = {}
        if fcsvsr == None:
            fcsvsr = os.path.join(".", "files", "neutralloss.csv")
        if fcsvsrc == None:
            fcsvsrc = os.path.join(".", "files", "neutrallosscompo.csv")
        fcsvrsfile = open(fcsvsr)
        for neutralloss in csv.DictReader(fcsvrsfile, delimiter=b','):
            self.srlist.append(neutralloss)
        fcsvrsfile.close()
        fcsvsrcfile = open(fcsvsrc)
        for srcompo in csv.DictReader(fcsvsrcfile, delimiter=b','):
            self.srcdict[srcompo['name']] = {}
            for key in srcompo:
                if key != 'name':
                    self.srcdict[srcompo['name']][key] = srcompo[key]
        fcsvsrcfile.close()
        
    @db_session
    def cleanall(self):
        """delete all small residue"""
        allsr = select(sr for sr in NeutralLoss)
        for sr in allsr:
            sr.delete()
        commit()
    
    @db_session
    def insert_composition(self):
        """insert all small residue composition"""
        for key in self.srcdict:
            get_or_set(NeutralLoss, name=key)
            for atom in self.srcdict[key]:
                atomnb = get_or_set(AtomNB, f_atom=atom,
                                    occurences=self.srcdict
                                    [key][atom])
                atomnb.neutral_loss += get_or_set(NeutralLoss,
                                                  name=key)
                commit()
    
    @db_session
    def insert_ptmpos(self):
        """insert all amino acid and if it can have an action on the 
        ptm of each amino acid"""
        for elem in self.srlist:
            ptmpos = get_or_set(PTMPos, 
                                ptm=get_or_set(PTM, name=elem['ptm']), 
                                position=elem['position'], 
                                amino_acid=elem['aa'])
            ptmpos.neutral_loss += get_or_set(NeutralLoss,
                                              name=elem['name'])
    
    def insertall(self):
        """insert all small residue"""
        self.insert_composition()
        self.insert_ptmpos()
    
if __name__ == "__main__":
    #~ DB = Database('sqlite', 'temp.db', create_db=True)
    DB = Database('postgres', user='postgres', password='postgres',
                  host='localhost', database='pwaa')

    IA = InsertAtoms()
    IIA = InsertIsoAtoms()
    IAA = InsertAminoAcid()
    IP = InsertProtease()
    IL = InsertLink()
    IPTM = InsertPTM()
    IO = InsertOrganisms()
    IAAO = InsertAAOrganisms()
    ISR = InsertSmallResidue()
    INL = InsertNeutralLoss()
    
    #~ IA.cleanall()
    #~ IIA.cleanall()
    #~ IAA.cleanall()
    #~ IP.cleanall()
    #~ IL.cleanall()
    #~ IPTM.cleanall()
    #~ IO.cleanall()
    #~ IAAO.cleanall()
    #~ ISR.cleanall()
    #~ INL.cleanall()
    
    IA.insertall()
    IIA.insertall()
    IAA.insertall()
    IP.insertall()
    IL.insertall()
    IPTM.insertall()
    IO.insertall()
    IAAO.insertall()
    ISR.insertall()
    INL.insertall()

