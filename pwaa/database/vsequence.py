#!/usr/bin/env python
# encoding: utf-8
"""vsequence.py the base data get sequences from a mass, 
ppm
@author: Vezin Aurelien
@license: CECILL-B"""

from pony.orm import Database, select, db_session
from sets import Set
from decimal import Decimal
import sys

from pwaa.database.maadb import (Atom, IsoAtom, Protease, AminoAcid, Link,
                                 PTM, SmallResidue, NeutralLoss, AAPTMNL,
                                 Sequence, Mass)
 
from pwaa.basicknowledge.melemscreate import fill_all_link
from pwaa.basicknowledge.mcombielemscreate import reason_all_doublelinks
from pwaa.utils.atoms import MAtomsMassOp
                    
DB = Database('postgres', user='postgres', password='postgres',
              host='localhost', database='pwaa')



@db_session                
def get_mass_sequences(mass, ppm):
    """get the mass and the composition of all saved sequences
    @param mass:  the mass we are looking for
    @type mass: float
    @param ppm: the precision we have (in part per million)
    @type ppm: float
    @return: the list of all sequences with data 
    related to this sequence
    @rtype: list(dict(str: any))"""
    sequences = []
    massmin = Decimal(mass - mass * (ppm/1000000.))
    massmax = Decimal(mass + mass * (ppm/1000000.))
    #select all mass in the good range
    masses = select(m for m in Mass if m.mass >= massmin and m.mass <= massmax)
    for m in masses:
        for elem in m.sequences:
            #check if a sequence is knowed
            if len(elem.aaptmnls) != 0:
                #if a sequence is knowed adding it to the sequence list
                seq = dict()
                sequences.append(seq)
                seq["joinside"] = elem.joinside
                seq["cutside"] = elem.cutside
                seq["complement"] = elem.complement
                seq["compo"] = []
                for aaptmnl in elem.aaptmnls:
                    #save the compositon
                    aaptmnldic = dict()
                    seq["compo"].append(aaptmnldic)
                    aaptmnldic["amino_acid"] = aaptmnl.amino_acid.symbol
                    aaptmnldic["ptm"] = aaptmnl.ptm.name
                    if aaptmnl.nl != None:
                        aaptmnldic["nl"] = aaptmnl.nl.name
                    else:
                        aaptmnldic["nl"] = "None"
    return sequences
    

def get_double_center_dlinks(dlinks):
    """ get only the dlinks with center on both side
    @param dlinks: list of all double links
    @type dlinks: list(MDoubleLink)
    @return: the list of all double links matching with center
    on both side
    @rtype: list(MDoubleLink)"""
    ldcenter = []
    for elem in dlinks:
        if ((elem.cutside != "n-term" and elem.joinside != "c-term") and
                (elem.cutside != "c-term" and elem.joinside != "n-term")):
            ldcenter.append(elem)
    return ldcenter

def get_one_term_dlinks(dlinks):
    """ get only the dlinks with center on one side and a term on the 
    other side
    @param dlinks: list of all double links
    @type dlinks: list(MDoubleLink)
    @return: the list of all double links matching with center on 
    one side and a term on the other side
    @rtype: list(MDoubleLink)"""
    l_oneterm = []
    for elem in dlinks:
        if ((elem.joinside == "c-term" and elem.cutside != "n-term") or
                (elem.joinside == "n-term" and elem.cutside != "c-term")):
            l_oneterm.append(elem)
    return l_oneterm

def get_double_term_dlinks(dlinks):
    """ get only the dlinks with term on both side
    @param dlinks: list of all double links
    @type dlinks: list(MDoubleLink)
    @return: the list of all double links matching with a term on
    both side
    @rtype: list(MDoubleLink)"""
    l_doubleterm = []
    for elem in dlinks:
        if ((elem.joinside == "c-term" and elem.cutside == "n-term") or
                (elem.joinside == "n-term" and elem.cutside == "c-term")):
            l_doubleterm.append(elem)
    return l_doubleterm


@db_session
def get_sub_mass_fuzzy(mass, ppm, dlinks, firstside, secondside):
    """ get all sequences data related to a fuzzy sequence 
    (the composition of the sequence in apptmnl is not knowed) 
    and matching with all params
    @param mass: the mass we are looking for
    @type mass: float
    @param ppm: the precision we have (in part per million)
    @type ppm: int
    @param dlinks: the list of dlink to take in consideration
    @type dlinks: list(MDoubleLink)
    @param firstside: the type of cut we have to have on one side
    (center, nterm, cterm)
    @type firstside: str
    @param secondside: the type of cut we have to have on 
    the other side (center, nterm, cterm)
    @type secondside: str 
    @return: the set of all links we can have 
    with for all links the link related to the side 
    (joinside, cutside, complement)
    @rtype: list(tuple(str, str, str))"""
    sequences = Set([])
    masses = []
    for elem in dlinks:
        mlink = MAtomsMassOp.atomlist_to_masses(elem.composition, "mono")
        nmass = mass - mlink
        #to check if not nmass - mass
        mmin = Decimal(nmass - nmass * (ppm/1000000.))
        mmax = Decimal(nmass + nmass * (ppm/1000000.))
        masses.append((mmin, mmax, elem.joinside, 
                       elem.cutside, elem.lcomplement))
    for mmin, mmax, r_joinside, r_cutside, r_complement in masses:
        db_masses = select(m for m in Mass if m.mass >= mmin and m.mass <= mmax)
        for m in db_masses:
            for elem in m.sequences:
                if (len(elem.aaptmnls) == 0 and 
                        ((elem.cutside == firstside and 
                          elem.joinside == secondside) or
                         (elem.cutside == secondside and 
                          elem.joinside == firstside))):
                    seq = (r_joinside, r_cutside, r_complement)
                    sequences.add(seq)
    return sequences
                    
@db_session
def get_mass_fuzzy_sequences(mass, ppm, dlinks):
    """ get all sequence data from fuzzy sequence (we don't know the
    composition of the sequence) matching with a mass and  
    precision
    @param mass: the mass we are looking for
    @type mass: float
    @param ppm: the precision of the mass in part per million
    @type ppm: int
    @param dlinks: the list of double link to work with
    @type dlinks: list(MDoubleLink)
    """
    centerlinks = get_double_center_dlinks(dlinks)
    centersequences = get_sub_mass_fuzzy(mass, ppm, centerlinks, 
                                         "center", "center")
    onetermlinks = get_one_term_dlinks(dlinks)
    ntermsequences = get_sub_mass_fuzzy(mass, ppm, onetermlinks, 
                                        "nterm", "center")
    ctermsequences = get_sub_mass_fuzzy(mass, ppm, onetermlinks, 
                                        "center", "cterm")
    twotermlinks = get_double_term_dlinks(dlinks)
    cntermsequences = get_sub_mass_fuzzy(mass, ppm, twotermlinks, 
                                         "n-term", "cterm")
    sequences = centersequences.union(ntermsequences)
    sequences = sequences.union(ctermsequences)
    sequences = sequences.union(cntermsequences)
    return sequences


@db_session
def get_all_mass():
    """ get all mass from the database"""
    db_masses = select(m for m in Mass)
    masses_dict = {}
    for elem in db_masses:
        print elem.mass, elem.sequence


class StaticDBAcces(object):
    """ A static class to accees on sequences and masses in DB """
    
    db_masses = get_all_mass()
    links = fill_all_link()
    dlinks = reason_all_doublelinks(links)

    @classmethod
    def get_mass(cls, mass, ppm, join, end):
        """ The method to get possible sequences
        from a mass a ppm and a join and a cut side
        @param mass: the experiment mass
        @type mass: float
        @param ppm: the precision in ppm
        @type ppm: int
        @param join: the join side identifier
        @type join: str
        @param end: the cut side identifier
        @type end: str
        @return: a list of sequences (with mass)
        @rtype: Mass (database) """
        sequences = []
        massmin = Decimal(mass - mass * (ppm/1000000.))
        massmax = Decimal(mass + mass * (ppm/1000000.))
        cutside = []
        if end == True:
            cutside = ["n-term", "c-term"]
        else:
            cutside = [link.link for link in cls.links 
                       if link.link not in ["n-term", "c-term"]]
        
        sequences = [mass for mass in cls.db_masses 
                     if (mass.sequences.joinside == join and 
                         mass.mass > massmin and 
                         mass.mass < massmax and
                         mass.sequences.cutside in cutside)]
        return sequences

if __name__ == "__main__":
    #~ print get_mass_sequences(500., 50)
    LINKS = fill_all_link()
    DLINKS = reason_all_doublelinks(LINKS)
    get_all_mass()
    #~ print StaticDBAcces.get_mass(400, 50, "y-cut", False)
    
    #~ print get_mass_fuzzy_sequences(400., 50, DLINKS)
    #~ print get_mass_sequences(400., 50)
    #~ CENTER_DLINKS = get_double_center_dlinks(DLINKS)
    #~ print len(get_sub_mass_fuzzy(400., 10, CENTER_DLINKS,
                                 #~ "center", "center"))
