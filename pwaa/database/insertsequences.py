#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=unexpected-keyword-arg, no-value-for-parameter, no-self-use
"""insertsequences.py insert generated sequences and mass 
into database
@author: Vezin Aurelien
@license: CECILL-B"""

from pony.orm import commit, Database, db_session
import sys
from decimal import Decimal, ROUND_HALF_EVEN


from pwaa.database.maadb import (AAPTMNL, Sequence, Mass, AminoAcid, 
                                 PTM, NeutralLoss)
                            

from pwaa.basicknowledge.mcombielemscreate import (reason_all_doublelinks,
                                                   reason_all_aaptm,
                                                   reason_all_aaptmnl,
                                                   ReasonAllDLINKSequence,
                                                   ReasonAllMasses)                                            

from pwaa.basicknowledge.melemscreate import (fill_all_aa, fill_all_ptm,
                                              fill_all_link,
                                              fill_all_neutral_loss)
from pwaa.utils.atoms import MAtomsMassOp
from pwaa.utils.dbop import get_or_set



class InsertSequences(object):
    """ Class used to insert generated sequences and sequences mass to
    the database
    @ivar exactsize: the size of sequence to generate 
    with all informations
    @type exactsize: int
    @ivar masssize: the size of sequence to generate with only a
    mass
    @type masssize: int
    @ivar sequences: list of all sequences generated in a 
    ReasonAllSequences class
    @type sequences: ReasonAllSequences
    @ivar masses: masses for all possibles size and for each size
    of sequence until masssize (center, cterm, nterm, cnterm)
    @type masses: dict(list(int)), dict(list(int)), dict(list(int)),
    dict(list(int))"""
    
    def __init__(self, exactsize, masssize):
        """compute all sequences and masses possibles then
        save it in instance var.
        @param exactsize: the size of sequence to generate 
        with all informations
        @type exactsize: int
        @param masssize: the size of sequence to generate with only a
        mass
        @type masssize: int"""
        l_all_aa = fill_all_aa()
        l_all_ptm = fill_all_ptm()
        l_all_nl = fill_all_neutral_loss()
        l_all_link = fill_all_link()
        l_d_links = reason_all_doublelinks(l_all_link)
        l_all_aaptm = reason_all_aaptm(l_all_aa, l_all_ptm)
        l_all_aaptmnl = reason_all_aaptmnl(l_all_aaptm, l_all_nl)
        self.exactsize = exactsize
        self.masssize = masssize
        self.sequences = ReasonAllDLINKSequence(l_all_aaptmnl,
                                                l_d_links,
                                                exactsize)
        self.sequences.reason_all_sequence()
        mass = ReasonAllMasses(masssize, l_all_aaptmnl, 2)
        mass.set_base()
        self.masses = mass.set_all_masses()
    
    
    @db_session
    def save_exact_sequences(self):
        """ save exact sequences with mass (4 digit after comma)
        and exact links """
        for key in self.sequences.seqs:
            for elem in self.sequences.seqs[key]:
                mass = MAtomsMassOp.atomlist_to_masses(elem.composition,
                                                       "mono")
                massd = Decimal(Decimal(mass).quantize(Decimal('.0001'), 
                                                       rounding=
                                                       ROUND_HALF_EVEN))
                #saving mass                          
                get_or_set(Mass, mass=massd)
                #create sequence
                sequencedb = Sequence(joinside=elem.joinside,
                                      cutside=elem.cutside,
                                      complement=elem.compl,
                                      mass=Mass.get(mass=massd))                                   
                for aaptmnl in elem.sequence:
                    nldb = NeutralLoss.get(name=aaptmnl[2])
                    aadb = AminoAcid.get(symbol=aaptmnl[0])
                    ptmdb = PTM.get(name=aaptmnl[1])
                    aaptmnldb = get_or_set(AAPTMNL, amino_acid=aadb, 
                                           ptm=ptmdb, nl=nldb)
                    sequencedb.aaptmnls += aaptmnldb
                    commit()
                commit()
    
    @db_session
    def save_mass_side(self, lmass, csideval, nsideval):
        """save all mass in lmass (a list of mass) in the database
        @param lmass: lmass is a list of mass rounded with 2 digits
        after comma and convert in int
        @type lmass: int
        @param csideval: the type of link on the cside (center, c-term)
        @type csideval: str
        @param nsideval: the type of link on the nside (center, n-term)
        @type nsideval: str"""
        for mass in lmass:
            massf = float(mass)/(10 ** 2)
            massd = Decimal(Decimal(massf).quantize(Decimal('.01'), 
                                                    rounding=
                                                    ROUND_HALF_EVEN))
            get_or_set(Mass, mass=massd)
            if csideval == "center" and nsideval == "center":
                Sequence(joinside=csideval,
                         cutside=nsideval,
                         complement="unknow",
                         mass=Mass.get(mass=massd))
            elif csideval == "center" and nsideval == "n-term":
                Sequence(joinside=nsideval,
                         cutside=csideval,
                         complement="None",
                         mass=Mass.get(mass=massd))
            elif csideval == "c-term" and nsideval == "center":
                Sequence(joinside=csideval,
                         cutside=nsideval,
                         complement="None",
                         mass=Mass.get(mass=massd))
            else:
                #double to have both order cterm to nterm and n-term to
                #c-term. Check if it will be really usefull later
                Sequence(joinside=csideval,
                         cutside=nsideval,
                         complement="None",
                         mass=Mass.get(mass=massd))
                Sequence(joinside=nsideval,
                         cutside=csideval,
                         complement="None",
                         mass=Mass.get(mass=massd))
                
            commit()
            
    def save_round_mass(self):
        """save all mass in the database"""
        center, cterm, nterm, cnterm = self.masses
        for key in range(self.exactsize+1, self.masssize+1):
            #~ print key
            self.save_mass_side(center[key], "center", "center")
            #~ print "center"
            self.save_mass_side(cterm[key], "cterm", "center")
            #~ print "cterm"
            self.save_mass_side(nterm[key], "center", "nterm")
            #~ print "nterm"
            self.save_mass_side(cnterm[key], "cterm", "nterm")
            #~ print "cnterm"

if __name__ == "__main__":
    #~ DB = Database('sqlite', 'temp.db', create_db=True)
    DB = Database('postgres', user='postgres', password='postgres',
                  host='localhost', database='pwaa')
    
    IES = InsertSequences(2, 4)
    IES.save_exact_sequences()
    IES.save_round_mass()
