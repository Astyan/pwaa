#!/usr/bin/env python
# encoding: utf-8
#pylint: disable=too-few-public-methods, no-init
"""maadb.py the model of the object in the database 
The goal of the database is to store all knowed
stuff on proteomic
@author: Vezin Aurelien
@license: CECILL-B"""

from pony.orm import (Database, PrimaryKey, Required, Set, 
                      composite_key, Optional)
from decimal import Decimal


#~ DB = Database('sqlite', 'temp.db', create_db=True)
DB = Database('postgres', user='postgres', password='postgres',
              host='localhost', database='pwaa')

#atom and isoatom

class Atom(DB.Entity):
    """This class reprensent atoms with theres symbols and names
    it's db table with relations:
    It's a table of atoms with:
    atom symbol as primary key
    the name of the atom
    a N relation to IsoAtom class/table 
    a N relation to AtomNB class/table 
    """
    symbol = PrimaryKey(str, auto=False)
    name = Required(str, unique=True)
    iso_atoms = Set('IsoAtom')
    atoms_nb = Set('AtomNB')

class IsoAtom(DB.Entity):
    """This class represent isotop for all atoms with an atom the 
    atom, the isotopic number the mass (in dalton) and an abound(%)
    it's a db table with relations:
    a 1 relation to Atom class/table
    a isotop number
    a mass
    a abound value
    the key is composed of the atom and the isotop number
    """
    f_atom = Required('Atom')
    isotop = Required(int)
    mass = Required(float)
    abound = Required(float)
    composite_key(f_atom, isotop)
    
class AtomNB(DB.Entity):
    """This class represent an atom with it number of occurences
    it's a db table with relations:
    a 1 relation to Atom class/table
    a number of occurences
    a N relation to AminoAcid class/table
    a N relation to Link class/table
    a N relation to SmallResidue class/table
    the key is composed of the atom and the number of occurences
    """
    f_atom = Required('Atom')
    occurences = Required(int)
    amino_acid = Set('AminoAcid')
    ptm = Set('PTM')
    link = Set('Link', reverse='atoms')
    unprotlink = Set('Link', reverse='unprotatoms')
    small_residue = Set('SmallResidue')
    neutral_loss = Set('NeutralLoss')
    composite_key(f_atom, occurences)

#Protease

class Protease(DB.Entity): 
    """This class represent a protease with it name and a name and the
    amino acid where it's cut.
    its a db table with relations:
    the name of the protease as primary key
    a N relation to AminoAcid class/table
    """
    name = PrimaryKey(str, auto=False)
    cleave = Set('AminoAcid', column="symbol")

#Amino Acid

class AminoAcid(DB.Entity):
    """This class represent amino acid with there symbols,
    there name, a composition in atoms
    iuts a db table with relations:
    the amino acid symbol as primary key
    a name 
    a N relation to Protease class/table
    a N relation to AtomNB class/table
    a N relation to PTMPos class/table
    a N relation to AAPTM class/table
    a N relation to AAOrganism class/table
    a N relation to AAPTMNL class/table
    """
    symbol = PrimaryKey(str, auto=False)
    name = Required(str, unique=True)
    protease = Set('Protease', column="name")
    atoms = Set('AtomNB')
    ptm_pos = Set('PTMPos')
    aaorganism = Set('AAOrganism')
    aaptmnl = Set('AAPTMNL')

#Link

class Link(DB.Entity):
    """This class represent a link cut/link/term type like (c-term, 
    or y-cut) it has a name a composition in atom, a complementary
    (for exemple the complementary of y-cut is b-but), an 
    unproteoned composition in atoms and a side
    (n-term or c-term)
    it's a db table with relations:
    A name of the link (n-term, b-cut, etc ...)
    a N relation to AtomNB class/table
    another N relation to AtomNB class/table
    A complementary
    A side
    """
    name = PrimaryKey(str, auto=False)
    atoms = Set('AtomNB', reverse='link')
    unprotatoms = Set('AtomNB', reverse='unprotlink')
    complementary = Optional('Link', reverse='selflink')
    selflink = Optional('Link', reverse='complementary')
    opposite = Set('Link', reverse='myside')
    myside = Set('Link', reverse='opposite')
    

#PTM

class PTM(DB.Entity):
    """This class represent PTM it has a name, a composition in atoms
    it's a db table with relations:
    the name of the PTM
    a N relation to AtomNB
    a N relation to PTMPos class/table
    a N relation to the PTMOrganism class/table
    a N relation to AAPTMNL class/table
    """ 
    name = Required(str)
    atoms = Set('AtomNB')
    pos = Set('PTMPos')
    ptmorganism = Set('PTMOrganism')
    aaptmnl = Set('AAPTMNL')

class PTMPos(DB.Entity):
    """This class represent the capacity for a ptm to go on an amino 
    acid it has a position (c-term, n-term, center), a PTM and a
    Amino Acid
    a db table with relations:
    a position (c-term, n-term, center)
    a 1 relation to PTM class/table
    a 1 relation to AminoAcid class/table
    a N relation to SmallResidue class/table
    """
    position = Required(str)
    ptm = Required('PTM')
    amino_acid = Required('AminoAcid')
    small_residue = Set('SmallResidue')
    neutral_loss = Set('NeutralLoss')

#Organism

class Organism(DB.Entity):
    """This class represent  an organisme it has a name, can have 
    a parent and can have childs
    a db table with relations:
    A type of organism
    A possible 1 relation to another type of organism 
    which is his parent
    A N relation to X Organisms that are his childs
    A N relation to AAOrganism class/table
    A N relation to PTMOrganism class/table
    """
    name = PrimaryKey(str, auto=False)
    parent = Optional('Organism', reverse='childs')
    childs = Set('Organism', reverse='parent')
    aaorganism = Set('AAOrganism')
    ptmorganism = Set('PTMOrganism')

class AAOrganism(DB.Entity):
    """This class represent the possibility for an organisme to have 
    a kind of amino acid. It's have an organisme and an amino acid
    it's a db table with relations:
    A 1 relation to AminoAcid class/table
    A 1 relation to Organism class/table"""
    amino_acid = Required('AminoAcid')
    organism = Required('Organism')
    composite_key(amino_acid, organism)

class PTMOrganism(DB.Entity):
    """This class represent the possibility for an organisme to a 
    a kind of ptm. It's have a ptm and an organisme 
    it's a db table with relations:
    A 1 relation to PTM class/table
    A 1 relation to Organism class/table"""
    ptm = Required('PTM')
    organism = Required('Organism')
    composite_key(ptm, organism)

#SmallResidue

class SmallResidue(DB.Entity):
    """this class represent smallresidues (for exemple immonium ions)
    it has a name, a composition in atom and a PTMPos which give
    a ptm, an amino acid and a position(n-term, c-term, center) to 
    know on what kind of amino acid and ptm the small residue could 
    comes 
    a db table with relations:
    A name
    A N relation to AtomNB class/table to represent a composition
    A N relation to PTMPos class/table
    """
    name = PrimaryKey(str, auto=False)
    atoms = Set('AtomNB')
    ptmpos = Set('PTMPos')

#NeutralLoss

class NeutralLoss(DB.Entity):
    """ This class represent neutral loss (for exemple loss of H20)
    it has a name, a composition in atoms and a PTMPos which give
    a ptm, an amino acid and a position(n-term, c-term, center) to 
    know on what kind of amino acid and ptm the neutral loss could 
    comes 
    a db table with relations:
    A name
    A N relation to AtomNB class/table to represent a composition
    A N relation to PTMPos class/table
    A N relation to AAPTMNL class/table
    """
    name = PrimaryKey(str, auto=False)
    atoms = Set('AtomNB')
    ptmpos = Set('PTMPos')
    aaptmnl = Set('AAPTMNL')
    
#Sequence

class AAPTMNL(DB.Entity):
    """This class represente a AAPTMNL (a combinaison of amino acid,
    ptm and neutral loss
    it can be in an any number of sequence
    a db table with relations:
    A 1 relation to AminoAcid class/table
    A 1 relation to PTM class/table
    A 1 relation to NeutralLoss class/table
    A N rekatuib to a Sequence class/table"""
    amino_acid = Required('AminoAcid')
    ptm = Required('PTM')
    nl = Optional('NeutralLoss')
    sequence = Set('Sequence')

class Sequence(DB.Entity):
    """This class represente a Sequence with link or term of both side 
    (n-term, c-term, a-cut, x-cut, b-cut, y-cut, c-cut, z-cut, center).
    A various number of aaptmnls (can be none) and a mass.
    a db table with relations:
    A cside
    A nside
    A 1 relation to Mass class/table
    A N realtion to AAPTMNL class/table
    """
    #can be c-term and any c-link plus center
    pk = PrimaryKey(int, auto=True)
    joinside = Required(str)
    cutside = Required(str)
    complement = Required(str)
    mass = Required('Mass')
    aaptmnls = Set('AAPTMNL')

#Mass    

class Mass(DB.Entity):
    """This class represente a Mass with a mass in dalton representing
    in Decimal
    A mass (unique as Primary key)
    A N relation to Sequence class/table
    """
    mass = PrimaryKey(Decimal, 15, 5)
    sequences = Set('Sequence')


#do not put in main section
DB.generate_mapping(check_tables=True, create_tables=True)


