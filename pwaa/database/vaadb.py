#!/usr/bin/env python
# encoding: utf-8
"""initaadb.py the base data to insert into the database 
@author: Vezin Aurelien
@license: CECILL-B"""

from pony.orm import Database, select, db_session
import sys
from decimal import Decimal

from pwaa.database.maadb import (Atom, IsoAtom, Protease, AminoAcid, Link,
                                 PTM, SmallResidue, NeutralLoss, AAPTMNL,
                                 Sequence, Mass)

DB = Database('postgres', user='postgres', password='postgres',
              host='localhost', database='pwaa')

@db_session
def get_atoms():
    """ Create a dict of atoms
    @return: a dict with every atom in the db with there mass,
    abound and idotop number
    @rtype: dict(str : (float, float, int))"""
    #used by MAtom
    atomdict = {}
    atom = select(atom for atom in Atom)
    for a in atom:
        atomdict[a.symbol] = []
        iso = select(iso for iso in IsoAtom if iso.f_atom == a)
        for i in iso:
            atomdict[a.symbol].append((i.mass, i.abound, i.isotop))
    return atomdict
    
@db_session
def get_amino_acid():
    #used by MAminoAcid and fill_all_aa()
    """ Create a dict of amino acids composition 
    @return: a dict with every amino acid in the db with there 
    composition as a atom, number of occurences dict
    @rtype: dict(str : dict(str: int))"""
    amino_acid_composition = {}
    aas = select(aa for aa in AminoAcid)
    for aa in aas:
        amino_acid_composition[aa.symbol] = {}
        for atomnb in aa.atoms:
            amino_acid_composition[aa.symbol][atomnb.f_atom.symbol] = \
            atomnb.occurences
    return amino_acid_composition
 
    
@db_session
def get_protease():
    """ Create a dict of protease with the amino acid cleave by each
    proteases
    @return:  a dict of protease with name as key and a 
    list of amino acid cleave by the protease get from the bd
    @rtype: dict(str: list(str))"""
    #used by MProtease
    proteasedict = {}
    proteases = select(protease for protease in Protease)
    for protease in proteases:
        proteasedict[protease.name] = []
        for aa in protease.cleave:
            proteasedict[protease.name].append(aa.symbol)
    return proteasedict
    
@db_session
def get_ptm_compo():
    """ Create a dict of ptms composition 
    @return: a dict with every ptms in the db with there 
    composition as a atom, number of occurences dict
    @rtype: dict(str : dict(str: int))"""
    #used by MPTM and fill_all_ptm
    ptm_composition = {}
    ptms = select(ptm for ptm in PTM)
    for ptm in ptms:
        ptm_composition[ptm.name] = {}
        for atom in ptm.atoms:
            ptm_composition[ptm.name][atom.f_atom.symbol] = \
            atom.occurences
    return ptm_composition 

@db_session
def get_ptm_pos():
    """ Create a dict of ptms position
    @return: a dict with every ptms in the db with the amino acid and
    position they can combine
    @rtype: dict(str : dict(str: list(str)))"""
    #used by MPTM and fill_all_ptm
    ptm_position = {}
    ptms = select(ptm for ptm in PTM)
    for ptm in ptms:
        ptm_position[ptm.name] = {}
        for pos in ptm.pos:
            ptm_position[ptm.name][pos.amino_acid.symbol] = []
        for pos in ptm.pos:
            ptm_position[ptm.name][pos.amino_acid.symbol].append(pos.position)
    return ptm_position 

@db_session
def get_small_residue_compo():
    """ create a dict of small residue and there composition
    @return: a dict with every small residue and there composition
    @rtype: dict(str: dict(char : int)) """
    #used by fill_all_residue
    srs_compo = {}
    srs = select(sr for sr in SmallResidue)
    for sr in srs:
        srs_compo[sr.name] = {}
        for atomnb in sr.atoms:
            srs_compo[sr.name][atomnb.f_atom.symbol] = atomnb.occurences
    return srs_compo

@db_session
def get_small_residue_pos():
    """create a dict of small residue and there possible possition
    @return: a dict with the small residu name and the amino acid and
    if it can append for ptm of this amino acid
    @rtype: dict(str :dict(str : char))
    """
    #used by fill_all_residue
    srs_pos = {}
    srs = select(sr for sr in SmallResidue)
    for sr in srs:
        srs_pos[sr.name] = []
        for ptmpos in sr.ptmpos:
            srs_pos[sr.name].append({})
            srs_pos[sr.name][-1]['aa'] = ptmpos.amino_acid.symbol
            srs_pos[sr.name][-1]['ptms'] = ptmpos.ptm.name
            srs_pos[sr.name][-1]['position'] = ptmpos.position
    return srs_pos
    
@db_session
def get_link():
    """create a list of links with the name, 
    composition and the complementary
    @return: a list of link with (name, composition, complementary)"""
    links_list = []
    links = select(link for link in Link)
    for link in links:
        d_link = {}
        d_link["name"] = link.name
        d_link["composition"] = {}
        d_link["unprotcompo"] = {}
        d_link["opposite"] = []
        if link.complementary != None:
            d_link["complementary"] = link.complementary.name
        else:
            d_link["complementary"] = None
        for atomnb in link.atoms:
            d_link["composition"][atomnb.f_atom.symbol] = atomnb.occurences
        for atomnb in link.unprotatoms:
            d_link["unprotcompo"][atomnb.f_atom.symbol] = atomnb.occurences
        for lelem in link.opposite:
            d_link["opposite"].append(lelem.name)
        links_list.append(d_link)
    return links_list

@db_session
def get_link_opposite(link_attr):
    """ create a list of opposite cut/name name to link 
    @param link_attr: the name of a link (cut or term)
    @type link_attr: string
    @return: a list of link(cut or term) name
    @rtype: list(string)"""
    links_name_l = []
    links = select(link for link in Link)
    for link in links:
        if link.name == link_attr:
            for opposite in link.opposite:
                links_name_l.append(opposite.name)
    return links_name_l
    
@db_session
def get_neutral_loss_compo():
    """ create a dict of neutral loss and there composition
    @return: a dict with every neutral loss and there composition
    @rtype: dict(str: dict(char : int)) """
    #used by fill_all_neutral_loss
    nls_compo = {}
    nls = select(nl for nl in NeutralLoss)
    for nl in nls:
        nls_compo[nl.name] = {}
        for atomnb in nl.atoms:
            nls_compo[nl.name][atomnb.f_atom.symbol] = atomnb.occurences
    return nls_compo

@db_session
def get_neutral_loss_pos():
    """create a dict of neutral loss and there possible possition
    @return: a dict with the neutral loss name and the amino acid and
    if it can append for ptm of this amino acid
    @rtype: dict(str :dict(str : char))
    """
    #used by fill_all_neutral_loss
    nls_pos = {}
    nls = select(nl for nl in NeutralLoss)
    for nl in nls:
        nls_pos[nl.name] = []
        for ptmpos in nl.ptmpos:
            nls_pos[nl.name].append({})
            nls_pos[nl.name][-1]['aa'] = ptmpos.amino_acid.symbol
            nls_pos[nl.name][-1]['ptms'] = ptmpos.ptm.name
            nls_pos[nl.name][-1]['position'] = ptmpos.position
    return nls_pos
    

if __name__ == "__main__":
    pass
    #~ get_mass_sequences(400., 50)
    #~ links = fill_all_link()
    #~ get_mass_fuzzy_sequences(400., 50, links)
    #~ print get_link_opposite("n-term")
    #~ print get_neutral_loss_compo()
    #~ print get_neutral_loss_pos()
    #~ get_small_residue_pos()
    #~ print get_ptm_compo()
    #~ print get_ptm_pos()
